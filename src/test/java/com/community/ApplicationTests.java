package com.community;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.community.model.Consumer;
import com.community.repository.ConsumerRepository;
import com.jayway.restassured.RestAssured;

/**
 * 
 * @author Sameer Shukla
 *
 *         Ideally Rest assured should be used for few important scenarios, not
 *         for every single operation it might effect build time. Also, rest
 *         development happens periodically ideally one should used Rest assured
 *         for version tracking n all. for ex /url/v1/op etc.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@Ignore
public class ApplicationTests {

	// @Test
	public void contextLoads() {
	}

	@Autowired
	private ConsumerRepository consumerRepository;

	@Value("${local.server.port}")
	private int serverPort;

	@Before
	public void setUp() {
		RestAssured.port = serverPort;
	}

	@After
	public void flushAndClean() {
	}

	/**
	 * Check Status Code 200Ok
	 */
	@Test
	public void testGetConsumerId() {
		Consumer entity = create();
		given().contentType("application/json").when().get("/consumer/" + entity.getId()).then()
				.statusCode(HttpStatus.SC_OK);
		this.consumerRepository.delete(entity.getId());
	}

	/**
	 * Check Status Code 200Ok
	 */

	@Test
	public void testGetConsumers() {

		given().contentType("application/json").when().get("/consumer").then().statusCode(HttpStatus.SC_OK);
	}

	/**
	 * Validate Response Body
	 */
	@Test
	public void testGetConsumerBody() {
		Consumer entity = create();
		given().contentType("application/json").when().get("/consumer/" + entity.getId()).then().statusCode(200)
				.body("firstName", equalTo("Sameer"), "emailAddress", equalTo("test@test.com"), 
						"phoneNumber", equalTo(8806491795L));
		this.consumerRepository.delete(entity.getId());
	}

	/**
	 * Handle 404 scenarios
	 */
	@Test
	public void handle404NotFound() {
		given().contentType("application/json").when().get("/consumer/100000").then()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}

	/**
	 * Handle 500 Internal Server Error
	 */
	@Test
	public void handleAllInternalServerErrors() {
		given().contentType("application/json").when().get("/consumer/abcd").then()
				.statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
	}

	/**
	 * Return Id create Consumer
	 * 
	 * TODO: Abhijeet, please handle exception here. If creation failed, then
	 * terminate test case execution gracefully.
	 */
	private Consumer create() {
		Consumer consumer = new Consumer();
		consumer.setFirstName("Sameer");
		consumer.setLastName("Shukla");
		consumer.setEmailAddress("test@test.com");
		consumer.setPassword("abc123");
		consumer.setZipCode("411021");
		consumer.setPhoneNumber(8806491795L);
		Consumer entity = this.consumerRepository.save(consumer);
		return entity;
	}
}
