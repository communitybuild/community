-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.14.04.1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema community
--

CREATE DATABASE IF NOT EXISTS community;
USE community;

--
-- Definition of table `community`.`account`
--

DROP TABLE IF EXISTS `community`.`account`;
CREATE TABLE  `community`.`account` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(200) NOT NULL,
  `ENABLED` tinyint(1) NOT NULL DEFAULT '1',
  `CREDENTIALSEXPIRED` tinyint(1) NOT NULL DEFAULT '0',
  `EXPIRED` tinyint(1) NOT NULL DEFAULT '0',
  `LOCKED` tinyint(1) NOT NULL DEFAULT '0',
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LAST_UPDATE_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UQ_ACCOUNT_USERNAME` (`USERNAME`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`account`
--

/*!40000 ALTER TABLE `account` DISABLE KEYS */;
LOCK TABLES `account` WRITE;
INSERT INTO `community`.`account` VALUES  (2,'a@g.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-22 22:26:39','2016-10-17 16:15:43'),
 (3,'m@g.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-21 17:04:20','2016-11-22 15:38:35'),
 (4,'a@d.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-11-25 17:13:59','2016-11-25 17:13:59'),
 (5,'h@b.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-23 15:02:32','0000-00-00 00:00:00'),
 (6,'yo@g.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-23 15:13:46','0000-00-00 00:00:00'),
 (7,'try@g.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-23 15:22:26','0000-00-00 00:00:00'),
 (8,'a@b.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-23 15:30:36','0000-00-00 00:00:00'),
 (9,'a@c.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-23 15:42:39','0000-00-00 00:00:00'),
 (10,'a@e.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-23 15:51:37','0000-00-00 00:00:00'),
 (11,'new@g.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2016-12-27 16:37:20','0000-00-00 00:00:00'),
 (13,'aishwarya.dhopate1@anveshak.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-17 11:10:04','0000-00-00 00:00:00'),
 (16,'aish.dhopate@gmail.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-04 15:23:43','0000-00-00 00:00:00'),
 (17,'','d41d8cd98f00b204e9800998ecf8427e',1,0,0,0,'2017-01-10 10:53:56','0000-00-00 00:00:00'),
 (18,'sis@hd.cjjc','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-11 10:07:42','0000-00-00 00:00:00'),
 (19,'a@m.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-11 10:18:36','0000-00-00 00:00:00'),
 (21,'madhuri.dixit@anveshak.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-11 11:14:50','0000-00-00 00:00:00'),
 (22,'adityvy@gmail.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-11 11:20:57','0000-00-00 00:00:00'),
 (23,'aditivy@gmail.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-16 09:24:25','0000-00-00 00:00:00'),
 (25,'shashank.thakur@anveshak.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-17 11:03:43','0000-00-00 00:00:00'),
 (30,'aishwarya.dhopate@anveshak2.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-30 18:21:26','0000-00-00 00:00:00'),
 (31,'test1@gmail.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-17 11:26:46','0000-00-00 00:00:00'),
 (32,'sunil.marne@anveshak.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-01-20 10:20:18','0000-00-00 00:00:00'),
 (33,'snigdha.kadam@anveshak.com','e10adc3949ba59abbe56e057f20f883e',1,0,0,0,'2017-02-04 12:57:05','0000-00-00 00:00:00');
UNLOCK TABLES;
/*!40000 ALTER TABLE `account` ENABLE KEYS */;


--
-- Definition of table `community`.`account_roles`
--

DROP TABLE IF EXISTS `community`.`account_roles`;
CREATE TABLE  `community`.`account_roles` (
  `ACCOUNT_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ACCOUNT_ID`,`ROLE_ID`),
  KEY `FK_ACCOUNT_ROLES_ROLE_ID` (`ROLE_ID`),
  CONSTRAINT `FK_ACCOUNT_ROLES_ACCOUNT_ID` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ACCOUNT_ROLES_ROLE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`account_roles`
--

/*!40000 ALTER TABLE `account_roles` DISABLE KEYS */;
LOCK TABLES `account_roles` WRITE;
INSERT INTO `community`.`account_roles` VALUES  (2,1),
 (3,1),
 (4,1),
 (5,1),
 (6,1),
 (7,1),
 (8,1),
 (9,1),
 (10,1),
 (11,1),
 (13,1),
 (16,1),
 (17,1),
 (18,1),
 (19,1),
 (21,1),
 (22,1),
 (23,1),
 (25,1),
 (30,1),
 (31,1),
 (32,1),
 (33,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `account_roles` ENABLE KEYS */;


--
-- Definition of table `community`.`activity`
--

DROP TABLE IF EXISTS `community`.`activity`;
CREATE TABLE  `community`.`activity` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(5000) DEFAULT NULL,
  `ACTIVITY_COST` bigint(20) NOT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`activity`
--

/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
LOCK TABLES `activity` WRITE;
INSERT INTO `community`.`activity` VALUES  (1,'Household Help','Household Help',10,'2017-01-22 15:20:00'),
 (2,'Pet Sitting','abc',5,'2017-01-22 15:20:00'),
 (3,'Errands','abc',15,'2017-01-22 15:20:00'),
 (4,'Yard work','abc',20,'2017-01-22 15:20:00');
UNLOCK TABLES;
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;


--
-- Definition of table `community`.`businesses`
--

DROP TABLE IF EXISTS `community`.`businesses`;
CREATE TABLE  `community`.`businesses` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(250) NOT NULL,
  `IMAGE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(5000) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(200) DEFAULT NULL,
  `PHONE_NUMBER` bigint(20) DEFAULT NULL,
  `CONSUMER_ID` bigint(20) NOT NULL,
  `COMMUNITY_ID` bigint(20) NOT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `community`.`businesses`
--

/*!40000 ALTER TABLE `businesses` DISABLE KEYS */;
LOCK TABLES `businesses` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `businesses` ENABLE KEYS */;


--
-- Definition of table `community`.`chat`
--

DROP TABLE IF EXISTS `community`.`chat`;
CREATE TABLE  `community`.`chat` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SENDER_ID` bigint(20) unsigned NOT NULL,
  `RECEIVER_ID` bigint(20) unsigned NOT NULL,
  `CHAT_CONTENT` varchar(5000) DEFAULT NULL,
  `WORKREQUEST_ID` bigint(20) DEFAULT NULL,
  `READ_FLAG` int(11) NOT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `community`.`chat`
--

/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
LOCK TABLES `chat` WRITE;
INSERT INTO `community`.`chat` VALUES  (1,1,3,'1',2,0,'2016-12-20 12:27:15'),
 (117,1,3,'2',2,0,'2016-12-20 12:48:36'),
 (118,3,1,'3',NULL,0,'2016-12-20 12:51:01'),
 (119,3,1,'3',2,0,'2016-12-20 14:17:06'),
 (120,1,3,'4',2,0,'2016-12-20 14:17:11'),
 (121,1,3,'5',2,0,'2016-12-20 14:20:30'),
 (122,1,3,'6',2,0,'2016-12-20 14:24:09'),
 (123,1,3,'7',2,0,'2016-12-20 14:29:51'),
 (124,1,3,'8',2,0,'2016-12-20 14:30:51'),
 (125,3,1,'9',4,1,'2017-01-04 09:59:32'),
 (126,3,1,'cccccccccccccccccccccccccccccccccccv',4,1,'2017-01-04 09:59:32'),
 (127,3,1,'yo',3,1,'2016-12-28 09:55:41'),
 (128,1,3,'1',3,0,'2016-12-26 14:42:45'),
 (129,1,2,'yoo gal',4,1,'2016-12-28 09:54:40'),
 (130,1,2,'hows u',4,1,'2016-12-28 09:54:40'),
 (133,1,3,'2',3,0,'2016-12-28 09:55:45'),
 (134,2,1,'1',4,1,'2017-01-04 09:59:32'),
 (135,2,1,'2',4,1,'2017-01-04 09:59:32'),
 (136,2,1,'3',4,1,'2017-01-06 14:02:28'),
 (137,3,12,'Hey',14,1,'2017-01-10 13:58:20'),
 (138,12,3,'hi',14,0,'2017-01-11 10:40:08'),
 (139,12,1,'aish',28,1,'2017-01-30 14:20:56'),
 (140,1,12,'hey',28,1,'2017-01-30 14:30:03'),
 (141,12,1,'yo',28,1,'2017-01-30 19:01:57'),
 (142,12,1,'abc',28,1,'2017-01-30 19:02:47'),
 (143,12,1,'dcccc',28,1,'2017-01-31 10:25:22'),
 (144,22,1,'abc',32,1,'2017-02-05 12:59:39'),
 (145,22,1,'abc',32,1,'2017-02-05 12:25:25'),
 (146,22,1,'a@g.com',33,1,'2017-02-05 14:15:11'),
 (147,22,1,'abc',32,1,'2017-02-05 14:18:30'),
 (148,22,1,'abc',33,1,'2017-02-05 14:18:34'),
 (149,1,22,'abc',33,1,'2017-02-05 14:52:55'),
 (150,1,22,'a',33,1,'2017-02-05 14:52:55'),
 (151,1,22,'a',33,1,'2017-02-05 14:52:55'),
 (152,1,22,'1',33,1,'2017-02-05 14:52:55'),
 (153,1,22,'2',33,1,'2017-02-05 14:52:55'),
 (154,1,22,'3',33,1,'2017-02-05 14:52:55'),
 (155,1,22,'4',32,1,'2017-02-05 14:49:12'),
 (156,1,22,'4',33,1,'2017-02-05 14:52:55'),
 (157,1,22,'5',33,1,'2017-02-05 14:52:55'),
 (158,22,1,'6',33,1,'2017-02-05 15:00:00');
UNLOCK TABLES;
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;


--
-- Definition of table `community`.`community`
--

DROP TABLE IF EXISTS `community`.`community`;
CREATE TABLE  `community`.`community` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(5000) DEFAULT NULL,
  `ZIP_CODE` varchar(20) DEFAULT NULL,
  `REFERRAL_CODE` varchar(50) DEFAULT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TYPE` varchar(50) NOT NULL,
  `COMMUNITY_IMAGES` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`community`
--

/*!40000 ALTER TABLE `community` DISABLE KEYS */;
LOCK TABLES `community` WRITE;
INSERT INTO `community`.`community` VALUES  (1,'abc','abc','415110','XPNG9','2017-02-05 20:15:01','school','community1,community2,community3,community4,community5,community6,community7'),
 (2,'pqr','pqr','415101','XPNG8','2016-12-23 14:04:41','Neighborhood',NULL),
 (3,'xyz','xyz','415122','XPNG7','2017-01-17 11:25:45','Church',NULL),
 (4,'Pune','Pune','410110','XPNG6','2017-02-03 16:22:25','City',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `community` ENABLE KEYS */;


--
-- Definition of table `community`.`community_activities`
--

DROP TABLE IF EXISTS `community`.`community_activities`;
CREATE TABLE  `community`.`community_activities` (
  `COMMUNITY_ID` bigint(20) NOT NULL,
  `ACTIVITY_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`COMMUNITY_ID`,`ACTIVITY_ID`),
  KEY `COMMUNITY_ACTIVITIES_COMMUNITY_ID_idx` (`COMMUNITY_ID`),
  KEY `COMMUNITY_ACTIVITIES_ACTIVITY_ID_idx` (`ACTIVITY_ID`),
  CONSTRAINT `FK_COMMUNITY_ACTIVITIES_ACTIVITY_ID` FOREIGN KEY (`ACTIVITY_ID`) REFERENCES `activity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_COMMUNITY_ACTIVITIES_COMMUNITY_ID` FOREIGN KEY (`COMMUNITY_ID`) REFERENCES `community` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`community_activities`
--

/*!40000 ALTER TABLE `community_activities` DISABLE KEYS */;
LOCK TABLES `community_activities` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `community_activities` ENABLE KEYS */;


--
-- Definition of table `community`.`consumer`
--

DROP TABLE IF EXISTS `community`.`consumer`;
CREATE TABLE  `community`.`consumer` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `EMAIL_ADDRESS` varchar(255) NOT NULL,
  `SSN` varchar(50) DEFAULT NULL,
  `DOB` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(200) NOT NULL,
  `PROFILE_PIC` varchar(255) DEFAULT NULL,
  `PHONE_NUMBER` bigint(20) DEFAULT NULL,
  `ZIP_CODE` varchar(20) DEFAULT NULL,
  `MANAGER_COMMUNITY` bigint(20) DEFAULT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LAST_UPDATE_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CONSUMER_UNIQUE_EMAIL_ADDRESS` (`EMAIL_ADDRESS`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`consumer`
--

/*!40000 ALTER TABLE `consumer` DISABLE KEYS */;
LOCK TABLES `consumer` WRITE;
INSERT INTO `community`.`consumer` VALUES  (1,'aish','dhopate','a@g.com','111','2016-11-27','e10adc3949ba59abbe56e057f20f883e','profile_1486369615665.jpg',NULL,NULL,1,'2017-02-08 16:05:37','2016-10-17 16:15:44'),
 (2,'maddy','dixit','m@g.com','','','e10adc3949ba59abbe56e057f20f883e','profile_1482313454929',123456789,'123456',0,'2016-12-21 17:04:35','2016-11-22 15:38:35'),
 (3,'aish','1','a@d.com','',NULL,'e10adc3949ba59abbe56e057f20f883e','profile_1481791838480',123456,'123456',0,'2017-01-05 18:15:07','2016-11-25 17:13:59'),
 (7,'try','1','a@b.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e','profile_1481791838480',NULL,'123456',NULL,'2017-01-05 18:14:57','0000-00-00 00:00:00'),
 (8,'try','2','a@c.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2016-12-23 15:42:39','0000-00-00 00:00:00'),
 (9,'try','3','a@e.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2016-12-23 15:51:37','0000-00-00 00:00:00'),
 (10,'new','regi','new@g.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2016-12-27 16:37:20','0000-00-00 00:00:00'),
 (12,'Aishwarya','Dhopate','aishwarya.dhopate1@anveshak.com',NULL,'','e10adc3949ba59abbe56e057f20f883e','profile_1483618249007',NULL,'1234567',0,'2017-01-17 11:10:17','0000-00-00 00:00:00'),
 (15,'Aish','Gmail','aish.dhopate@gmail.com',NULL,'','e10adc3949ba59abbe56e057f20f883e','profile_1484198314248',NULL,'123456',NULL,'2017-01-12 10:48:36','0000-00-00 00:00:00'),
 (16,'','','',NULL,NULL,'d41d8cd98f00b204e9800998ecf8427e',NULL,NULL,'',NULL,'2017-01-10 10:53:56','0000-00-00 00:00:00'),
 (17,'ddd','d','sis@hd.cjjc',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123',NULL,'2017-01-11 10:07:42','0000-00-00 00:00:00'),
 (18,'Aditi','Madane','a@m.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2017-01-11 10:18:36','0000-00-00 00:00:00'),
 (20,'yss','yhwy','madhuri.dixit@anveshak.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',2,'2017-01-16 12:07:45','0000-00-00 00:00:00'),
 (21,'Aditi','Madane','adityvy@gmail.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2017-01-30 17:02:21','0000-00-00 00:00:00'),
 (22,'Aditi','Madane','aditivy@gmail.com',NULL,'','e10adc3949ba59abbe56e057f20f883e','profile_1486128394366',NULL,'123456',NULL,'2017-02-03 18:56:35','0000-00-00 00:00:00'),
 (24,'Shashak','Thakur','shashank.thakur@anveshak.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2017-01-17 11:03:43','0000-00-00 00:00:00'),
 (29,'A','D','aishwarya.dhopate@anveshak2.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123',NULL,'2017-01-30 15:37:24','0000-00-00 00:00:00'),
 (30,'test','1','test1@gmail.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2017-01-30 18:21:15','0000-00-00 00:00:00'),
 (31,'Sunil','Marne','sunil.marne@anveshak.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2017-01-20 10:20:18','0000-00-00 00:00:00'),
 (32,'Snigdha','kadam','snigdha.kadam@anveshak.com',NULL,NULL,'e10adc3949ba59abbe56e057f20f883e',NULL,NULL,'123456',NULL,'2017-02-04 12:57:05','0000-00-00 00:00:00');
UNLOCK TABLES;
/*!40000 ALTER TABLE `consumer` ENABLE KEYS */;


--
-- Definition of table `community`.`consumer_communities`
--

DROP TABLE IF EXISTS `community`.`consumer_communities`;
CREATE TABLE  `community`.`consumer_communities` (
  `CONSUMER_ID` bigint(20) NOT NULL,
  `COMMUNITY_ID` bigint(20) NOT NULL,
  `ACTIVE_STATUS` int(11) NOT NULL DEFAULT '1',
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `CONSUMER_COMMUNITIES_CONSUMER_ID_idx` (`CONSUMER_ID`),
  KEY `CONSUMER_COMMUNITIES_COMMUNITY_ID_idx` (`COMMUNITY_ID`),
  CONSTRAINT `FK_CONSUMER_COMMUNITIES_COMMUNITY_ID` FOREIGN KEY (`COMMUNITY_ID`) REFERENCES `community` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CONSUMER_COMMUNITIES_CONSUMER_ID` FOREIGN KEY (`CONSUMER_ID`) REFERENCES `consumer` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`consumer_communities`
--

/*!40000 ALTER TABLE `consumer_communities` DISABLE KEYS */;
LOCK TABLES `consumer_communities` WRITE;
INSERT INTO `community`.`consumer_communities` VALUES  (1,1,1,1,'2016-12-22 17:46:48'),
 (2,1,1,2,'2016-12-22 18:24:47'),
 (2,2,1,3,'2016-12-22 18:23:48'),
 (3,1,0,9,'2017-01-10 12:52:17'),
 (8,1,1,10,'2016-12-23 15:42:39'),
 (9,2,1,11,'2017-01-03 15:41:14'),
 (12,1,1,14,'2017-01-10 17:22:13'),
 (15,2,1,17,'2017-01-04 15:23:43'),
 (17,2,1,18,'2017-01-11 10:07:42'),
 (18,1,0,19,'2017-01-11 10:48:09'),
 (20,2,1,21,'2017-01-11 11:14:50'),
 (21,1,1,22,'2017-01-11 11:20:57'),
 (22,1,1,23,'2017-01-16 09:24:25'),
 (24,2,1,25,'2017-01-17 11:03:43'),
 (29,2,1,30,'2017-01-17 11:21:08'),
 (30,3,1,31,'2017-01-17 11:26:46'),
 (31,2,1,32,'2017-01-20 10:20:18'),
 (32,4,1,33,'2017-02-04 12:57:05');
UNLOCK TABLES;
/*!40000 ALTER TABLE `consumer_communities` ENABLE KEYS */;


--
-- Definition of table `community`.`events`
--

DROP TABLE IF EXISTS `community`.`events`;
CREATE TABLE  `community`.`events` (
  `EVENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(5000) DEFAULT NULL,
  `EVENT_DATE` varchar(50) NOT NULL,
  `START_TIME` varchar(50) NOT NULL,
  `END_TIME` varchar(50) NOT NULL,
  `LOCATION` varchar(250) DEFAULT NULL,
  `EVENT_IMAGE` varchar(255) DEFAULT NULL,
  `COMMUNITY_ID` bigint(20) NOT NULL,
  `CONSUMER_ID` bigint(20) NOT NULL,
  `SHARED_FLAG` int(11) NOT NULL DEFAULT '0',
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `eventsrsvp` tinyblob,
  PRIMARY KEY (`EVENT_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `community`.`events`
--

/*!40000 ALTER TABLE `events` DISABLE KEYS */;
LOCK TABLES `events` WRITE;
INSERT INTO `community`.`events` VALUES  (12,'Aishwarya\'s Birthday','Do come for the birthday bash','2017-01-15','07:00 PM','08:25 PM','Pune','event_1483617397591',1,1,1,'2017-01-09 15:06:34',NULL),
 (13,'Annual party','123','2017-02-28','07:07 PM','11:07 PM','Anveshak','',1,1,1,'2017-01-30 14:52:09',NULL),
 (14,'Last testing event','Last testing','2017-02-09','5 : 15 PM','3 : 12 PM','Edubuddy room','event_1486115310109',1,1,0,'2017-02-03 15:19:43',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;


--
-- Definition of table `community`.`events_rsvp`
--

DROP TABLE IF EXISTS `community`.`events_rsvp`;
CREATE TABLE  `community`.`events_rsvp` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EVENT_ID` bigint(20) NOT NULL,
  `CONSUMER_ID` bigint(20) NOT NULL,
  `RSVP` varchar(50) NOT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `FK_lddhun94o4dmoawv86qp8nin8` (`EVENT_ID`),
  CONSTRAINT `FK_lddhun94o4dmoawv86qp8nin8` FOREIGN KEY (`EVENT_ID`) REFERENCES `events` (`EVENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `community`.`events_rsvp`
--

/*!40000 ALTER TABLE `events_rsvp` DISABLE KEYS */;
LOCK TABLES `events_rsvp` WRITE;
INSERT INTO `community`.`events_rsvp` VALUES  (13,12,1,'Going','2017-01-12 10:18:41'),
 (15,13,2,'Maybe','2017-01-18 17:44:08'),
 (16,13,1,'Not Going','2017-02-03 13:55:53');
UNLOCK TABLES;
/*!40000 ALTER TABLE `events_rsvp` ENABLE KEYS */;


--
-- Definition of table `community`.`posts`
--

DROP TABLE IF EXISTS `community`.`posts`;
CREATE TABLE  `community`.`posts` (
  `POST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(5000) DEFAULT NULL,
  `IMAGE` varchar(255) DEFAULT NULL,
  `COMMUNITY_ID` bigint(20) NOT NULL,
  `CONSUMER_ID` bigint(20) NOT NULL,
  `SHARED_FLAG` int(11) NOT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`POST_ID`) USING BTREE,
  KEY `FK_9b69ggdwxvxt4nq784d8jdwi8` (`CONSUMER_ID`),
  CONSTRAINT `FK_9b69ggdwxvxt4nq784d8jdwi8` FOREIGN KEY (`CONSUMER_ID`) REFERENCES `consumer` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `community`.`posts`
--

/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
LOCK TABLES `posts` WRITE;
INSERT INTO `community`.`posts` VALUES  (16,'3456','image_post_1483614351542',1,1,1,'2017-01-05 16:35:53'),
 (17,'Must watch this video','video_post_1483617625244',1,1,1,'2017-01-05 17:30:26'),
 (19,'post','image_post_1484301336066',1,1,0,'2017-01-13 15:25:36'),
 (22,'Check for issues','image_post_1485327516869',1,1,0,'2017-01-25 12:28:36'),
 (26,'Yo','image_post_1486228143379',1,1,0,'2017-02-04 22:39:03'),
 (35,'','image_post_1486229472516',1,1,0,'2017-02-04 23:01:12'),
 (36,'','image_post_1486229479266',1,1,0,'2017-02-04 23:01:19'),
 (37,'','image_post_1486229581507',1,1,0,'2017-02-04 23:03:01'),
 (38,'','image_post_1486229679530',1,1,0,'2017-02-04 23:04:39'),
 (39,'','image_post_1486229697569',1,1,0,'2017-02-04 23:04:57'),
 (40,'','image_post_1486355059929',1,1,0,'2017-02-06 09:54:20'),
 (41,'',NULL,1,1,0,'2017-02-06 15:07:42'),
 (42,'','image_post_1486373914705',1,1,0,'2017-02-06 15:08:34'),
 (43,'','image_post_1486374987698',1,1,0,'2017-02-06 15:26:27'),
 (44,'yo','',1,1,0,'2017-02-06 15:35:34'),
 (45,'','image_post_1486375545988',1,1,0,'2017-02-06 15:35:45'),
 (46,'',NULL,1,1,0,'2017-02-06 21:58:46'),
 (47,'','video_post_1486398624182',1,1,0,'2017-02-06 22:00:24'),
 (48,'','image_post_1486462949452',1,1,0,'2017-02-07 15:52:29'),
 (49,'','image_post_1486463090080',1,1,0,'2017-02-07 15:54:50'),
 (50,'','image_post_1486463159531',1,1,0,'2017-02-07 15:55:59'),
 (51,'',NULL,1,1,0,'2017-02-07 15:56:32'),
 (52,'','video_post_1486463204188',1,1,0,'2017-02-07 15:56:44'),
 (54,'','image_post_1486463602409',1,1,0,'2017-02-07 16:03:22');
UNLOCK TABLES;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;


--
-- Definition of table `community`.`provider`
--

DROP TABLE IF EXISTS `community`.`provider`;
CREATE TABLE  `community`.`provider` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CONSUMER_ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(5000) DEFAULT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PROVIDER_CONSUMER_ID_idx` (`CONSUMER_ID`),
  CONSTRAINT `FK_PROVIDER_CONSUMER_ID` FOREIGN KEY (`CONSUMER_ID`) REFERENCES `consumer` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`provider`
--

/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
LOCK TABLES `provider` WRITE;
INSERT INTO `community`.`provider` VALUES  (13,1,'','2016-11-18 15:00:32'),
 (14,3,'123456','2017-01-05 18:17:13'),
 (15,12,'he Centre will hold a tripartite dialogue with rebel Naga group United Naga Council (UNC) and Manipur government brass here on Friday to explore wa ffffffffffffff','2017-02-03 12:30:42'),
 (20,22,'123ssssssssssss dhhhhh ddddd ddddddddddddddd dddddddddddddddddddddddddddddddd aaaaaaaaaaaaaaa njnnnnnnnnnnnnnnnnnnnnn aaaaaaaaaaaaaaaaaaa','2017-02-03 12:29:35'),
 (21,15,'abc','2017-01-22 14:29:19');
UNLOCK TABLES;
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;


--
-- Definition of table `community`.`provider_activities`
--

DROP TABLE IF EXISTS `community`.`provider_activities`;
CREATE TABLE  `community`.`provider_activities` (
  `PROVIDER_ID` bigint(20) NOT NULL,
  `ACTIVITY_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PROVIDER_ID`,`ACTIVITY_ID`),
  KEY `PROVIDER_ACTIVITIES_PROVIDER_ID_idx` (`PROVIDER_ID`),
  KEY `PROVIDER_ACTIVITIES_ACTIVITY_ID_idx` (`ACTIVITY_ID`),
  CONSTRAINT `FK_PROVIDER_ACTIVITIES_ACTIVITY_ID` FOREIGN KEY (`ACTIVITY_ID`) REFERENCES `activity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PROVIDER_ACTIVITIES_PROVIDER_ID` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `provider` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`provider_activities`
--

/*!40000 ALTER TABLE `provider_activities` DISABLE KEYS */;
LOCK TABLES `provider_activities` WRITE;
INSERT INTO `community`.`provider_activities` VALUES  (13,1),
 (14,1),
 (15,1),
 (15,2),
 (15,3),
 (20,2),
 (20,3),
 (21,2),
 (21,3);
UNLOCK TABLES;
/*!40000 ALTER TABLE `provider_activities` ENABLE KEYS */;


--
-- Definition of table `community`.`role`
--

DROP TABLE IF EXISTS `community`.`role`;
CREATE TABLE  `community`.`role` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ROLE_NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UQ_ROLE_NAME` (`ROLE_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`role`
--

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
LOCK TABLES `role` WRITE;
INSERT INTO `community`.`role` VALUES  (1,'USER','Role for normal user'),
 (2,'ADMIN','Role for admin user'),
 (3,'SYSADMIN','Role for system admin user');
UNLOCK TABLES;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


--
-- Definition of table `community`.`work_request`
--

DROP TABLE IF EXISTS `community`.`work_request`;
CREATE TABLE  `community`.`work_request` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CONSUMER_ID` bigint(20) NOT NULL,
  `ACTIVITY_ID` bigint(20) NOT NULL,
  `REQUEST_DATE` varchar(50) NOT NULL,
  `DESCRIPTION` varchar(5000) DEFAULT NULL,
  `ACTUAL_HOURS` float DEFAULT NULL,
  `ESTIMATED_HOURS` int(11) NOT NULL DEFAULT '0',
  `PROCESSING_STATUS` int(11) DEFAULT '0',
  `ESTIMATED_COST` double DEFAULT '0',
  `PROVIDER_ID` bigint(20) DEFAULT NULL,
  `COMMUNITY_ID` bigint(20) NOT NULL,
  `RATINGS` float DEFAULT NULL,
  `REVIEWS` varchar(250) DEFAULT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `FK_WORK_REQUEST_CONSUMER_ID` (`CONSUMER_ID`),
  KEY `FK_WORK_REQUEST_ACTIVITY_ID` (`ACTIVITY_ID`),
  KEY `FK_WORK_REQUEST_PROVIDER_ID` (`PROVIDER_ID`),
  CONSTRAINT `FK_WORK_REQUEST_ACTIVITY_ID` FOREIGN KEY (`ACTIVITY_ID`) REFERENCES `activity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WORK_REQUEST_CONSUMER_ID` FOREIGN KEY (`CONSUMER_ID`) REFERENCES `consumer` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`work_request`
--

/*!40000 ALTER TABLE `work_request` DISABLE KEYS */;
LOCK TABLES `work_request` WRITE;
INSERT INTO `community`.`work_request` VALUES  (32,22,3,'2017-02-15 11 : 40 AM','check with no helper',NULL,2,1,30,13,1,NULL,NULL,'2017-01-31 14:45:51'),
 (33,22,4,'2017-03-08 2 : 04 PM','aa',NULL,1,1,20,13,1,NULL,NULL,'2017-02-01 14:06:20'),
 (35,12,1,'2017-03-11 2 : 07 PM','dd',NULL,2,0,20,0,1,NULL,NULL,'2017-02-07 14:07:42'),
 (37,1,3,'2017-03-10 3 : 53 PM','abc',NULL,3,1,45,15,1,NULL,NULL,'2017-02-08 15:54:39'),
 (39,1,2,'2017-02-04 3 : 59 PM','abc',4,4,3,20,15,1,NULL,NULL,'2017-02-08 16:04:36'),
 (40,1,2,'2017-03-02 1 : 46 AM','',NULL,4,0,20,0,1,NULL,NULL,'2017-02-10 23:49:10');
UNLOCK TABLES;
/*!40000 ALTER TABLE `work_request` ENABLE KEYS */;


--
-- Definition of table `community`.`work_request_providers`
--

DROP TABLE IF EXISTS `community`.`work_request_providers`;
CREATE TABLE  `community`.`work_request_providers` (
  `WORK_REQUEST_ID` bigint(20) NOT NULL,
  `PROVIDER_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`WORK_REQUEST_ID`,`PROVIDER_ID`),
  KEY `WORK_REQUEST_PROVIDERS_WORK_REQUEST_ID_idx` (`WORK_REQUEST_ID`),
  KEY `WORK_REQUEST_PROVIDERS_PROVIDER_ID_idx` (`PROVIDER_ID`),
  CONSTRAINT `FK_WORK_REQUEST_PROVIDERS_PROVIDER_ID` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `provider` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WORK_REQUEST_PROVIDERS_WORK_REQUEST_ID` FOREIGN KEY (`WORK_REQUEST_ID`) REFERENCES `work_request` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`.`work_request_providers`
--

/*!40000 ALTER TABLE `work_request_providers` DISABLE KEYS */;
LOCK TABLES `work_request_providers` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `work_request_providers` ENABLE KEYS */;


--
-- Definition of procedure `community`.`getWorkRequestsforProvider`
--

DROP PROCEDURE IF EXISTS `community`.`getWorkRequestsforProvider`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE  `community`.`getWorkRequestsforProvider`(IN activityIds TEXT, IN communityIds TEXT)
BEGIN

declare consumer_id_list text;

select group_concat(distinct CONSUMER_ID) INTO consumer_id_list from consumer_communities 
where find_in_set(COMMUNITY_ID, communityIds);

SET @workrequestsquery = CONCAT('select * from work_request where PROCESSING_STATUS = 0 and REQUEST_DATE > now()
and find_in_set(activity_id,\'', activityIds,'\') and find_in_set(consumer_id,\'', consumer_id_list,'\')');

PREPARE workrequeststmt FROM @workrequestsquery;
	
EXECUTE workrequeststmt;	

DEALLOCATE 	PREPARE workrequeststmt;
	

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;