angular.module('app').service('service',
		[ '$http', '$log', '$q', '$window', '$location','$rootScope','Notification',
        function($http, $log, $q, $window, $location,$rootScope, Notification) {

			var config = {
				headers : {
					'Content-Type' : 'application/json;'
				}
			};

			this.post = function(url, data, redirectUrl) {
				try{
					$http.post(url, data, config).success(function(data) {
						if (localStorage.getItem("createUser") == 0) {
							localStorage.setItem("consumerId", data.id);
						} 
						else if (localStorage.getItem("createUser") == 1){
							var redirectTOPath=localStorage.getItem("redirectTOPath");
							 localStorage.clear();
				                $rootScope.provider=null;
				                $rootScope.communityManager=null;
							localStorage.setItem("consumerId", data.consumerId);
							localStorage.setItem("consumerName", data.consumerName);
							this.fetchProvider($rootScope.communityContextPath+"/consumers/"+data.consumerId+"/providers");
						
						}
	
						if (redirectUrl === 'loginRe'){
					
						//	Notification.success('Registration Successful!!');
						}else if (redirectUrl === 'success'){
							localStorage.setItem("regimail",1);
							$("body").removeClass("modal-open");
							//$location.path('/dashboard');
							$window.location.href = '#/dashboard';
						//	Notification.success('Registration Successful!!');
						}else 
							if(redirectUrl=== 'providerReg'){
							var href = data._links.self.href;
							var indexOf = href.lastIndexOf("/");
							var providerId = href.substring(indexOf+1,href.length);
							localStorage.setItem("providerId",providerId);  
							$rootScope.provider=localStorage.getItem("providerId");
							swal("Thanks! You have been successfully registered as a helper");
							$location.path('/dashboard');
						}else if(redirectUrl === 'pricing'){
							$("body").removeClass("modal-open");
						}
						else{
							if(redirectTOPath){
								$("body").removeClass("modal-open");
							//	$location.path('/'+redirectTOPath);
								$window.location.href = '#/'+redirectTOPath;
							}else{
								$("body").removeClass("modal-open");
								//$location.path(redirectUrl);
								$window.location.href = '#'+redirectUrl;
							}
						
						}
					}).error(function(data, status) {
						Notification.error(data.message);
					})
					
				}catch(e){
					alert(e);
				}
			}

		   fetchProvider = function(url) {
				$http.get(url).success(function(data){
					if(data=="" || data==null){
					var consumerID=localStorage.getItem("consumerId");
					$http.get($rootScope.communityContextPath+"/consumers/"+consumerID).success(function(data){
						if(data.managerCommunity){
						localStorage.setItem("communityManager", data.managerCommunity); 
						$rootScope.communityManager=localStorage.getItem("communityManager");
						}
						if(data.communityType=="notactive" || !data.communityType){
							localStorage.setItem("HiddenHelper", ["1"]);  
							$rootScope.hiddenHelper=localStorage.getItem("HiddenHelper");
							if(data.communityType=="notactive"){
								localStorage.setItem("communityActiveStatus","0");
								$rootScope.communityActiveStatus=localStorage.getItem("communityActiveStatus");
							}
							}else{
								var hiddenSection=data.communityType.split(",");
								localStorage.setItem("HiddenHelper", hiddenSection);  
								$rootScope.hiddenHelper=localStorage.getItem("HiddenHelper");
								
						}
						
						return data;
					});
				}else{
					if(data.description=="notactive" || !data.description){
						localStorage.setItem("HiddenHelper", ["1"]);  
						if(data.description=="notactive"){
							localStorage.setItem("communityActiveStatus","0");
							$rootScope.communityActiveStatus=localStorage.getItem("communityActiveStatus");
						}
					}else{
						var hiddenSection=data.description.split(",");
						localStorage.setItem("HiddenHelper", hiddenSection);  					}
					localStorage.setItem("providerId", data.id);  
				
					$rootScope.provider=localStorage.getItem("providerId");
					if(data.consumer.managerCommunity){
					localStorage.setItem("communityManager", data.consumer.managerCommunity); 
					$rootScope.communityManager=localStorage.getItem("communityManager");
					}
					return data;
				}
				}).error(function(data){
					return undefined;
				});
			}

		} ]);