'use strict';

var app = angular.module('app', ['ngRoute', 'ui-notification','ngSanitize']);

// Configuring Routes
app.config(['$httpProvider','$routeProvider', function($httpProvider,$routeProvider) {
	   $httpProvider.defaults.cache = false;
	    if (!$httpProvider.defaults.headers.get) {
	      $httpProvider.defaults.headers.get = {};
	    }
	    // disable IE ajax request caching
	    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
	
	$routeProvider
	.when('/', {
		templateUrl : 'views/homepage.html',
	//	controller : 'SignUpCtrl'
	})
	.when('/dashboard', {
		templateUrl : 'views/dashboard.html',
	//	controller : 'DashboardCtrl'
	}).when('/provider', {
		templateUrl : 'views/provider.html',
	//	controller : 'DashboardCtrl'
	}).when('/registration', {
		templateUrl : 'views/registration.html',
	//	controller : 'RegistrationCtrl'
	}).when('/providerorders', {
		templateUrl : 'views/providerorderdetail.html',
	//	controller : 'orderDetailCtrl'
	}).when('/myProfile', {
		templateUrl : 'views/myProfile.html',
		controller : 'myProfileCtrl'
	}).when('/openRequests', {
		templateUrl : 'views/openRequests.html',
	//	controller : 'openRequestsCtrl'
	}).when('/acceptedRequests', {
		templateUrl : 'views/acceptedRequests.html',
	//	controller : 'acceptedRequestCtrl'
	}).when('/requestHistory', {
		templateUrl : 'views/requestHistory.html',
		//controller : 'requestHistoryCtrl'
	}).when('/myCommunity', {
		templateUrl : 'views/myCommunity.html'
	//	controller : 'myCommunityCtrl'
	}).when('/businesses', {
		templateUrl : 'views/businesses.html'
			//	controller : 'myCommunityCtrl'
	}).when('/aboutus', {
		templateUrl : 'views/aboutus.html'
	}).when('/apartment', {
		templateUrl : 'views/apartment.html'
	}).when('/church', {
		templateUrl : 'views/church.html'
	}).when('/communityNeeds', {
		templateUrl : 'views/needs.html'
	}).when('/communityMembers', {
		templateUrl : 'views/communityMembers.html'
	}).when('/plansandpricing', {
		templateUrl : 'views/plansandpricing.html'
	}).when('/forum', {
		templateUrl : 'views/forum.html'
	}).when('/forumTopic', {
		templateUrl : 'views/forumTopic.html'
	}).when('/signupCommunity', {
		templateUrl : 'views/signupCommunity.html'
	}).when('/communityEvents', {
		templateUrl : 'views/communityEventsCalendar.html'
	}).when('/us/:name*', {
		templateUrl : 'views/communityHomepage.html'
	}).otherwise({
		redirectTo : '/'
	});
}]); // end

app.run(function($rootScope,$location,$window) {
//	Use this on localhost $rootScope.communityPaths="upload/";
	//$rootScope.communityPaths="upload/";
	$rootScope.communityPaths="http://ec2-54-191-202-240.us-west-2.compute.amazonaws.com:8080/upload/"; 
	//$rootScope.communityContextPath='';
	$rootScope.communityContextPath='/communityforus';
	//$rootScope.communityPaths="http://www.communityforus.com/cmmfs/upload/";
	var consumerId=localStorage.getItem("consumerId");
	var path=$location.absUrl();
	var redirectTO=path.split("/").pop();
	if(path.indexOf("?")!= -1){
		var redirectTO=path.split("?").pop();
		localStorage.setItem("referralCode",redirectTO);
		$window.location.href = '#/';
        $window.location.reload();
	}else if(redirectTO=="apartment" && redirectTO=="aboutus" && redirectTO=="church"){
		$window.location.reload();
	}
		/*var redirectTO=path.split("/").pop();
	if(!consumerId && redirectTO!="apartment" && redirectTO!="aboutus"){
		localStorage.setItem("redirectTOPath",redirectTO);
		$location.path('/');
	}
	}else{
		var redirectTO=path.split("?").pop();
		localStorage.setItem("referralCode",redirectTO);
		$location.path('/');
		location.reload();
	} */
});