//Author: Sameer Shukla
//Since it's not a SinglePageApplication, Service and Factories doesn't make any sense in this application. Using it just for resourcing purpose.
//Although a Independent Service can be injected like Validation or Customization
//var app = angular.module('app', []);
angular.module('app').controller('SignUpCtrl', [ '$scope', '$http', '$location', '$rootScope','$window','service','Notification','$timeout', function($scope, $http, $location, $rootScope, $window, service,Notification,$timeout) {
	$window.scrollTo(0, 0);	
	$rootScope.location = $location.url();
	$rootScope.provider=localStorage.getItem("providerId");
	$rootScope.communityManager=localStorage.getItem("communityManager");
	$scope.referralFromStorage=localStorage.getItem("referralCode");
	$scope.communityPaths=$rootScope.communityPaths;
	$scope.selectedPlanName="";
	$('.flexslider').flexslider({
		animation: "slide"
	});
	//Model
	$scope.signup = {"firstName" : "","lastName" : "","emailAddress" : "","password" : "","zipCode" : ""};
	$scope.login = {"username":"", "password":"","enabled": true, "credentialsexpired": false, "expired": false, "locked": false};
	$scope.referralCodes="";
	if(!$scope.referralFromStorage){
		$http.get($rootScope.communityContextPath+"/communities").success(function(data){
			$scope.referralCodes=data;
		});
	}

	//Create User Block
	$scope.createUser = function() {
		if($scope.signup.password.length<6 || $scope.signup.password.length>25){
			Notification.error({message:"The password length should be between 6 to 25 characters",replaceMessage: true});
			return false;
		}
		localStorage.setItem("createUser", 0);
		if($scope.referralFromStorage){
			var referralVal=$scope.referralFromStorage;
		}else{
			var referralVal=$scope.referralCodes.referralCode;
		}
		service.post($rootScope.communityContextPath+"/consumers/register/"+referralVal, $scope.signup,'success');
	}
	//Create User Block
	$scope.createUserInPricing = function() {
		localStorage.setItem("createUser", 0);
		if($scope.referralFromStorage){
			var referralVal=$scope.referralFromStorage;
		}else{
			var referralVal=$scope.referralCodes.referralCode;
		}
		service.post($rootScope.communityContextPath+"/consumers/register/"+referralVal, $scope.signup,'pricing');
	}

	//Login 
	$scope.validateLogin = function(){
		localStorage.setItem("createUser", 1);
		service.post($rootScope.communityContextPath+"/login", $scope.login,'/dashboard');
	}

	//Handle Enter key on login
	$scope.DoWork = function(){
		this.validateLogin();
	}

	//Handle enter key event on Signup
	$scope.DoWorkCreateUser = function(){
		this.createUser();
	}
	//Handle enter key event on Signup
	$scope.DoWorkCreateUserInPricing = function(){
		this.createUserInPricing();
	}


	$scope.homepage=function(){
		$window.location.href = '#/';
		$window.location.reload();
	}

	$scope.message="";
	$scope.email="";

	$scope.contactUsEmail=function(){
		if($scope.email==""){
			Notification.error({message:"Email field is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.email){
			Notification.error({message:"Please enter the valid email",replaceMessage: true});
			return false;
		}
		if(!$scope.message){
			Notification.error({message:"Message field is mandatory",replaceMessage: true});
			return false;
		}

		$http.post($rootScope.communityContextPath+"/consumers/contactUs/"+$scope.message,$scope.email).success(function(data){
			swal("Thank you for contacting us!!");
			$scope.message="";
			$scope.email="";
		});
	}

	$scope.communityTypes=["Church","Apartment","Neighborhood","Other"];

	$scope.contactForm={}

	$scope.contactFormSubmit=function(){
		$scope.loading=true;
		$http.post($rootScope.communityContextPath+"/consumers/contactUsForm",$scope.contactForm).success(function(data){
			$scope.loading=false; 
			swal("Thank you for contacting us!!");
			if($scope.selectedPlanName=="Basic"){
				$scope.closeCurrentModal();
				$("#popup-basicPlan").modal("show");
			}else if($scope.selectedPlanName=="Premium"){
				$scope.closeCurrentModal();
				$("#popup-premiumPlan").modal("show");
			}else{
				$scope.closeCurrentModal();
			}
			angular.copy({},$scope.contactForm);
			
		});
	}


	$scope.checkForClassChanges=function(){
		if ($('#slider1').hasClass('flex-active-slide')){
			$(".hero").attr('id','herosection1');
		}else if ($('#slider2').hasClass('flex-active-slide')){
			$(".hero").attr('id','herosection2');
		}else if ($('#slider3').hasClass('flex-active-slide')){
			$(".hero").attr('id','herosection3');
		}
		var a=$timeout(function(){
			$scope.checkForClassChanges()
		}, 1000);
		if($rootScope.location!="/"){
			$timeout.cancel(a); }
	}
	if($rootScope.location=="/"){
		$timeout(function() {
		$scope.checkForClassChanges();
		},1500);
	}

	/* When user clicks the Icon */
	$('.nav-toggle').click(function() {
		$(this).toggleClass('active');
		$('.header-nav').toggleClass('open');
		event.preventDefault();
	});
	/* When user clicks a link */
	$('.header-nav li a').click(function() {
		$('.nav-toggle').toggleClass('active');
		$('.header-nav').toggleClass('open');

	});

//	Smooth Scrolling 
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 2000);
				return false;
			}
		}
	});

	$scope.navigatetopricing=function(){
		$window.location.href = '#/plansandpricing';
	}

	$scope.selectedPlan=function(plan){
		$scope.selectedPlanName=plan;
	}
	$scope.closeCurrentModal=function(){
		$(".modal").modal("hide");
	}
	
	$scope.forgotPasswordModal=function(){
		$(".modal").modal("hide");
		$("#popup-forgotPassword").modal();
	}
	$scope.forgotpasswordEmail="";
	$scope.forgotPassword=function(){
		if($scope.forgotpasswordEmail==""){
			Notification.error({message:"Email Address field is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.forgotpasswordEmail){
			Notification.error({message:"Invalid Email Address",replaceMessage: true});
			return false;
		}
		$http.post($rootScope.communityContextPath+"/consumers/forgotPassword/",$scope.forgotpasswordEmail).success(function(data) { 								
			if(data==0){
				Notification.error({message:"Consumer with this email address does not exist in the system",replaceMessage: true});
			}else{			
			swal("The password has been sent to your email address");
			$(".modal").modal("hide");
			$scope.forgotpasswordEmail="";
		}
		});
	}
} ]);

