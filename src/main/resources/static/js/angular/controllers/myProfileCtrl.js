//File created by Aishwarya on 14th dec 2016
angular.module('app').controller('myProfileCtrl',[
						'$scope',
						'$http',
						'$location',
						'$rootScope',
						'$timeout',
						'$window',
						'Notification',
						function($scope, $http, $location, $rootScope,$timeout,$window,Notification) {
							var consumerId = localStorage.getItem("consumerId");
							var providerId = localStorage.getItem("providerId");
							$scope.communityActiveStatus=localStorage.getItem("communityActiveStatus");
							$scope.providerId=providerId;
							$scope.consumersProfileInfo="";
							$scope.loading=false;
						    $scope.myCroppedImage=null;
						    $scope.$watch('myCroppedImage', function(newVal) {
					            if (newVal) {}
					          });
						    $scope.myProfileImage='';
						    $scope.showImageCropper = true;
						    $scope.resImgSize = [{w: 300, h: 300}];
							$scope.getProfileInfoOfConsumer=function(){
								$scope.loading=true;
								$http.get($rootScope.communityContextPath+"/consumers/"+consumerId).success(function(data){
									$scope.loading=false;
									if(data.profilePic){
									$scope.myProfileImage=$rootScope.communityPaths+'profile_pics/'+data.profilePic;
									}else{
										$scope.myProfileImage="images/Commun-ity_03.png";
									}
									$scope.consumersProfileInfo=data;
								}).error(function(data){
								 Notification.error(data.message);
							});
								if ($(window).width() <= 992) {
									$(".tabHeightLeft").css("min-height",0);
								} else {
								$(".tabHeightLeft").css("min-height","310px");
								}
							}
							$scope.getProfileInfoOfConsumer();
							$scope.showGeneralInfoForm=function(){
								if ($(window).width() <= 992) {
									$(".tabHeightLeft").css("min-height",0);
								} else {
								$(".tabHeightLeft").css("min-height","310px");
								}
							$("#displayGeneralInformation").hide();
							$("#editGeneralInformation").show();
							}
							 $scope.myImage='';
							    $scope.myCroppedImage='';
							    $scope.dataURItoBlob=function(dataURI) {
							        // convert base64 to raw binary data held in a string
							        var byteString = atob(dataURI.split(',')[1]);

							        // separate out the mime component
							        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

							        // write the bytes of the string to an ArrayBuffer
							        var arrayBuffer = new ArrayBuffer(byteString.length);
							        var _ia = new Uint8Array(arrayBuffer);
							        for (var i = 0; i < byteString.length; i++) {
							            _ia[i] = byteString.charCodeAt(i);
							        }

							        var dataView = new DataView(arrayBuffer);
							        var blob = new Blob([dataView], { type: mimeString });
							        return blob;
							    }
							$scope.submitGenerealInfo = function(){
						            var now = new Date();
						            if($scope.myProfileImage==$scope.myCroppedImage){
						            var imageName=$scope.dataURItoBlob($scope.myProfileImage);
						            }else{
						            	var imageName=null;
						            }
									 if(!$scope.consumersProfileInfo.emailAddress){
							    		 Notification.error({message:"Invalid email address",replaceMessage: true});
							    		 return false;
							    	 }
									 if($("#profpwd").hasClass("profpwd")){
									 if($scope.consumersProfileInfo.password.length<6 || $scope.consumersProfileInfo.password.length>25){
										    Notification.error({message:"The password length should be between 6 to 25 characters",replaceMessage: true});
								    		return false;
									 }
									 }
									$scope.loading=true;
									if(imageName){
									 var oldProfPic=$scope.consumersProfileInfo.profilePic;
									var imageName1="profile_"+now.getTime();
									$scope.consumersProfileInfo.profilePic=imageName1;
									}
										$http.post($rootScope.communityContextPath+"/consumers/"+ consumerId,$scope.consumersProfileInfo).success(function(data) { 								
											if(imageName){
												var fd = new FormData();    
												fd.append('uploadfile',imageName);
												fd.append('uploadfilename',imageName1);

												var folderName="profile_pics";
											$.ajax({
												url: $rootScope.communityContextPath+"/uploadProfileFile/"+oldProfPic,
												type: "POST",
												data: fd,
												contentType:false,
												cache: false,
												processData:false,
												success: function(data){ 
													$scope.loading=false;
										            $window.location.reload();
												}
																			});
											}else{
									             $window.location.reload();
											}
										});
							}
							

							    $scope.handleFileSelect=function(evt) {
							      var reader = new FileReader();
							      reader.onload = function (evt) {
							        $scope.$apply(function($scope){
							          $scope.myImage=evt.target.result;
							        });
							      };
							      reader.readAsDataURL(evt.files[0]);
							    };

							$scope.changeProfImg = function(input) {
							     	var reader = new FileReader();
                                    reader.onload = function(e) {
										$('#profimg').attr('src', e.target.result);
									}
                                    reader.readAsDataURL(input.files[0]);
							}

							$("#proffile").change(function() {
							//	$scope.changeProfImg(this);
								$("#popup-cropImage").modal("show");
							//	$scope.handleFileSelect(this);
							});
							$scope.showBrowseDialog=function(){
								//$("#proffile").click();
								$("#popup-cropImage").modal("show");
							}

						$scope.community="";
						$scope.communities=function(tabid){
							$scope.addActiveToTabs(tabid,'120');
							$http.get($rootScope.communityContextPath+"/consumers/community/"+ consumerId).success(function(data) {
								$scope.community=data;
								
								});
						}
						
						$scope.helperProfileInfo="";
						$scope.showhelperProfile=function(tabid){
							$scope.addActiveToTabs(tabid,'300');
							$http.get($rootScope.communityContextPath+"/consumers/"+ consumerId+"/providers/"+providerId+"/provider").success(function(data) {
					    	$scope.helperProfileInfo=data;	
								});
						}
					
						$scope.displayActivities="";
						$scope.getActivities=function(){
						$http.get($rootScope.communityContextPath+"/activities").success(function(data){
							$scope.displayActivities=data;
							if($scope.displayActivities){
								$timeout(function(){
							$scope.helperProfileInfo.activities.forEach(function(a){
								$scope.select(a.id);
								});
								},300);
							}
						
						}).error(function(data){
							 Notification.error(data.message);
						});
						}
						$scope.showHelperInfoForm=function(){
							if ($(window).width() <= 992) {
								$(".tabHeightLeft").css("min-height",0);
							} else {
							$(".tabHeightLeft").css("min-height","200px");
							}
						$("#displayHelperInformation").hide();
						$("#editHelperInformation").show();
						$timeout(function(){
						$scope.getActivities();
						},200)
						}
						$scope.selected=[];
		
						$scope.select = function(index) {	
							var pushedInd=$scope.selected.indexOf(index);						
							if(pushedInd=== -1){
								$scope.selected.push(index);
								if(index==1){
									$("#"+index).attr("src","images/homeselected.png");
								}else if(index==2){
									$("#"+index).attr("src","images/petsselected.png");
								}else if(index==3){
									$("#"+index).attr("src","images/cartselected.png");
								}else{
									$("#"+index).attr("src","images/yardworkselected.png");
								}
							}else{
								$scope.selected.splice(pushedInd,1);
								if(index==1){
									$("#"+index).attr("src","images/homelogo.png");
								}else if(index==2){
									$("#"+index).attr("src","images/pets.png");
								}else if(index==3){
									$("#"+index).attr("src","images/cart.png");
								}else{
									$("#"+index).attr("src","images/yardworklogo.png");
								}
							}
					
					}
						
						$scope.submitHelperInfo=function(){
							$scope.activities = {};				
							$scope.helperProfileInfo.activities=[];
							$scope.selected.forEach(function(a){				
							var ind=a-1;
								$scope.activities = {
									"id" : a,
									"activityName" : $scope.displayActivities[ind].activityName,
									"description" : $scope.displayActivities[ind].description
								};
							
								$scope.helperProfileInfo.activities
										.push($scope.activities);
							});
							if($scope.helperProfileInfo.activities.length<=0){
								Notification.error({message:"Please select atleast 1 help type",replaceMessage: true});
								return false;
							}
							$http.post($rootScope.communityContextPath+"/consumers/"
									+ consumerId + "/providers/"+providerId+"/updateProvider",
									$scope.helperProfileInfo).success(function(data) {
										$scope.helperProfileInfo=data;
										$("#displayHelperInformation").show();
                                        $("#editHelperInformation").hide();
									})
						
						}
						
						
						
						$scope.addActiveToTabs=function(tabid,heights){
						    $('.tab').removeClass('active');
						    $("#"+tabid+"tab").addClass('active');
						    $('.tab-content').removeClass('active');
						    $("#"+tabid).addClass('tab-content active');
							if ($(window).width() <= 992) {
								$(".tabHeightLeft").css("min-height",0);
							} else {
								var tabsHeights=heights-120;
								$(".tabHeightLeft").css("min-height",tabsHeights);
							}
					}
						
						$scope.uploadCroppedImage=function(){
							$("#popup-cropImage").modal("hide");
							$scope.myProfileImage=$scope.myCroppedImage;
						}
						
						} ]);


//angular.module('app').directive('datetimepicker', function() {
//    return function($scope, element) {
//      element.datetimepicker({
//    	  format: "YYYY-MM-DD"
//      });
//    };
//});
