//Author: Aishwarya Dhopate
angular.module('app').controller('openRequestsCtrl', [ '$scope', '$http', '$location', '$rootScope','$window','service', function($scope, $http, $location, $rootScope, $window, service) {
	var consumerId = localStorage.getItem("consumerId");
	var providerId = localStorage.getItem("providerId");
	$scope.openrequests=[];
	$scope.consumerName=localStorage.getItem("consumerName");
	$scope.timeFinal=[];
	$scope.getOpenRequests=function(){
		$http.get($rootScope.communityContextPath+"/workrequests/consumers/"+consumerId+"/"+providerId).success(
						function(data) {
							for ( var index in data) {
								var json = data[index];
								//if (json.requestDate > new Date()) {
								//	var formatDate = new Date(json.requestDate);
									//json.requestDate = formatDate;
								$scope.dateFinal=json.requestDate.split(" ");
								json.requestDate=$scope.dateFinal[0];
								var time=$scope.dateFinal[1]+$scope.dateFinal[2]+$scope.dateFinal[3]+" "+$scope.dateFinal[4];
								$scope.timeFinal.push(time);
								$scope.openrequests.push(json);
								//}
							}
						}).error(function(data) {
					console.log(JSON.stringify(data))
				});
}
	
	$scope.getOpenRequests();
	$scope.timeOfReq="";
$scope.getWorkRequestDetails=function(event){
	$scope.openrequestsByReqid=[];
	var workRequestId=event.target.id;
	$http.get($rootScope.communityContextPath+"/workrequests/"+workRequestId).success(
			function(data) {
			//	if (data.requestDate > new Date()) {
				$scope.dateOfReq=data.requestDate.split(" ");
				data.requestDate=$scope.dateOfReq[0];
				$scope.timeOfReq=$scope.dateOfReq[1]+$scope.dateOfReq[2]+$scope.dateOfReq[3]+" "+$scope.dateOfReq[4];
                $scope.openrequestsByReqid.push(data);
			//	}
			}).error(function(data) {
		console.log(JSON.stringify(data))
	});
}
	$scope.closeCurrentModal=function(){
		$("#popup-request-details").modal("hide");
	}
	  
	$scope.acceptRequests=function(value){
		  var workRequestID=value;
		  $scope.Procstatus="accept";
		$http.post($rootScope.communityContextPath+"/workrequests/"+workRequestID+"/"+providerId+"/",$scope.Procstatus).success(function(data) {
			$scope.openrequests=[];
			$scope.getOpenRequests();
			swal("Accepted!", "The work request has been accepted successfully");
			var functionName="openReq";
			$http.post($rootScope.communityContextPath+"/workrequests/reqMailSending/"+workRequestID+"/"+providerId+"/"+$scope.consumerName+"/"+functionName).success(function(data){});
	});
	}
	$scope.currentDeclinedReq={}
	$scope.declineRequests=function(value){
		var workid=$scope.openrequests[value].id;
		$http.post($rootScope.communityContextPath+"/workrequests/"+workid+"/"+"declineReq").success(function(data) {
			$scope.currentDeclinedReq.activity=$scope.openrequests[value].activity;
			$scope.currentDeclinedReq.consumer=$scope.openrequests[value].consumer;
			$scope.openrequests=[];
			swal("Declined!", "The work request has been declined");
			$scope.getOpenRequests();
			$http.post($rootScope.communityContextPath+"/workrequests/declineReqMailSend/"+providerId+"/"+$scope.consumerName,$scope.currentDeclinedReq).success(function(data){});
	});
	}
} ]);
