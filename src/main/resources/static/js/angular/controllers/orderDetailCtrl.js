//Since it's not a SinglePageApplication, Service and Factories doesn't make any sense in this application. Using it just for resourcing purpose.
//Although a Independent Service can be injected like Validation or Customization
//Changes in a file by Aishwarya on 16th dec 2016
angular.module('app').controller('orderDetailCtrl', [ '$scope', '$http', '$location', '$rootScope','$window','$timeout','service','Notification', function($scope, $http, $location, $rootScope, $window,$timeout, service,Notification) {
	        $rootScope.location = $location.url();
			var consumerId = localStorage.getItem("consumerId");
		    $rootScope.$emit("CallCommImagesMethod", {consumer:consumerId});
			var providerId = localStorage.getItem("providerId");
			$scope.consumerId=consumerId;
			$scope.tabCount=0;
			$scope.orders = [];
			$scope.timeFinal=[];
	
			//Fetch My Orders on Load
			$http.get($rootScope.communityContextPath+"/workrequests/consumer/"+consumerId).success(function(data){
				for(var index in data){
					var json = data[index];
					$scope.dateFinal=json.requestDate.split(" ");
					json.requestDate=$scope.dateFinal[0];
					var time=$scope.dateFinal[1]+$scope.dateFinal[2]+$scope.dateFinal[3]+" "+$scope.dateFinal[4];
					$scope.timeFinal.push(time);
					if(index==0 && json.chatCount>0){
						$scope.markMsgsAsRead(json.id,consumerId);
						json.chatCount=0;
					}
						$scope.orders.push(json);
						$scope.tabCount++;
				}
				if ($(window).width() <= 992) {
					$(".tabHeightLeft").css("min-height",0);
				} else {
					var tabsHeights=$(".tabs-container").height()-($scope.tabCount*60);
					$(".tabHeightLeft").css("min-height",tabsHeights);
				}
			}).error(function(data){
				 Notification.error(data.message);
			})
			$scope.myOrderInfo={}
			$scope.editMyorder=function(id,index){
				    var date=$("#requestDate").val();
				    var time=$("#requestTime").val();
			     	$scope.myOrderInfo.requestDate=date+" "+time;
					$scope.myOrderInfo.description=$scope.orders[index].description;
					$scope.myOrderInfo.estimatedHours=$scope.orders[index].estimatedHours;
					$scope.myOrderInfo.activity=$scope.orders[index].activity;
					$scope.myOrderInfo.estimatedCost=$scope.orders[index].estimatedCost;
			$http.post($rootScope.communityContextPath+"/workrequests/myorders/"+id,$scope.myOrderInfo).success(function(data) {
				});
			}
		
			$scope.chat={
					senderID:consumerId,
					chatContent:""
			}
			$scope.submitMessage=function(receiver,workId){
				if($scope.chat.chatContent==""){
					Notification.error({message:"Message cannot be empty",replaceMessage: true});
					return false;
				}
				$scope.chat.receiverID=receiver;
				$scope.chat.workRequestId=workId;
				var i=$("#currentindex").val();
				$http.post($rootScope.communityContextPath+"/chat/save",$scope.chat).success(function(data) {
					$scope.chat.chatContent="";
					$scope.showChat($scope.chat.workRequestId);
			    	}).error(function(data){
						 Notification.error(data.message);
					});
			}

			$scope.showChat=function(workId){
				$http.get($rootScope.communityContextPath+"/chat/"+workId).success(function(data){
				$scope.myArray=[];
				var i=$('.tab-content.active').attr('id');
				i=i-1;	
					for(var index in data){
						var json = data[index];
						$scope.orders[i].chat=[];
					    $scope.myArray.push(json);
					}
					$scope.orders[i].chat.toString();
					$scope.orders[i].chat=$scope.myArray;
					if($scope.chatLength!=$scope.orders[i].chat.length){
					var height = 0;
					$('.chat-with-helper-req div').each(function(i, value){
					    height += parseInt($(this).height());
					});

					height += '';
                    $(".chat-with-helper-req").scrollTop(height);
					}
				}).error(function(data){
					 Notification.error(data.message);
				});	
			}
			$scope.markMsgsAsRead=function(workid,consumerid){
				$http.post($rootScope.communityContextPath+"/chat/markAsRead/"+workid+"/"+consumerid).success(function(data) {
			    	}).error(function(data){
						 Notification.error(data.message);
					});
			}
			$scope.addActiveToTabs=function(element){
				 var _self = $(this);
				    $('.tab').removeClass('active');
				    $("#"+element.id).addClass('active');
				    var ids=element.id.split("_").pop();
				    $('.tab-content').removeClass('active');
				    $("#"+ids).addClass('tab-content active');
				    $scope.chat.chatContent="";
				    var ind=ids-1;
					if($scope.orders[ind].chatCount>0){
					$scope.markMsgsAsRead($scope.orders[ind].id,consumerId);
					$scope.orders[ind].chatCount=0;
					}
			}
			var interval=setInterval(function () {
				if($scope.orders.length>0){
				var ind=$('.tab-content.active').attr('id');
				ind=ind-1;	
					 if($scope.orders[ind].id){
						 $scope.chatLength=$scope.orders[ind].chat.length;
					$scope.showChat($scope.orders[ind].id);}
				}
				    }, 2000);
			$scope.$on('$destroy', function(){
				clearInterval(interval);
			});
			
} ]);

angular.module('app').directive('timepicker', function() {
    return function($scope, element) {
      element.wickedpicker({now:"12:00"}); 
    };
});