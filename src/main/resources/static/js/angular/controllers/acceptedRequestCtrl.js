//Author: Aishwarya Dhopate
angular.module('app').controller('acceptedRequestCtrl', [ '$scope', '$http', '$location', '$rootScope','service','Notification','$interval', function($scope, $http, $location, $rootScope, service, Notification,$interval) {
	$rootScope.location = $location.url();	
        	var providerId = localStorage.getItem("providerId");
			var consumerId = localStorage.getItem("consumerId");
			
            $rootScope.$emit("CallCommImagesMethod", {consumer:consumerId});
			$scope.consumerId=consumerId;
			$scope.orders = [];
			$scope.receiversId="";
			$scope.timeFinal=[];
			$scope.tabCount=0;

			//Fetch work requests mapped by providerId on Load
			$http.get($rootScope.communityContextPath+"/workrequests/provider/"+consumerId+"/"+providerId).success(function(data){
				for(var index in data){
					var json = data[index];	
					$scope.dateFinal=json.requestDate.split(" ");
					json.requestDate=$scope.dateFinal[0];
					var time=$scope.dateFinal[1]+$scope.dateFinal[2]+$scope.dateFinal[3]+" "+$scope.dateFinal[4];
					$scope.timeFinal.push(time);
					if(index==0 && json.chatCount>0){
						$scope.markMsgsAsRead(json.id,consumerId);
						json.chatCount=0;
					}
					
					$scope.orders.push(json);
					$scope.tabCount++;
				}
				if ($(window).width() <= 992) {
					$(".tabHeightLeft").css("min-height",0);
				} else {
					var tabsHeights=$(".tabs-container").height()-($scope.tabCount*60);
					$(".tabHeightLeft").css("min-height",tabsHeights);
				}
		
				
			}).error(function(data){
				 Notification.error(data.message);
			})
//			$scope.tabselected=function(order,id){
//				$scope.chat.chatContent="";
//				$scope.selected=order;
//				if($scope.orders[order].chatCount>0){
//				$scope.markMsgsAsRead($scope.orders[order].id,consumerId);
//				$scope.orders[order].chatCount=0;
//				}
//			}
			$scope.chat={
					senderID:consumerId,
					chatContent:""
			}
			$scope.submitMessage=function(receiver,workId){
				if($scope.chat.chatContent==""){
					Notification.error({message:"Message cannot be empty",replaceMessage: true});
					return false;
				}
				$scope.chat.receiverID=receiver;
				$scope.chat.workRequestId=workId;
				var i=$("#currentindex").val();
				$http.post($rootScope.communityContextPath+"/chat/save",$scope.chat).success(function(data) {
					$scope.chat.chatContent="";
					$scope.showChat($scope.chat.workRequestId);
			    	}).error(function(data){
						 Notification.error(data.message);
					});
			}

			$scope.showChat=function(workId){
				$http.get($rootScope.communityContextPath+"/chat/"+workId).success(function(data){
				$scope.myArray=[];
						var i=$('.tab-content.active').attr('id');
						i=i-1;			
					for(var index in data){
						var json = data[index];
						$scope.orders[i].chat=[];
					    $scope.myArray.push(json);
					}
					$scope.orders[i].chat.toString();
					$scope.orders[i].chat=$scope.myArray;
					if($scope.chatLength!=$scope.orders[i].chat.length){
					var height = 0;
					$('.chat-with-helper-req div').each(function(i, value){
					    height += parseInt($(this).height());
					});

					height += '';
                    $(".chat-with-helper-req").scrollTop(height);
					}
				}).error(function(data){
					 Notification.error(data.message);
				});	
			}
			$scope.markMsgsAsRead=function(workid,consumerid){
				$http.post($rootScope.communityContextPath+"/chat/markAsRead/"+workid+"/"+consumerid).success(function(data) {
			    	}).error(function(data){
						 Notification.error(data.message);
					});
			}
		var interval=setInterval(function () {
			if($scope.orders.length>0){
			var ind=$('.tab-content.active').attr('id');
			ind=ind-1;	
				 if($scope.orders[ind].id){
					 $scope.chatLength=$scope.orders[ind].chat.length;
$scope.showChat($scope.orders[ind].id); }
			}
			    }, 2000);
	
			$scope.$on('$destroy', function(){
				clearInterval(interval);
			});
			
			$scope.addActiveToTabs=function(element){
				 var _self = $(this);
				    $('.tab').removeClass('active');
				    $("#"+element.id).addClass('active');
				    var ids=element.id.split("_").pop();
				    $('.tab-content').removeClass('active');
				    $("#"+ids).addClass('tab-content active');
				    $scope.chat.chatContent="";
				    var ind=ids-1;
					if($scope.orders[ind].chatCount>0){
					$scope.markMsgsAsRead($scope.orders[ind].id,consumerId);
					$scope.orders[ind].chatCount=0;
					}
			}
} ]);