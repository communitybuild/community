//Author: Aishwarya Dhopate
angular.module('app').controller('forumCtrl',
		[       '$scope',
		        '$window',
		        '$http',
		        'Notification',
		        '$rootScope',
		        '$location',
		        '$timeout',
		        function($scope, $window,$http,Notification,$rootScope,$location,$timeout) {
			var consumerId=localStorage.getItem("consumerId");
			$scope.consumerId=consumerId;
			$scope.forumTopicList=[];
			$scope.loading1=false;
			$scope.listofForumTopics=function(){
				$http.get(
						$rootScope.communityContextPath
						+ "/communityforum/"
						+ $scope.consumerId).success(
								function(data) {
									if(data){
										for(var index in data){
											var json = data[index];
											var today = new Date();
											var Christmas = new Date(json.createDate);	
											var diffMs = (today-Christmas); // milliseconds between now & Christmas
											var diffDays = Math.floor(diffMs / 86400000); // days
											var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
											var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
											if(diffDays>1){
												json.createDate=diffDays+" days";
											}else if(diffDays==1){
												json.createDate=diffDays+" day";
											}else if(diffDays<1 && diffHrs>1){
												json.createDate=diffHrs+" hours "+diffMins+" minutes";
											}else if(diffDays<1 && diffHrs==1){
												json.createDate=diffHrs+" hour "+diffMins+" minutes";
											}else if(diffDays<1 && diffHrs<1){
												json.createDate=diffMins+" minutes";
											}
											if(data.length==Number(index)+1){
												$scope.forumTopicList=data;
											}
										}}else{
											$scope.forumTopicList=data;
										}

								})
			}
			$scope.listofForumTopics();	

			$scope.topic={};
			$scope.createForumTopic=function(){
				$scope.loading1=true;
				var imageName=document.getElementById("proffile").files[0];
				var now = new Date();
				if(imageName){
					var imageName1="forum_"+now.getTime();
					$scope.topic.topicAttachment=imageName1;
				}
				if(!$scope.topic.topicTitle){
					$scope.loading1=false;
					Notification.error({message:"Title field is mandatory",replaceMessage: true});
					return false;
				}
				if(!$scope.topic.topicDescription && !imageName){
					$scope.loading1=false;
					Notification.error({message:"Please give us more information about this topic",replaceMessage: true});
					return false;
				}
				$http.post($rootScope.communityContextPath+"/communityforum/"+$scope.consumerId,$scope.topic).success(function(data) {
					if(imageName){
						var fd = new FormData();    
						fd.append('uploadfile',imageName);
						fd.append('uploadfilename',imageName1);
						var folderName="forum_images";
						$.ajax({
							url: $rootScope.communityContextPath+"/uploadFile/"+folderName,
							type: "POST",
							data: fd,
							contentType:false,
							cache: false,
							processData:false,
							success: function(data){
								$scope.loading1=false;
								$scope.$apply();
								$scope.forumTopicList=[];
								$scope.listofForumTopics();
								$("#createForumTopic").modal("hide");
								$scope.$apply();
								angular.copy({},$scope.topic);
								$("#linkedForumImg").text("");
							}
						});	
					}else{
						$scope.loading1=false;
						$scope.forumTopicList=[];
						$scope.listofForumTopics();
						$("#createForumTopic").modal("hide");
						angular.copy({},$scope.topic);

					}

				}).error(function(data, status) {
					console.log(JSON.stringify(data))
					Notification.error(data.message);
				});
			}

			$scope.showBrowseDialog=function(){
				$("#proffile").click();
			}

			$("#proffile").change(function(e) {
				$("#linkedForumImg").text(e.target.files[0].name);
			});

			$scope.openTopicPage=function(topicId){
				localStorage.setItem("topicId",topicId);
				$window.location.href = '#/forumTopic';
				$window.location.reload();
			}

			var interval=setInterval(function () {
				$scope.listofForumTopics();
			}, 10000);
			$scope.$on('$destroy', function(){
				clearInterval(interval);
			});
			

		} ]);
