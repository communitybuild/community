//Author: Aishwarya Dhopate
angular.module('app').controller('apartmentCtrl',
				[       '$scope',
						'$window',
						'$http',
						'$rootScope',
						function($scope, $window,$http,$rootScope) {
					$scope.loading=false;
					   $scope.homePage=function(){
							 $window.location.href = '#/';
				             $window.location.reload();
						}
					
					//Following are the js functions from template
					 $("#owl-demo").owlCarousel({
					        autoPlay: 3000,
					        items: 1, //10 items above 1000px browser width
					        itemsDesktop: [1370, 1], //5 items between 1000px and 901px
					        itemsDesktopSmall: [900, 1], // betweem 900px and 601px
					        itemsTablet: [600, 1], //2 items between 600 and 0
					    });
					 
					 setInterval(function() {
                            var widnowHeight = $(window).height();
					        var containerHeight = $(".home-container").height();
					        var padTop = widnowHeight - containerHeight;
					        $(".home-container").css({
					            'padding-top': Math.round(padTop / 2) + 'px',
					            'padding-bottom': Math.round(padTop / 2) + 'px'
					        });
					    }, 10);
					 
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
					    $(window).scroll(function() {
					        "use strict";
					        
					        if ($(window).scrollTop() > 80) {
					            $(".navbar").css({
					                'margin-top': '0px',
					                'opacity': '1',
					                'border-bottom':'1px solid #ccc'
					            })
					            $(".navbar-nav>li>a").css({
					                'padding-top': '15px',
					                'font-size':'small'
					            });
					            $(".navbar-brand img").css({
					                'height': '100%',
					                'margin-top':'15px',
					                'padding-top':'0'
					            });
					            $(".navbar-default").css({
					                'background-color': '#fff'
					            });
					        	if ($(window).width() <= 992) {
					        	    $(".navbar-brand img").css({
					                    'height': '100%',
					                    'margin-top':'5px',
					                    'padding-top':'0'
					                });
								} 
					        } else {
					            $(".navbar").css({
					                'margin-top': '-100px',
					                'opacity': '0'
					            })
					            $(".navbar-nav>li>a").css({
					                'padding-top': '45px'
					            });
					            $(".navbar-brand img").css({
					                'height': '45px'
					            });
					            $(".navbar-brand img").css({
					                'padding-top': '20px'
					            });
					            $(".navbar-default").css({
					                'background-color': 'rgba(59, 59, 59, 0)'
					            });
					        }
					    });
					    
					    $scope.communityTypes=["Church","Apartment","Neighborhood","Other"];
					    
					    $scope.contactForm={}
					    
					    $scope.contactFormSubmit=function(){
					    	$scope.loading=true;
					    	 $http.post($rootScope.communityContextPath+"/consumers/contactUsForm",$scope.contactForm).success(function(data){
					    		 $scope.loading=false; 
					    		 swal("Thank you for contacting us!!");
							  });
					    }
					 
						} ]);
//angular.module('app').directive('loadDirective', function($rootScope) {
//	  return {
//	    restrict: 'E',
//	    link: function($scope, $el,attr) {
//	    	if($rootScope.location=="/apartment"){
//	    	if(attr.id=="apartmentScript"){
//	    		$("#apartmentScript").append('<script id="innerDiv" src="js/apartment.js"></script>');
//	    	}
//	    	}
//	    }
//	  }
//	});