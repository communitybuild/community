//Author: Aishwarya Dhopate
angular
.module('app')
.controller(
		'eventsCalendar',
		[
		 '$scope',
		 '$http',
		 '$location',
		 '$rootScope',
		 '$window',
		 'service',
		 '$timeout',
		 function($scope, $http, $location, $rootScope, $window,
				 service, $timeout) {
			 $rootScope.location = $location.url();
			 $scope.consumerId = localStorage
			 .getItem("consumerId");
			 $scope.events=[];
			 $scope.eventDetails=[];
			 $scope.selectIndex="";
			 $timeout(function(){
			 $http
				.get(
						$rootScope.communityContextPath
								+ "/communitymanager/eventsdashboard/"
								+ $scope.consumerId)
				.success(
						function(
								data) {
							for(var index in data){
								var eventsObj={};
								var json = data[index];
								$scope.eventDetails.push(json);
								eventsObj.id=index;
								eventsObj.title=json.title;
								eventsObj.start=json.eventDate;
								$scope.events.push(eventsObj);
							}
							//callback($scope.events);
							});
			 },0);
			 
			 $timeout(function(){
			 $('#eventcalendar').fullCalendar({
				 events:$scope.events,
				 eventRender: function(event, element) {
					 if($scope.eventDetails[event.id].eventsrsvp!=null){
		                if($scope.eventDetails[event.id].eventsrsvp.rsvp == "Going") {
		                    element.css({'background-color':'#00a651','border-color':'#00a651'});
		                }else if($scope.eventDetails[event.id].eventsrsvp.rsvp == "Not Going") {
		                    element.css({'background-color':'#ed1c24','border-color':'#ed1c24'});
		                }else{
		                    element.css({'background-color':'#e27a14','border-color':'#e27a14'});
		                }
					 }
		            },
				 eventClick: function(event) {
					$scope.selectIndex=event.id;
					$scope.$apply();
					$("#popup-calendarevent").modal("show");
				 }
			    });
			 },600);
			 
				$scope.rsvpData = {
						"consumerId" : $scope.consumerId
					}
					$scope.rsvpEvents = function(eventID, reply) {
						$scope.rsvpData.eventId = eventID;
						$scope.rsvpData.rsvp = reply;
						if ($scope.eventDetails[$scope.selectIndex].eventsrsvp == null) {
							$http
									.post(
											$rootScope.communityContextPath
													+ "/communitymanager/rsvp",
											$scope.rsvpData)
									.success(
											function(data) {
												swal(
														"Thanks!",
														"Thanks for a RSVP",
														"success");
												$window.location.reload();
											});
						} else {
							$scope.rsvpData.id = $scope.eventDetails[$scope.selectIndex].eventsrsvp.id;
							$http
									.post(
											$rootScope.communityContextPath
													+ "/communitymanager/rsvpupdate",
											$scope.rsvpData)
									.success(
											function(data) {
												swal(
														"Thanks!",
														"Thanks for a RSVP",
														"success");
												$window.location.reload();
											});
						}
					}
		 } ]);


