//Author: Sunil Marne
//Modified by: Aishwarya Dhopate

angular.module('app').controller('communityHomepageCtrl', [ '$scope', '$http', '$location', '$rootScope','$window','service','Notification','$timeout', function($scope, $http, $location, $rootScope, $window, service, Notification,$timeout) {
		
	$rootScope.location = $location.url();
	
	// Get community name from url
	//window.location.reload();
	var currentUrl = window.location.href;
    currentUrl = currentUrl.split('/');
    var communityParent=currentUrl[6];
    $scope.subCommunities="";
	$scope.communityImages="";
	$scope.communityPaths=$rootScope.communityPaths;
	$scope.selectedReferral="";
	
	$('.flexslider').flexslider({
		animation: "slide"
	});
	
	$scope.signup = {"firstName" : "","lastName" : "","emailAddress" : "","password" : "","zipCode" : ""};
	$scope.login = {"username":"", "password":"","enabled": true, "credentialsexpired": false, "expired": false, "locked": false};
		
	
	$http.get($rootScope.communityContextPath+"/communities/"+communityParent+"/communityParent").success(function(data){
$scope.subCommunities=data;
	});
	
	//Create User Block
	$scope.createUser = function() {
		if($scope.signup.password.length<6 || $scope.signup.password.length>25){
			Notification.error({message:"The password length should be between 6 to 25 characters",replaceMessage: true});
			return false;
		}
		localStorage.setItem("createUser", 0);
			var referralVal=$scope.subCommunities.referralCode;
		service.post($rootScope.communityContextPath+"/consumers/register/"+referralVal, $scope.signup,'success');
	}
	
	//Login 
	$scope.validateLogin = function(){
		localStorage.setItem("createUser", 1);
		service.post($rootScope.communityContextPath+"/login", $scope.login,'/dashboard');
	}

	//Handle Enter key on login
	$scope.DoWork = function(){
		this.validateLogin();
	}
	$scope.checkForClassChanges=function(){
		if ($('#slider1').hasClass('flex-active-slide')){
			$(".hero").css("background", "url(images/kids.png) center center");  
		}else if ($('#slider2').hasClass('flex-active-slide')){
			$(".hero").css("background", "url(images/kids.png) center center");  
		}else if ($('#slider3').hasClass('flex-active-slide')){
			$(".hero").css("background", "url(images/kids.png) center center");  
		}
		var a=$timeout(function(){
			$scope.checkForClassChanges()
		}, 1000);
		if($rootScope.location.indexOf("us")!= -1){
			$timeout.cancel(a); }
	}
	if($rootScope.location.indexOf("us")!= -1){
		$timeout(function() {
		$scope.checkForClassChanges();
		},1500);
	}
	$scope.homePage=function(){
		var consumerId = localStorage.getItem("consumerId");
		if(consumerId){
			$window.location.href = '#/dashboard';
			$window.location.reload();
		}else{
			$window.location.href = '#/';
			$window.location.reload();
		}
	}
	//Handle enter key event on Signup
	$scope.DoWorkCreateUser = function(){
		this.createUser();
	}
	
	$scope.forgotPasswordModal=function(){
		$(".modal").modal("hide");
		$("#popup-forgotPassword").modal();
	}
	$scope.forgotpasswordEmail="";
	$scope.forgotPassword=function(){
		if($scope.forgotpasswordEmail==""){
			Notification.error({message:"Email Address field is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.forgotpasswordEmail){
			Notification.error({message:"Invalid Email Address",replaceMessage: true});
			return false;
		}
		$http.post($rootScope.communityContextPath+"/consumers/forgotPassword/",$scope.forgotpasswordEmail).success(function(data) { 								
			if(data==0){
				Notification.error({message:"Consumer with this email address does not exist in the system",replaceMessage: true});
			}else{			
			swal("The password has been sent to your email address");
			$(".modal").modal("hide");
			$scope.forgotpasswordEmail="";
		}
		});
	}
	$scope.contactUsEmail=function(){
		if($scope.email==""){
			Notification.error({message:"Email field is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.email){
			Notification.error({message:"Please enter the valid email",replaceMessage: true});
			return false;
		}
		if(!$scope.message){
			Notification.error({message:"Message field is mandatory",replaceMessage: true});
			return false;
		}

		$http.post($rootScope.communityContextPath+"/consumers/contactUs/"+$scope.message,$scope.email).success(function(data){
			swal("Thank you for contacting us!!");
			$scope.message="";
			$scope.email="";
		});
	}

	/* When user clicks the Icon */
	$('.nav-toggle').click(function() {
		$(this).toggleClass('active');
		$('.header-nav').toggleClass('open');
		event.preventDefault();
	});
	/* When user clicks a link */
	$('.header-nav li a').click(function() {
		$('.nav-toggle').toggleClass('active');
		$('.header-nav').toggleClass('open');

	});
//	Smooth Scrolling 
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 2000);
				return false;
			}
		}
	});
	
	$scope.joinCommunity=function(referral){
		$scope.subCommunities.referralCode=referral;
		$("#commList").hide();
	}
} ]);

angular.module('app')
.filter('split', function() {
    return function(input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        return input.split(splitChar)[splitIndex];
    }
});