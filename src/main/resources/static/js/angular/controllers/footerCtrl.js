//Author: Aishwarya Dhopate
angular.module('app').controller('footerController',
		[       '$scope',
		        '$http',
		        '$location',
		        '$rootScope',
		        '$window',
		        function($scope, $http, $location,$rootScope,$window) {
			$window.scrollTo(0, 0);
			$rootScope.location = $location.url();
			$(".hero").attr("id","aboutUsPage");
			setTimeout(function(){
				$(".tempHeightDiv").css("min-height","0");
			}, 4000);
			$scope.aboutUs=function(){
				$window.location.href = '#/aboutus';
			}
			$scope.homePage=function(){
				var consumerId = localStorage.getItem("consumerId");
				if(consumerId){
					$window.location.href = '#/dashboard';
					$window.location.reload();
				}else{
					$window.location.href = '#/';
					$window.location.reload();
				}
			}
		} ]);
