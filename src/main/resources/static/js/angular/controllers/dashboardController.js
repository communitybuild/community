//Since it's not a SinglePageApplication, Service and Factories doesn't make any sense in this application. Using it just for resourcing purpose.
//Although a Independent Service can be injected like Validation or Customization
//var app = angular.module('app', []);
angular
		.module('app')
		.controller(
				'DashboardCtrl',
				[
						'$scope',
						'$http',
						'$location',
						'$rootScope',
						'$window',
						'$timeout',
						'service',
						'Notification',
						function($scope, $http, $location, $rootScope, $window,
								$timeout, service, Notification) {
							localStorage.removeItem("createUser");
							var consumerId = localStorage.getItem("consumerId");
							$scope.consumerId = consumerId;
							$scope.consumerName = localStorage.getItem("consumerName");
							$scope.communityPaths = $rootScope.communityPaths;
							$scope.leftSection="5";
							$scope.orders = [];
							$scope.isProvider = 0;
							$scope.images = [];
							$scope.communitiesActive = [];
							$scope.communitiesNonActive = [];
							$scope.businesses = [];
							$scope.communityId="";
							$scope.communitySponsor="";
							$scope.communityNeedsList=[];
							$scope.hideSections="";
							$scope.pages=[1,2,3];
							$scope.currentPage = 0;
							$scope.pageSize=3;
							$scope.posts = [];
							$scope.communitySectionTitles=["News and Events",
"Featured Helpers",
"Featured Businesses",
"Needs"];
							$rootScope.$emit("CallCommImagesMethod", {
								consumer : consumerId
							});
//							$timeout(function() {
//							$http.post(
//									$rootScope.communityContextPath
//											+ "/consumers/mailTry1").success(
//									function(data) {
//alert("sk");
//									});
//							},1200);
							$timeout(function() {
								$http.get(
										$rootScope.communityContextPath
												+ "/consumers/communities/"
												+ consumerId).success(
										function(data) {
											for ( var index in data) {
												var json = data[index];
												$scope.communityId=json.communityID.id;
												$scope.communitySponsor=json.communityID.communitySponsors;
												var hiddenHelperValue=localStorage.getItem("HiddenHelper");
												if(hiddenHelperValue){
													$scope.hideSections=hiddenHelperValue;
													if($scope.hideSections.indexOf("Featured Businesses")!= -1 && $scope.hideSections.indexOf("Featured Helpers")!= -1){
														$scope.leftSection="7";
													}
												}
											if($scope.communitySponsor){
												$scope.communitySponsor=$scope.communitySponsor.split(",");
											}
											if(json.communityID.communityTitles){
												$scope.communitySectionTitles=json.communityID.communityTitles.split(",");
												
											}
												if (json.activeStatus == 0) {
													$scope.communitiesNonActive
															.push(json);
												} else {
													$scope.communitiesActive
															.push(json);
												}

											}

										})
							}, 100);
							
							//Show Community needs
							$timeout(function() {
								if($scope.hideSections.indexOf("Needs")== -1){
								$http.get(
										$rootScope.communityContextPath
												+ "/communityneeds/"
												+ $scope.consumerId+"/needsListDash").success(
										function(data) {
												$scope.communityNeedsList=data;
										})
								}
							}, 200);

							//Show posts on dashboard
							$timeout(function() {
										$timeout(
												function() {
													$http
															.get(
																	$rootScope.communityContextPath
																			+ "/communitymanager/postsdashboard/"
																			+ consumerId)
															.success(
																	function(
																			data) {
																		for ( var index in data) {
																			var json = data[index];
																			if (json.image) {
																				var fileType = json.image
																						.split("_");
																				$scope.images
																						.push(fileType[0]);
																			} else {
																				$scope.images
																						.push(0);
																			}
																			$scope.posts
																					.push(json);
																		}
																	})
															.error(
																	function(
																			data) {
																		Notification
																				.error(data.message);
																	});
												}, 100);
										
										//Show events on dashboard
										$timeout(function() {
													$http
															.get(
																	$rootScope.communityContextPath
																			+ "/communitymanager/eventsdashboard/"
																			+ consumerId)
															.success(
																	function(
																			data) {
																		for(var index in data){
																			var json = data[index];
																			$scope.posts.push(json);
																		}																	})
															.error(
																	function(
																			data) {
																		Notification
																				.error(data.message);
																	});
												}, 300);
										
									}, 400);

							//Show upcoming orders
							$timeout(function() {
										$rootScope.location = $location.url();
										// Fetch the ProviderId of the loggedIn
										var providerId = localStorage
												.getItem("providerId");
										if ((providerId === null || providerId === undefined)
												&& consumerId != undefined) {
											$scope
													.fetchOrdersOfConsumer(consumerId);
										} else {
											$scope
													.fetchOrdersOfProvider(providerId);
										}

									}, 600);
							
							//Show community businesses
							$timeout(function() {
								if($scope.hideSections.indexOf("Featured Businesses")== -1){
								$http.get(
										$rootScope.communityContextPath
												+ "/businesses/" + consumerId)
										.success(function(data) {
												$scope.businesses=data;
										}).error(function(data) {
											console.log(JSON.stringify(data))
										})
								}
							}, 800);
						
							
							//Send mail after registration
							$timeout(function() {
										var regimail = localStorage.getItem("regimail");
										if (regimail == 1) {
											$http.post(
															$rootScope.communityContextPath
																	+ "/consumers/mailSendRegister/"
																	+ consumerId)
													.success(function(data) {
													});
											localStorage.removeItem("regimail");
										}
									}, 1000);

							$scope.fetchOrdersOfProvider = function(providerId) {
								$scope.isProvider = 1;
								$http
										.get(
												$rootScope.communityContextPath
														+ "/workrequests/provider/"
														+ consumerId + "/"
														+ providerId).success(
												function(data) {
														$scope.orders=data;
												}).error(function(data) {
											console.log(JSON.stringify(data))
										})

							}

							$scope.fetchOrdersOfConsumer = function() {
								$scope.isProvider = 0;
								$http.get(
										$rootScope.communityContextPath
												+ "/workrequests/consumer/"
												+ consumerId).success(
										function(data) {
												$scope.orders=data;
										}).error(function(data) {
									console.log(JSON.stringify(data));
									Notification.error(data.message);
								})

							}

						
//							 Fetch  featured helpers On load
							
							$scope.providers = [];
							if($scope.hideSections.indexOf("Featured Helpers")== -1){
							$http.get(
									$rootScope.communityContextPath
											+ "/providers/" + consumerId)
									.success(function(data) {
										$scope.providers = data;
									});
						}
							$scope.navigateToProvider = function() {
								$location.path("/provider")
							}

							$scope.navigateToRegistration = function(provider) {
								localStorage.setItem("providerInfo", JSON
										.stringify(provider));
								$location.path("/registration")
							}

							$scope.navigateToProviderOrders = function() {
								var providerId = localStorage
										.getItem("providerId");
								if ((providerId === null || providerId === undefined)
										&& consumerId != undefined)
									$location.path("/providerorders");
								else
									$location.path("/acceptedRequests");
							}

							$scope.rsvpData = {
								"consumerId" : consumerId
							}
							$scope.rsvpEvents = function(eventID, reply, ind) {
								$scope.rsvpData.eventId = eventID;
								$scope.rsvpData.rsvp = reply;
								if ($scope.posts[ind].eventsrsvp == null) {
									$http
											.post(
													$rootScope.communityContextPath
															+ "/communitymanager/rsvp",
													$scope.rsvpData)
											.success(
													function(data) {
														$scope.posts[ind].eventsrsvp = $scope.rsvpData;
														swal(
																"Thanks!",
																"Thanks for a RSVP",
																"success");
													});
								} else {
									$scope.rsvpData.id = $scope.posts[ind].eventsrsvp.id;
									$http
											.post(
													$rootScope.communityContextPath
															+ "/communitymanager/rsvpupdate",
													$scope.rsvpData)
											.success(
													function(data) {
														$scope.posts[ind].eventsrsvp = $scope.rsvpData;
														swal(
																"Thanks!",
																"Thanks for a RSVP",
																"success");
													});
								}
							}

							$scope.checkForFloatNumber = function(numberf) {
								if (Number(numberf) === numberf
										&& numberf % 1 !== 0) {
									return true;
								}
							}
							
							$scope.NavigateToneedsList=function(){
								$location.path("/communityNeeds");
							}
							

							$scope.numberOfPages=function(){
								return Math.ceil($scope.posts.length/$scope.pageSize);                
							}
							$scope.prevArrow=function(){
								if($scope.currentPage==0 || $scope.pages[0]<=1){
									return false;
								}else{
									$scope.currentPage=$scope.currentPage-1;
									$scope.pages.splice(0,3,$scope.pages[0]-1,$scope.pages[0],$scope.pages[1]);
								}
								$timeout(function () {
									var divHeight=$('.frowDivs').height();
									$(".tabHeightLeft").css("min-height",divHeight+40);
								});
							}
							$scope.nextArrow=function(){
								if($scope.currentPage >= $scope.numberOfPages()-1 || $scope.pages[2]>=$scope.numberOfPages()){
									return false;
								}else{
									$scope.currentPage=$scope.currentPage+1;
									$scope.pages.splice(0,3,$scope.pages[1],$scope.pages[2],$scope.pages[2]+1);
								}
								$timeout(function () {
									var divHeight=$('.frowDivs').height();
									$(".tabHeightLeft").css("min-height",divHeight+40);
								});
							}
							$scope.showPostsofCurrentPg=function(page){
								if(page>$scope.numberOfPages()){
									return false;
								}else{
									$scope.currentPage=page-1;
								}
								$timeout(function () {
									var divHeight=$('.frowDivs').height();
									$(".tabHeightLeft").css("min-height",divHeight+40);
								});
							}
							
						} ]);

angular.module('app').filter('range', function() {
	return function(input, total) {
		total = parseInt(total);
		for (var i = 0; i < total; i++) {
			input.push(i);
		}
		return input;
	};
});
