//Author: Aishwarya Dhopate
angular.module('app').controller('forumTopicCtrl',
				[       '$scope',
						'$window',
						'$http',
						'Notification',
						'$rootScope',
						function($scope, $window,$http,Notification,$rootScope) {
					var consumerId=localStorage.getItem("consumerId");
					var topicId=localStorage.getItem("topicId");
					$scope.consumerId=consumerId;
					$scope.forumTopic="";
					$scope.loading1=false;
					var imagestobeDeleted=[];
					$http.get(
							$rootScope.communityContextPath
									+ "/communityforum/"
									+ topicId+"/topicData").success(
							function(data) {
								if(data[0].forumReplies!=''){
									for(var index in data[0].forumReplies){
										var json = data[0].forumReplies[index];
										var today = new Date();
										var Christmas = new Date(json.createDate);	
										var diffMs = (today-Christmas); // milliseconds between now & Christmas
										var diffDays = Math.floor(diffMs / 86400000); // days
										var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
										var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
									if(diffDays>1){
										json.createDate=diffDays+" days";
									}else if(diffDays==1){
										json.createDate=diffDays+" day";
									}else if(diffDays<1 && diffHrs>1){
										json.createDate=diffHrs+" hours "+diffMins+" minutes";
									}else if(diffDays<1 && diffHrs==1){
										json.createDate=diffHrs+" hour "+diffMins+" minutes";
									}else if(diffDays<1 && diffHrs<1){
										json.createDate=diffMins+" minutes";
									}
									if(data[0].forumReplies.length==Number(index)+1){
										$scope.forumTopic=data;
									}											
									}}else{
										$scope.forumTopic=data;
									}

							});
					
					
					$scope.reply={
							topicId:topicId
					};
					$scope.giveReply=function(){
						var imageName=document.getElementById("proffile").files[0];
						if(!$scope.reply.topicReply){
							Notification.error({message:"Reply cannot be empty",replaceMessage: true});
							return false;
						}
						$scope.loading1=true;
						var now = new Date();
						 if(imageName){
						var imageName1="forum_"+now.getTime();
						$scope.reply.topicReplyAttachment=imageName1;
						 }
				       $http.post($rootScope.communityContextPath+"/communityforum/"+$scope.consumerId+"/reply",$scope.reply).success(function(data) {
				    	   if(imageName){
								var fd = new FormData();    
								fd.append('uploadfile',imageName,imageName1);
								fd.append('uploadfilename',imageName1);
								var folderName="forum_images";
								$.ajax({
									url: $rootScope.communityContextPath+"/uploadFile/"+folderName,
									type: "POST",
									data: fd,
									contentType:false,
									cache: false,
									processData:false,
									success: function(data){
										$scope.loading1=false;
										$scope.$apply();
											  $scope.listofForumReplies();
										  $("#replyToForumTopic").modal("hide");
										  angular.copy({},$scope.reply);
										  $("#linkedForumImg").text("");
									}
								});	
								}else{
									$scope.loading1=false;
									   $scope.listofForumReplies();
									  $("#replyToForumTopic").modal("hide");
									  angular.copy({},$scope.reply);
								}
				    
						}).error(function(data, status) {
						console.log(JSON.stringify(data))
						Notification.error(data.message);
					});
					}
					
					$scope.showBrowseDialog=function(){
						$("#proffile").click();
					}
					
					$("#proffile").change(function(e) {
						$("#linkedForumImg").text(e.target.files[0].name);
					});
					
					$scope.listofForumReplies=function(){
						$http.get(
							$rootScope.communityContextPath
									+ "/communityforum/"
									+ topicId+"/topicReply").success(
							function(data) {
								if(data.length>0){
									for(var index in data){
										var json = data[index];
										var today = new Date();
										var Christmas = new Date(json.createDate);	
										var diffMs = (today-Christmas); // milliseconds between now & Christmas
										var diffDays = Math.floor(diffMs / 86400000); // days
										var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
										var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
									if(diffDays>1){
										json.createDate=diffDays+" days";
									}else if(diffDays==1){
										json.createDate=diffDays+" day";
									}else if(diffDays<1 && diffHrs>1){
										json.createDate=diffHrs+" hours "+diffMins+" minutes";
									}else if(diffDays<1 && diffHrs==1){
										json.createDate=diffHrs+" hour "+diffMins+" minutes";
									}else if(diffDays<1 && diffHrs<1){
										json.createDate=diffMins+" minutes";
									}
									if(data.length==Number(index)+1){
										$scope.forumTopic[0].forumReplies=data;
									}
									if(imagestobeDeleted.indexOf(json.topicReplyAttachment)== -1 && json.topicReplyAttachment){
									imagestobeDeleted.push(json.topicReplyAttachment);
									}
								}}else{
										$scope.forumTopic[0].forumReplies=data;
									}

							});
					}
					
					$scope.deleteTopic=function(){
						if($scope.forumTopic[0].topicAttachment){
						imagestobeDeleted.push($scope.forumTopic[0].topicAttachment);
						}
						
						  swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Remove",   closeOnConfirm: false }, function(){   
									$http.post($rootScope.communityContextPath+"/communityforum/"+topicId+"/deleteTopic",imagestobeDeleted).success(function(data) {
										swal("Removed!", "The topic has been removed", "success");
										$window.location.href = '#/forum';
						                $window.location.reload();
									}).error(function(data, status) {
										console.log(JSON.stringify(data))
										Notification.error(data.message);
									});
							  });
					}
					
					$scope.deleteReply=function(replyid,replyAttachment){
						  swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Remove",   closeOnConfirm: false }, function(){   
								$http.post($rootScope.communityContextPath+"/communityforum/"+replyid+"/"+topicId+"/deleteReply",replyAttachment).success(function(data) {
									swal("Removed!", "The reply has been removed", "success");
									$scope.listofForumReplies();
								}).error(function(data, status) {
									console.log(JSON.stringify(data))
									Notification.error(data.message);
								});
						  });
				}
					var interval1=setInterval(function () {
							$scope.listofForumReplies();
						    }, 3000);
					$scope.$on('$destroy', function(){
						clearInterval(interval1);
					});
				
						} ]);

