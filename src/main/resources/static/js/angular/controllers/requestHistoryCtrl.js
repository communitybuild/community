//Author: Aishwarya Dhopate
angular.module('app').controller('requestHistoryCtrl', [ '$scope', '$http', '$location', '$rootScope','service','Notification','$filter', function($scope, $http, $location, $rootScope,service,Notification,$filter) {

	var providerId = localStorage.getItem("providerId");
	var consumerId=localStorage.getItem("consumerId");
	$scope.consumerName=localStorage.getItem("consumerName");
	$scope.requests = [];
	$scope.timeFinal=[];
	$scope.getRequestHistory=function(){
		$http.get($rootScope.communityContextPath+"/workrequests/requesthistory/"+providerId+"/"+consumerId).success(function(data){
			for(var index in data){
				var json = data[index];
//				var newDate = new Date(json.requestDate);
//				json.requestDate = newDate;
				$scope.dateFinal=json.requestDate.split(" ");
				json.requestDate=$scope.dateFinal[0];
				var time=$scope.dateFinal[1]+$scope.dateFinal[2]+$scope.dateFinal[3]+" "+$scope.dateFinal[4];
				$scope.timeFinal.push(time);
				if(json.processingStatus!=3)
				{
					json.actualHours=json.estimatedHours;
				}						
				$scope.requests.push(json);
			}
		}).error(function(data){
			Notification.error(data.message);
		})
	}
	$scope.getRequestHistory();
	$scope.markRequestAsCompleted=function(workRequestId,hours){
		var actualHours=$filter('number')(hours, 1);
		$scope.actualHr={ 
				"actualHours":actualHours
		}

		$http.post($rootScope.communityContextPath+"/workrequests/markComplete/"+workRequestId,$scope.actualHr).success(function(data){
			$scope.requests = [];
			$scope.getRequestHistory();
			var functionName="reqHistory";
			$http.post($rootScope.communityContextPath+"/workrequests/reqMailSending/"+workRequestId+"/"+providerId+"/"+$scope.consumerName+"/"+functionName,$scope.actualHr).success(function(data){});
		}).error(function(data){
			Notification.error(data.message);
		})}

	$scope.decrementCountofActualHours=function(ind){
		var ActualHr=$scope.requests[ind].actualHours;
		if((Math.round((Number($scope.requests[ind].actualHours))*Number(10)))%10==0){
			$scope.requests[ind].actualHours=$scope.requests[ind].actualHours-0.5;
		}else{
			$scope.requests[ind].actualHours=$scope.requests[ind].actualHours-0.1;
		}
		if($scope.requests[ind].actualHours<=0.3){
			$scope.requests[ind].actualHours=ActualHr;
		}
	}
	$scope.incrementCountofActualHours=function(ind){
		$scope.requests[ind].actualHours=$scope.requests[ind].actualHours+0.1;
		var actualH=$scope.requests[ind].actualHours-0.1;
		if((Math.round((actualH)*Number(10)))%5==0 && (Math.round((actualH)*Number(10)))%10!=0){
			$scope.requests[ind].actualHours=Math.round($scope.requests[ind].actualHours);
		}
	}
} ]);