//Author: Aishwarya Dhopate
angular
.module('app')
.controller(
		'navigationCtrl',
		[
		 '$scope',
		 '$http',
		 '$location',
		 '$rootScope',
		 '$window',
		 'service',
		 '$timeout',
		 function($scope, $http, $location, $rootScope, $window,
				 service, $timeout) {
			 $rootScope.location = $location.url();
			 $scope.consumerId = localStorage
			 .getItem("consumerId");
			 $scope.communityActiveStatus=localStorage.getItem("communityActiveStatus");
			 $scope.homepage = function() {
				 $window.location.href = '#/';
				 $window.location.reload();
			 }
			 if (!$scope.consumerId) {
				 $scope.homepage();
			 }

			 $rootScope.provider = localStorage
			 .getItem("providerId");
			 $rootScope.communityManager = localStorage
			 .getItem("communityManager");
			 $rootScope.hiddenHelper = localStorage
			 .getItem("HiddenHelper");
			 $scope.communityPaths = $rootScope.communityPaths;

			 $scope.communityImage = "";
			 $scope.communityId = "";
			 $rootScope.$on("CallCommImagesMethod", function(
					 event, args) {
				 $scope
				 .getHeaderImages(args.consumer,
						 args.flag);
			 });

			 $scope.getHeaderImages = function(consumer, flag) {
				 $http
				 .get(
						 $rootScope.communityContextPath
						 + "/communities/commImages/"
						 + consumer)
						 .success(
								 function(data) {
									 $scope.communityImage = "";
									 $scope.communityId = data[0]
									 .pop();
									 $scope.communityImage = data[0];
									 if ($scope.communityImage[0] === null
											 || $scope.communityImage[0] === "") {
										 $scope.communityImageFlag = true;
										 $scope.communityImage = [
										                          'images/dashboard-header-img-3.jpg',
										                          'images/dashboard-header-img-7.jpg',
										                          'images/dashboard-header-img-4.jpg',
										                          'images/dashboard-header-img-6.jpg',
										                          'images/dashboard-header-img-8.jpg',
										                          'images/dashboard-header-img-9.jpg',
										                          'images/dashboard-header-img-10.jpg' ];
									 } else {
										 $scope.communityImageFlag = false;
										 $scope.communityImage = $scope.communityImage[0]
										 .split(",");
										 if (flag == 1) {
											 $
											 .each(
													 $scope.communityImage,
													 function(
															 i,
															 file) {
														 var filename = file
														 + "?"
																 + new Date()
														 .getTime();
														 $scope.communityImage
														 .splice(
																 i,
																 1,
																 filename);
													 });
										 }
									 }
								 });
			 }
			 if (!$scope.communityImage && $scope.consumerId) {
				 $scope.getHeaderImages($scope.consumerId);
			 }

			 // Everytime a user clicks on home, it will be
			 // controlled from here
			 $scope.home = function() {
				 $location.path('/dashboard');
			 }
			 // Code added by Aishwarya
			 $scope.myProfile = function() {
				 $location.path('/myProfile');
			 }
			 $scope.openRequests = function() {
				 $location.path('/openRequests');
			 }
			 $scope.acceptedRequests = function() {

				 $location.path('/acceptedRequests');
			 }
			 $scope.requestHistory = function() {
				 $location.path('/requestHistory');
			 }
			 $scope.myCommunity = function() {
				 $location.path('/myCommunity');
			 }
			 $scope.myOrders = function() {
				 $location.path('/providerorders');
			 }
			 $scope.communityMembers = function() {
				 $location.path('/communityMembers');
			 }
			 $scope.communityMembers = function() {
				 $location.path('/communityMembers');
			 }
			 $scope.forum = function() {
				 $location.path('/forum');
			 }
			 $scope.communityEvents=function(){
				 $location.path('/communityEvents');
			 }
			 // Logout Function
			 $scope.logout = function() {
				 // In case if browser had some restriction and
				 // unable to clear
				 // then set -1
				 localStorage.setItem("consumerId", -1);
				 localStorage.setItem("createUser", -1);
				 localStorage.setItem("providerId", -1);
				 localStorage.setItem("consumerName", -1);

				 localStorage.removeItem("consumerId");
				 localStorage.removeItem("createUser");
				 localStorage.removeItem("providerId");
				 localStorage.removeItem("consumerName");

				 localStorage.clear();
				 // $location.path('/');
				 $window.location.href = '#/';
				 $window.location.reload();
			 };
			 $scope.navigateToBusinesses = function() {
				 $location.path("/businesses")
			 }

			 $("ul.navigation li a").click(function(e){
				 if(!$(this).hasClass("dropdown-toggle")){
					 e.preventDefault();
					 $("ul.navigation li").removeClass("active");
					 if($(this).closest(".dropdown").length>0){
						 $(this).parent().parent().parent().addClass("active");
						 $scope.navBarActive=this;
					 }
					 $(this).parent().addClass("active");
				 }
			 });

		 } ]);

//Directive to Handle Enter Key
angular.module('app').directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if (event.which === 13) {
				scope.$apply(function() {
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
});
