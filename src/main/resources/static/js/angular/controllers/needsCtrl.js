//Author: Aishwarya Dhopate
angular.module('app').controller('needsCtrl',
				[       '$scope',
						'$window',
						'$http',
						'$rootScope',
						function($scope, $window,$http,$rootScope) {
					var consumerId=localStorage.getItem("consumerId");
					var consumerName = localStorage.getItem("consumerName");
					$scope.consumerId=consumerId;
					$scope.communityNeedsList=[];
					
					$scope.getNeedsListwithSignups=function(){
					$http.get(
							$rootScope.communityContextPath
									+ "/communityneeds/"
									+ consumerId+"/needsList").success(
							function(data) {
								for ( var index in data) {
									var json = data[index];
									if(!json.communityId){
										json.communityId=0;
									}
									$scope.communityNeedsList.push(json);
								}
							})
					}
					$scope.getNeedsListwithSignups();
							$scope.needIndex="";
							$scope.disableSignUp=function(signupscnt,quantity,ind){
						if(signupscnt>quantity){
							return false;
						}else{
							$scope.needIndex=ind;
							$("#signUpNeedsModal").modal("show");
						}
					}
							$scope.signUpQuantityInfo={}
							$scope.signUpfortheNeed=function(){
								  if(!$scope.signUpQuantityInfo.signupQuantity){
									  $scope.signUpQuantityInfo.signupQuantity=1;
								  }
								$scope.signUpQuantityInfo.needId=$scope.communityNeedsList[$scope.needIndex].id;
								  $http.post($rootScope.communityContextPath+ "/communityneeds/"
											+ consumerId+"/createSignUps",$scope.signUpQuantityInfo).success(function(data) {
												swal("Signed up successfully", "", "success");
												$("#signUpNeedsModal").modal("hide");
												$scope.signUpQuantityInfo.signupQuantity="";
												$scope.communityNeedsList=[];
												$scope.getNeedsListwithSignups();
												  $http.post($rootScope.communityContextPath+ "/communityneeds/"
															+ consumerId+"/"+consumerName+"/mailSignUps",$scope.signUpQuantityInfo).success(function(data) {			
												  });
								  });
							};
							
							$scope.deleteSignup=function(needId,consumer){
								swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Delete",   closeOnConfirm: true }, function(){
								$http.post($rootScope.communityContextPath+ "/communityneeds/"
										+ consumer+"/"+needId).success(function(data) {
											swal("Signup deleted Successfully", "", "success");
											$scope.communityNeedsList=[];
											$scope.getNeedsListwithSignups();
							  });
								});
							}
						} ]);
