//Since it's not a SinglePageApplication, Service and Factories doesn't make any sense in this application. Using it just for resourcing purpose.
//Although a Independent Service can be injected like Validation or Customization
//var app = angular.module('app', []);
//Changes in code by Aishwarya
angular
.module('app')
.controller(
		'RegistrationCtrl',
		[
		 '$scope',
		 '$http',
		 '$location',
		 '$rootScope',
		 '$window',
		 '$timeout',
		 'service',
		 'Notification',
		 function($scope, $http, $location, $rootScope, $window,
				 $timeout, service,Notification) {
		     $window.scrollTo(0, 0);	
			 $scope.loading=false;
			 $rootScope.location = $location.url();
			 var consumerId = localStorage.getItem("consumerId");
			 $rootScope.$emit("CallCommImagesMethod", {consumer:consumerId});
			 $scope.provider = JSON.parse(localStorage.getItem("provider"));

			 $timeout(function() {
				 $rootScope.location = $location.url();
			 }, 5);
			 $timeout(function() {
				 $scope.ratingWorkData="";
				 $http.get($rootScope.communityContextPath+"/workrequests/ratingModal/"+consumerId).success(function(data){
					 if(data!=''){
						 $scope.ratingWorkData=data;
						 $("#popup-rating").modal()
					 }
				 }).error(function(data){
					 Notification.error(data.message);
				 });
			 }, 50);
			 $scope.displayActivities="";
			 $scope.providerInfo=JSON.parse(localStorage.getItem("providerInfo"));
			 if($scope.providerInfo){
				 $scope.displayActivities=$scope.providerInfo.activities;
			 }else{
				 $http.get($rootScope.communityContextPath+"/activities").success(function(data){
					 $scope.displayActivities=data;
				 }).error(function(data){
					 Notification.error(data.message);
				 });
			 }
			 $scope.activityCost="";
			 $scope.oldsrc=[];
			 $scope.select=function(index){
				 var activitySelected=$("input[name=activityRadio]:checked").val();	

				 var attri=$("#"+index).attr("src");
				 if($scope.oldsrc.indexOf(index)== -1){
					 $("#"+$scope.oldsrc[0]).attr("src",$scope.oldsrc[1]);
					 $scope.oldsrc.splice(0,2,index,attri);	
				 }
				 if(index==1){
					 $("#"+index).attr("src","images/homeselected.png");
				 }else if(index==2){
					 $("#"+index).attr("src","images/petsselected.png");
				 }else if(index==3){
					 $("#"+index).attr("src","images/cartselected.png");
				 }else{
					 $("#"+index).attr("src","images/yardworkselected.png");
				 }
				 $scope.registration.activity=JSON.parse(activitySelected);
			 }


			 // POOR Server side code, forcing me to create
			 // entire Json here rather than sending Id....Sammeer Shukla
			 $scope.registration = {
					 "requestDate" : "",
					 "description" : "",
					 "estimatedHours" : 1,
					 "estimatedCost" : 0
			 };
			 // Pending Validation before sending request......
			 $scope.createWorkRequest = function() {
				 var date=$("#reqDate").val();
				 var time=$("#timepick").val();
				 var today=new Date();
				 var dd = today.getDate();
				 var mm = today.getMonth()+1;
				 var yyyy = today.getFullYear();
				 if(mm.toString().length<2){
					 var zeroValm=0;
				 }
				 if(dd.toString().length<2){
					 var zeroVald=0;
				 }
				 todayDate=yyyy+"-"+zeroValm+mm+"-"+zeroVald+dd;
				 if(Date.parse(date)==Date.parse(todayDate)){
					 var hours = today.getHours();
					 var minutes = today.getMinutes();
					 var ampm = hours >= 12 ? 'PM' : 'AM';
					 hours = hours % 12;
					 hours = hours ? hours : 12; // the hour '0' should be '12'
					 minutes = minutes < 10 ? '0'+minutes : minutes;
					 var strTime = hours + ':' + minutes + ' ' + ampm;
					 strTime=new Date(Date.parse(todayDate.replace(/-/g, ' ') + " " +strTime));
					 var a=time.split(" ");
					 var startTimeFinal=a[0]+a[1]+a[2]+" "+a[3];
					 startTimeFinal=new Date(Date.parse(todayDate.replace(/-/g, ' ') + " " +startTimeFinal));
					 if(startTimeFinal<strTime){
						 Notification.error({message:"Start time should be greater than current time",replaceMessage: true});
						 return false;		
					 }
				 }
				 $scope.loading=true;
				 if($scope.providerInfo){
					 $scope.registration.providerId=$scope.providerInfo.id;
				 }else{$scope.registration.providerId=0}
				 if(date=="" || time=="" || $scope.registration.activity==null ||$scope.registration.estimatedHours==null){
					 $scope.loading=false;
					 Notification.error({message:"Please fill all fields",replaceMessage: true});
					 return false;
				 }else{
					 $scope.registration.requestDate = date +" "+ time; 
					 $http.post($rootScope.communityContextPath+"/workrequests/createRequest/"+consumerId, $scope.registration).success(function(data) {
					 $scope.loading=false;
					 localStorage.removeItem("providerInfo");
					 swal("Thanks! Work request has been created successfully");
					 $location.path('/dashboard');
					 }).error(function(data, status) {
					 console.log(JSON.stringify(data))
					 Notification.error(data.message);
					 })
				 }
			 }

			 $scope.decrementCount=function(){
				 if($scope.registration.estimatedHours>1){
					 $scope.registration.estimatedHours=$scope.registration.estimatedHours-1;
				 }else{
					 return false;
				 }
			 }

			 $scope.incrementCount=function(){
				 if($scope.registration.estimatedHours<8){
					 $scope.registration.estimatedHours=$scope.registration.estimatedHours+1;
				 }else{
					 return false;
				 }
			 }

			 $scope.reviewsObj={
					 ratings:"",
					 reviews:"",

			 }
			 $scope.submitReviews=function(){
				 if($scope.reviewsObj.ratings==""){
					 Notification.error({message:"Please select rating",replaceMessage: true});
					 return false;
				 }
				 $scope.reviewsObj.workRequestId=$scope.ratingWorkData.id;
				 $scope.reviewsObj.providerId=$scope.ratingWorkData.providerId;
				 $http.post($rootScope.communityContextPath+"/workrequests/reviews",$scope.reviewsObj).success(function(data) {
					 swal("Thanks! Ratings have been submitted successfully");
					 $("#popup-rating").modal("hide");
				 });
			 }
		 } ]);
angular.module('app').directive('datetimepickerfromtoday', function() {
	return function($scope, element) {
		element.datetimepicker({
			format: "YYYY-MM-DD",
			minDate:new Date()
		});
	};
});