//Author: Aishwarya Dhopate
angular.module('app').controller('communityMembersCtrl',
				[       '$scope',
						'$http',
						'$rootScope',
						function($scope, $http,$rootScope) {
				var consumerId=localStorage.getItem("consumerId");
				$scope.members=[];
				$http.get($rootScope.communityContextPath+"/consumers/"+consumerId+"/memberlist/").success(function(data){
				    for(var index in data){
						var json = data[index];
						$scope.members.push(json);
					}
				    });
						} ]);
