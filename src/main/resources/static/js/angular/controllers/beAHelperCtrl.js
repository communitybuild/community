//Author: Aishwarya Dhopate
angular.module('app').controller('BeAHelperCtrl',
		[       '$scope',
		        '$http',
		        '$location',
		        '$rootScope',
		        'service',
		        'Notification',
		        function($scope, $http, $location, $rootScope,service, Notification) {
			$rootScope.location = $location.url();
			$rootScope.$emit("CallCommImagesMethod", {consumer:consumerId});

			var consumerId = localStorage.getItem("consumerId");
			// $rootScope.$emit("CallCommImagesMethod", {consumer:consumerId});
			$scope.consumerName = localStorage.getItem("consumerName");

			$scope.displayActivities="";
			$http.get($rootScope.communityContextPath+"/activities").success(function(data){
				$scope.displayActivities=data;
			}).error(function(data){
				Notification.error(data.message);
			});
			$scope.communityName="";
			$http.get($rootScope.communityContextPath+"/consumers/community/"+consumerId).success(function(data){
				$scope.communityName=data;
			}).error(function(data){
				Notification.error(data.message);
			})
			$scope.selected = [];
			$scope.provider = {
					//"dateOfBirth" : "",
					//	"ssn" : "",
					"description" : "",
					//	"imageUrl" : "",
					"activities" : []
			};

			$scope.select = function(index) {	
				var pushedInd=$scope.selected.indexOf(index);
				if(pushedInd=== -1){
					$scope.selected.push(index);
					if(index==1){
						$("#"+index).attr("src","images/homeselected.png");
					}else if(index==2){
						$("#"+index).attr("src","images/petsselected.png");
					}else if(index==3){
						$("#"+index).attr("src","images/cartselected.png");
					}else{
						$("#"+index).attr("src","images/yardworkselected.png");
					}
				}else{
					$scope.selected.splice(pushedInd,1);
					if(index==1){
						$("#"+index).attr("src","images/homelogo.png");
					}else if(index==2){
						$("#"+index).attr("src","images/pets.png");
					}else if(index==3){
						$("#"+index).attr("src","images/cart.png");
					}else{
						$("#"+index).attr("src","images/yardworklogo.png");
					}
				}

			}

			/**
			 * I have corrected the mapping on the server which
			 * was also creating rows in Activity table Ideally
			 * rows should be created only in Provider and
			 * Provider_Activity Table, Only Activity ID should
			 * be picked from Activity Table - Sameer Shukla
			 */
			$scope.createProvider = function() {
				var consumerId = localStorage.getItem("consumerId");
				//	$scope.provider.dateOfBirth=$("#profdob").val();
				$scope.activities = {};
//				for ( var i in $scope.selected) {
//				var a=$scope.selected[i]-1;
				$scope.selected.forEach(function(a){
					var ind=a-1;
					$scope.activities = {
							"id" : a,
							"activityName" : $scope.displayActivities[ind].activityName,
							"description" : $scope.displayActivities[ind].description
					};
					$scope.provider.activities
					.push($scope.activities);
				});
				if($scope.provider.activities.length<=0){
					Notification.error({message:"Please select atleast 1 help type",replaceMessage: true});
					return false;
				}
				service.post($rootScope.communityContextPath+"/consumers/"
				+ consumerId + "/providers",
				$scope.provider, 'providerReg');
			}

			/**
			 * File Upload Piece
			 */

			$scope.uploadedFile = function(element) {
				//	$scope.provider.imageUrl = "C:/Files/"
				//		+ element.files[0].name;
				$scope.provider.imageUrl = "images/"+ element.files[0].name;
//				$scope.$apply(function($scope) {
//				$scope.files = element.files;
//				});
			}
			//	$scope.files=[];
			$scope.addFile = function() {
				this.uploadfile($scope.files, function(msg) // success
						{
					console.log('uploaded');
						}, function(msg) // error
						{
							console.log('error');
						});
			}

			$scope.uploadfile = function(files, success, error) {
				var fd = new FormData();
				var url = '/uploadFile';

				angular.forEach(files, function(file) {
					fd.append('uploadfile', file);
				});

				$.ajax({
					type: "POST",
					url: $rootScope.communityContextPath+"/uploadFile",
					data: fd,
					contentType: false,
					processData: false,
					cache: false,
					/*beforeSend: function(xhr, settings) {
								                    xhr.setRequestHeader("Content-Type", "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p");
								                    settings.data = {name: "file", file: inputElement.files[0]};                    
								                },*/
					success: function (result) {

						if ( result.reseponseInfo == "SUCCESS" ) {

						} else {

						}
					},
					error: function (result) {
						console.log(result.responseText);
					}
				});

			}

			$scope.readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function(e) {
						$('#blah').attr('src', e.target.result);
					}

					reader.readAsDataURL(input.files[0]);
				}
			}

			$("#imgInp").change(function() {
				$scope.readURL(this);
			});


		} ]);
