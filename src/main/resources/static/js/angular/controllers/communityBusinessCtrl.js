//Author: Aishwarya Dhopate
angular.module('app').controller('CommunityBusinessCtrl', [ '$scope', '$http', '$location', '$rootScope','$window','$timeout','service','Notification','$filter', function($scope, $http, $location, $rootScope, $window,$timeout, service,Notification,$filter) {
	$scope.communityPaths=$rootScope.communityPaths;
	var consumerId=localStorage.getItem("consumerId");
	$scope.loading=false;
	$scope.consumerName = localStorage.getItem("consumerName");
	$scope.businessInfo="";
	$scope.update=false;
	$scope.getConsumerBusinesses=function(){
		$scope.loading=true;
		$http.get($rootScope.communityContextPath+"/businesses/"+consumerId+"/consumerBusiness").success(function(data){
			$scope.loading=false;
			$scope.businessInfo=data;
			if($scope.businessInfo){
				$scope.update=true;
			}else{
				$scope.businessInfo={
						"businessName": "",
						"emailAddress": "",
						"phoneNumber": ""
				};
			}
		});
	}
	$scope.getConsumerBusinesses();
	$scope.createCommunityBusiness = function(){
		if($scope.businessInfo.businessName==""){
			Notification.error({message:"Business name is mandatory",replaceMessage: true});
			return false;
		}
		if($scope.businessInfo.emailAddress=="" && $scope.businessInfo.phoneNumber==""){
			Notification.error({message:"Contact information is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.businessInfo.emailAddress){
			Notification.error({message:"Invalid email address",replaceMessage: true});
			return false;
		}
		$scope.loading=true;
		var now = new Date();
		var fd = new FormData();
		var imageName=document.getElementById("proffile").files[0];
		if(imageName){
			var imageName1="business_"+now.getTime();
			$scope.businessInfo.communityImage=imageName1;
		}
		$http.post($rootScope.communityContextPath+"/businesses/"+consumerId,$scope.businessInfo).success(function(data){
			if(imageName){
				fd.append('uploadfile',imageName);
				fd.append('uploadfilename',imageName1);
				var foldername="businesses";
				$.ajax({
					url: $rootScope.communityContextPath+"/uploadFile/"+foldername,
					type: "POST",
					data: fd,
					contentType:false,
					cache: false,
					processData:false,
					success: function(data){		
						$scope.loading=false;
						swal("Thanks! Business information has been added successfully");
						//$location.path('/dashboard');
						$window.location.href = '#/dashboard';
						$window.location.reload();
					//	$scope.$apply();
					}
				});	
			}else{
				$scope.loading=false;
				swal("Thanks! Business information has been added successfully");
				$window.location.href = '#/dashboard';
				$window.location.reload();
				//$location.path('/dashboard');
			}
		});
	}



	$scope.updateCommunityBusiness = function(){
		if($scope.businessInfo.businessName==""){
			Notification.error({message:"Business name is mandatory",replaceMessage: true});
			return false;
		} 
		if($scope.businessInfo.emailAddress=="" && $scope.businessInfo.phoneNumber==""){
			Notification.error({message:"Contact information is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.businessInfo.emailAddress){
			Notification.error({message:"Invalid email address",replaceMessage: true});
			return false;
		}
		
		$scope.loading=true;
		var now = new Date();
		var fd = new FormData();
		var imageName=document.getElementById("proffile").files[0];
		if(imageName){
			var imageName1="business_"+now.getTime();
			$scope.businessInfo.communityImage=imageName1;
		}
		$http.post($rootScope.communityContextPath+"/businesses/"+$scope.businessInfo.id+"/updateBusiness",$scope.businessInfo).success(function(data){
			if(imageName){
				fd.append('uploadfile',imageName,imageName1);
				var foldername="businesses";
				$.ajax({
					url: $rootScope.communityContextPath+"/uploadFile/"+foldername,
					type: "POST",
					data: fd,
					contentType:false,
					cache: false,
					processData:false,
					success: function(data){		
						$scope.loading=false;
						swal("Thanks! Business information has been updated successfully");
						//$location.path('/dashboard');
						$window.location.href = '#/dashboard';
						$window.location.reload();
					//	$scope.$apply();
					}
				});	
			}else{
				$scope.loading=false;
				swal("Thanks! Business information has been updated successfully");
				//$location.path('/dashboard');
				$window.location.href = '#/dashboard';
				$window.location.reload();
			}
		});
	}


	$scope.changeProfImg = function(input) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#businessImage').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}

	$("#proffile").change(function() {
		$scope.changeProfImg(this);
	});
	$scope.showBrowseDialogCommunity=function(){
		$("#proffile").click();
	}

} ]);
