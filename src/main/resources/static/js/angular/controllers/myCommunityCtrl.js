//Author: Aishwarya Dhopate
angular.module('app').controller('myCommunityCtrl', [ '$scope', '$http', '$location', '$rootScope','$window','$timeout','service','Notification','$filter', function($scope, $http, $location, $rootScope, $window,$timeout, service,Notification,$filter) {
	var providerId = localStorage.getItem("providerId");
	$scope.communityPaths=$rootScope.communityPaths;
	$scope.consumerName=localStorage.getItem("consumerName");
	var consumerId=localStorage.getItem("consumerId");
	$scope.communityId=localStorage.getItem("communityManager");
	$scope.consumerID=consumerId;
	$scope.members=[];
	$scope.managerCommunity="";
	$scope.referralCode="";
	$scope.images=[];
	$scope.getMembersofCommunity=function(){
		$http.get($rootScope.communityContextPath+"/consumers/"+consumerId+"/memberlist/"+$scope.communityId).success(function(data){
			for(var index in data){
				var json = data[index];
				if(json.id==$scope.consumerID){		
					$scope.firstname=json.firstName;
					$scope.lastname=json.lastName;
					$scope.profileimg=json.profilePic;
					$scope.managerCommunity=json.managerCommunity;
				}
				$scope.members.push(json);
			}
			if ($(window).width() <= 992) {
				$(".tabHeightLeft").css("min-height",0);
			} else {
				var tabsHeights=$(".tab-content.active").height()-200;
				$(".tabHeightLeft").css("min-height",tabsHeights);
			}
		}).error(function(data){
			Notification.error(data.message);
		});
	};
	$scope.getMembersofCommunity();
	$timeout(function(){
		$http.get($rootScope.communityContextPath+"/communities/"+$scope.communityId+"/referral").success(function(data){
			$scope.referralCode=data;
		})
	},300);
	$scope.pages=[1,2,3];
	$scope.currentPage = 0;
	$scope.pageSize=3;
	$scope.posts=[];
	$scope.getPostsandEvents = function (tabid) {
		$scope.addActiveToTabs(tabid,'850');
		if($scope.posts==""){
			$scope.images=[];
			$timeout(function(){
				$http.get($rootScope.communityContextPath+"/communitymanager/posts/"+consumerId).success(function(data){
					for(var index in data){
						var json = data[index];
						if(json.image){
							var fileType=json.image.split("_");
							$scope.images.push(fileType[0]);
						}else{
							$scope.images.push(0);
						}
						$scope.posts.push(json);
					}

				}).error(function(data){
					Notification.error(data.message);
				});	},100);
			$timeout(function(){
				$http.get($rootScope.communityContextPath+"/communitymanager/events/"+consumerId).success(function(data){
					for(var index in data){
						var json = data[index];
						$scope.posts.push(json);
					}
					if($scope.posts!=""){
						$timeout(function() {
							var height=0;
							$('.frowNewsFeed').each(function(i, value){
							    height += parseInt($(this).height());
							});
							height += '';
							height=height+"px";
						$(".tabHeightLeft").css("min-height",height);
					},500);
//						$timeout(function () {
//							var divHeight=$('.frowDivs').height();
//							console.log(divHeight);
//							$(".tabHeightLeft").css("min-height",divHeight+40);
//						},500);
					}
				}).error(function(data){
					Notification.error(data.message);
				});	},300);			    		
		}else{
			var divHeight=$('.frowDivs').height();
			$(".tabHeightLeft").css("min-height",divHeight);
		}
	}

	$scope.numberOfPages=function(){
		return Math.ceil($scope.posts.length/$scope.pageSize);                
	}
	$scope.prevArrow=function(){
		if($scope.currentPage==0 || $scope.pages[0]<=1){
			return false;
		}else{
			$scope.currentPage=$scope.currentPage-1;
			$scope.pages.splice(0,3,$scope.pages[0]-1,$scope.pages[0],$scope.pages[1]);
		}
		$timeout(function () {
//			var divHeight=$('.frowDivs').height();
//			$(".tabHeightLeft").css("min-height",divHeight+40);
				var height=0;
				$('.frowNewsFeed').each(function(i, value){
				    height += parseInt($(this).height());
				});
				height += '';
				height=height+"px";
			$(".tabHeightLeft").css("min-height",height);
		});
	}
	$scope.nextArrow=function(){
		if($scope.currentPage >= $scope.numberOfPages()-1 || $scope.pages[2]>=$scope.numberOfPages()){
			return false;
		}else{
			$scope.currentPage=$scope.currentPage+1;
			$scope.pages.splice(0,3,$scope.pages[1],$scope.pages[2],$scope.pages[2]+1);
		}
		$timeout(function () {
//			var divHeight=$('.frowDivs').height();
//			$(".tabHeightLeft").css("min-height",divHeight+40);
			$timeout(function() {
				var height=0;
				$('.frowNewsFeed').each(function(i, value){
				    height += parseInt($(this).height());
				});
				height += '';
				height=height+"px";
			$(".tabHeightLeft").css("min-height",height);
		},500);
		});
	}
	$scope.showPostsofCurrentPg=function(page){
		if(page>$scope.numberOfPages()){
			return false;
		}else{
			$scope.currentPage=page-1;
		}
		$timeout(function () {
//			var divHeight=$('.frowDivs').height();
//			$(".tabHeightLeft").css("min-height",divHeight+40);
			$timeout(function() {
				var height=0;
				$('.frowDivs').each(function(i, value){
				    height += parseInt($(this).height());
				});
				height += '';
				height=height+"px";
			$(".tabHeightLeft").css("min-height",height);
		},500);
		});
	}


	$scope.showConfirm = function(value) {
		swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Remove",   closeOnConfirm: false }, function(){   
			var consumerDeleted=value;
			$http.post($rootScope.communityContextPath+"/consumers/removeConsumer/"+consumerDeleted+"/"+$scope.communityId).success(function(data) {
				swal("Removed!", "The user has been removed from community", "success");
				$scope.members=[];
				$scope.getMembersofCommunity();
			}).error(function(data, status) {
				console.log(JSON.stringify(data))
				Notification.error(data.message);
			});
		});

	};
	$scope.communityInfo=[];
	$scope.communitySectionsHidden="";
	$scope.communitySections=["News and Events","Featured Helpers","Featured Businesses","Needs"];
	$scope.communitySections1=["News and Events","Featured Helpers","Featured Businesses","Needs"];
	$scope.notselected=[];


	$scope.showSettings=function(tabid){
		$scope.addActiveToTabs(tabid,'750');
		if($scope.communityInfo==""){
			$http.get($rootScope.communityContextPath+"/communities/"+consumerId+"/communitySettings/"+$scope.communityId).success(function(data){
				$scope.communityInfo.push(data);
				
				if($scope.communityInfo[0].communityTitles){
					$scope.communitySections1=$scope.communityInfo[0].communityTitles.split(",");
					$scope.communitySections=$scope.communityInfo[0].communityTitles.split(",");	
				}
				if($scope.communityInfo[0].communityHiddenSections){
					$scope.communitySectionsHidden=$scope.communityInfo[0].communityHiddenSections.split(",");
					//$scope.notselected=$scope.communitySectionsHidden;
					for(var i=0;i<$scope.communitySectionsHidden.length;i++){
						var not=$scope.communitySections.indexOf($scope.communitySectionsHidden[i]);
						$scope.notselected.push(not);
					}
				}
			}).error(function(data){
				Notification.error(data.message);
			});
		}
	};

	$scope.notSelectedSection = function(index) {
		var pushedInd=$scope.notselected.indexOf(index);
		if(pushedInd=== -1){
			$scope.notselected.push(index);
		}else{
			$scope.notselected.splice(pushedInd,1);
		}
	}

	$scope.showSettingsForm=function(){
		$("#showCommunitySettings").hide();
		$("#editCommunitySettings").show();
	}

//	$scope.adjustTabHeight=function(id){
////	var tabid=$(".nav-tabs li a").attr("data-target");
////	var tabidmain=tabid.split("#");
//	var tabheight=$("div[id="+id+"]").outerHeight()+20;
//	$(".nav-tabs").css("min-height",tabheight); 
//	}
	$scope.settings={ 
			"id":$scope.communityId
	};
	$scope.submitSettingsInfo=function(tabid){
		var notsel=[];
		for(var i=0;i<$scope.notselected.length;i++){
			var ind=$scope.notselected[i];
			notsel.push($scope.communitySections[ind]);
		}
		$scope.settings.communityHiddenSections=notsel.join();		
		$scope.settings.communityType=$scope.communityInfo[0].communityType;
		$scope.settings.communityTitles=$scope.communitySections.join();
		if($scope.settings.communityType==""){
			Notification.error({message:"The community type field is mandatory",replaceMessage: true});
			return false;
		}
		if($scope.communitySections[0]=="" || $scope.communitySections[1]=="" || $scope.communitySections[2]=="" || $scope.communitySections[3]==""){
			Notification.error({message:"Dashboard titles can not be empty",replaceMessage: true});
			return false;
		}
		//var email=$scope.communityInfo[0].description;
//		if(email==""){
//			Notification.error({message:"The email address field is mandatory",replaceMessage: true});
//			return false;
//		}
		$http.post($rootScope.communityContextPath+"/communitymanager/settingsInfo/"+$scope.consumerID,$scope.settings).success(function(data) {
			$scope.communityInfo=[];
			if(!$scope.notselected){
				localStorage.setItem("HiddenHelper", ["1"]);  
				$rootScope.hiddenHelper=localStorage.getItem("HiddenHelper");
				window.location.href="#/myCommunity";
				$window.location.reload();
			}else{
				localStorage.setItem("HiddenHelper",notsel);
				$rootScope.hiddenHelper=localStorage.getItem("HiddenHelper");
				window.location.href="#/myCommunity";
				$window.location.reload();
			}
			$scope.notselected=[];
			$scope.showSettings(tabid);
		}).error(function(data, status) {
			console.log(JSON.stringify(data))
			Notification.error(data.message);
		});
	}

//	$scope.sharePost=function(postid){
//	service.post($rootScope.communityContextPath+"/communitymanager/sharePost/"+postid, "",'/dashboard');
//	}
//	$scope.shareEvent=function(eveid){
//	service.post($rootScope.communityContextPath+"/communitymanager/shareEvent/"+eveid, "",'/dashboard');
//	}
	$scope.event={
			title:"",
			location:"",
			eventImg:""
				}
	$scope.createEvent=function(commid){
		$scope.loading=true;
		$scope.event.communityID=commid;
		$scope.event.eventDate=$("#eventDates").val();
		$scope.event.startTime=$("#startTime").val();
		$scope.event.endTime=$("#endTime").val();
		$scope.event.consumerID=consumerId;

		var imageName=document.getElementById("proffile").files[0];
		var now = new Date();
		if(imageName){
			var imageName1="event_"+now.getTime();
			$scope.event.eventImg=imageName1;
		}
		if($scope.event.title==""){
			$scope.loading=false;
			Notification.error({message:"Title field is mandatory",replaceMessage: true});
			return false;
		}
		if($scope.event.location==""){
			$scope.loading=false;
			Notification.error({message:"Location field is mandatory",replaceMessage: true});
			return false;
		}
		if($scope.event.eventDate==""){
			$scope.loading=false;
			Notification.error({message:"Event date field is mandatory",replaceMessage: true});
			return false;
		}
		if($scope.event.startTime==""){
			$scope.loading=false;
			Notification.error({message:"Event's start time field is mandatory",replaceMessage: true});
			return false;
		}
		if($scope.event.endTime==""){
			$scope.loading=false;
			Notification.error({message:"Event's end time field is mandatory",replaceMessage: true});
			return false;
		}
		 var dd = now.getDate();
		 var mm = now.getMonth()+1;
		 var yyyy = now.getFullYear();
		 if(mm.toString().length<2){
			 var zeroValm=0;
		 }
		 if(dd.toString().length<2){
			 var zeroVald=0;
		 }
		 todayDate=yyyy+"-"+zeroValm+mm+"-"+zeroVald+dd;
		 var a=$scope.event.startTime.split(" ");
			var startTimeFinal=a[0]+a[1]+a[2]+" "+a[3];
			startTimeFinal=new Date(Date.parse($scope.event.eventDate.replace(/-/g, ' ') + " " +startTimeFinal));
		 if(Date.parse($scope.event.eventDate)==Date.parse(todayDate)){
			 var hours = now.getHours();
			 var minutes = now.getMinutes();
			 var ampm = hours >= 12 ? 'PM' : 'AM';
			 hours = hours % 12;
			 hours = hours ? hours : 12; // the hour '0' should be '12'
			 minutes = minutes < 10 ? '0'+minutes : minutes;
			 var strTime = hours + ':' + minutes + ' ' + ampm;
			 strTime=new Date(Date.parse(todayDate.replace(/-/g, ' ') + " " +strTime));
			 if(startTimeFinal<strTime){
				 $scope.loading=false;
				 Notification.error({message:"Event's start time should be greater than current time",replaceMessage: true});
				 return false;		
			 }
		 }
		
		var b=$scope.event.endTime.split(" ");
		var endTimeFinal=b[0]+b[1]+b[2]+" "+b[3];
		endTimeFinal=new Date(Date.parse($scope.event.eventDate.replace(/-/g, ' ') + " " +endTimeFinal));
		if(startTimeFinal>endTimeFinal){
			$scope.loading=false;
			Notification.error({message:"Event's end time should be greater than start time",replaceMessage: true});
			return false;		
		}
		$http.post($rootScope.communityContextPath+"/communitymanager/postEvent/",$scope.event).success(function(data) {
			if(imageName){
				var fd = new FormData();    
				fd.append('uploadfile',imageName);
				fd.append('uploadfilename',imageName1);
				var folderName="events";
				$.ajax({
					url: $rootScope.communityContextPath+"/uploadFile/"+folderName,
					type: "POST",
					data: fd,
					contentType:false,
					cache: false,
					processData:false,
					success: function(data){
						$scope.loading=false;
						$scope.posts=[];
						$scope.getPostsandEvents('news');
						$("#popup-event").modal("hide");
						angular.copy({},$scope.event);
						$("#linkedEventImg").text("");
					}
				});	
			}else{
				$scope.loading=false;
				$scope.posts=[];
				$scope.getPostsandEvents('news');
				$("#popup-event").modal("hide");
				angular.copy({},$scope.event);
			}

		}).error(function(data, status) {
			console.log(JSON.stringify(data))
			Notification.error(data.message);
		});
	}

	$scope.showBrowseDialog=function(){
		$("#proffile").click();
	}

	$("#proffile").change(function(e) {
		$("#linkedEventImg").text(e.target.files[0].name);
	});

	$scope.post={
			image:"",
			description:"",
			communityID:""
				}
	$scope.createPost=function(commid){
		$scope.loading=true;
		$scope.post.communityID=commid;
		$scope.post.consumerID=consumerId;
		var imageName=document.getElementById("postImgfile").files[0];
		if(imageName || $scope.post.description!=""){
			var now = new Date();
			if(imageName){
				var fileType=document.getElementById("postImgfile").files[0].type.split("/");
				if(fileType[0]=="image"){
					var imageName1="image_post_"+now.getTime();
				}else if(fileType[0]=="video"){
					var imageName1="video_post_"+now.getTime();
				}
				$scope.post.image=imageName1;
			}
			$http.post($rootScope.communityContextPath+"/communitymanager/createPost/",$scope.post).success(function(data) {
				if(imageName){
					var fd = new FormData();    
					fd.append('uploadfile',imageName);
					fd.append('uploadfilename',imageName1);
					var folderName="posts";
					$.ajax({
						url: $rootScope.communityContextPath+"/uploadFile/"+folderName,
						type: "POST",
						data: fd,
						contentType:false,
						cache: false,
						processData:false,
						success: function(data){  
							$scope.loading=false;
							$scope.posts=[];
							$scope.getPostsandEvents('news');
							$("#popup-post").modal("hide");
							angular.copy({},$scope.post);
							$("#linkedPostImg").text("");
						}
					});	
				}else{
					$scope.loading=false;
					$scope.posts=[];
					$scope.getPostsandEvents('news');
					$("#popup-post").modal("hide"); 
					angular.copy({},$scope.post);

				}
			}).error(function(data, status) {
				console.log(JSON.stringify(data))
				Notification.error(data.message);
			});
		}else{
			$scope.loading=false;
			Notification.error({message:"Post can not be empty",replaceMessage: true});
		}
	}

	$("#postImgfile").change(function(e) {
		$("#linkedPostImg").text(e.target.files[0].name);
	});	
	$scope.showBrowseDialogForPost=function(){
		$("#postImgfile").click();
	}
	$scope.eventsRsvps="";
	$scope.showEvents=function(tabid){
		$scope.addActiveToTabs(tabid,'470')
		$http.get($rootScope.communityContextPath+"/communitymanager/managerrsvp/"+$scope.communityId).success(function(data){
			$scope.eventsRsvps=data;
		}).error(function(data){
			Notification.error(data.message);
		});	
	}
	$scope.emailAddr="";
	$scope.inviteToCommunity=function(){
		$scope.referralCode
		if($scope.emailAddr==""){
			Notification.error({message:"Email field is mandatory",replaceMessage: true});
			return false;
		}
		$http.post($rootScope.communityContextPath+"/consumers/invitetoCommunity/"+$scope.communityId+"/"+$scope.consumerName,$scope.emailAddr).success(function(data) {
			$("#inviteToCommunity").modal("hide");
			swal("Thanks!");
			$scope.emailAddr= "";
		});
	}
	$scope.communityImage="";
	$scope.communitySponsorImage="";
	$scope.getHeaderImagesByCommunity=function(tabid){
		$scope.addActiveToTabs(tabid,'1000'); //680
		$http.get($rootScope.communityContextPath+"/communities/communityImages/"+$scope.communityId).success(function(data){
			var communityImage1=data[0];
			if(communityImage1!=null){
				$scope.communityImage=communityImage1.split(",");
				$.each($scope.communityImage,function(i,file){
					var filename=file+"?"+new Date().getTime();
					$scope.communityImage.splice(i,1,filename);
				});
				$scope.communityImageFlag=false;
			}else{
				$scope.communityImageFlag=true;
				$scope.communityImage=['images/dashboard-header-img-3.jpg','images/dashboard-header-img-7.jpg','images/dashboard-header-img-4.jpg',
				                       'images/dashboard-header-img-6.jpg','images/dashboard-header-img-8.jpg','images/dashboard-header-img-9.jpg',
				                       'images/dashboard-header-img-10.jpg'];
			}
			if(data[1]){
				$scope.communitySponsorImage=data[1].split(",");
				$.each($scope.communitySponsorImage,function(i,file){
					var filename=file;
					$scope.communitySponsorImage.splice(i,1,filename);
				});
			}
		});


	}
	$scope.submitCommunityImages = function(){
		if($scope.imageSelected.length<7 && $scope.communityImageFlag){
			Notification.error({message:"Please select all files",replaceMessage: true});
		}else{
			$scope.loading=true;
			var fd = new FormData();  
			$.each($("input[name^=commimages]"), function(i, obj) {
				var ind=i;
				$.each(obj.files,function(i,file){
					var index=ind+1;
					var communityname='community'+index;
					fd.append('uploadfile',file,communityname);
					if($scope.communityImageFlag){
						$scope.communityImage.splice(ind,1,communityname);
					}
				});
			});

			if($scope.communityImageFlag){
				fd.append('imagePaths',$scope.communityImage);
			}else{
				fd.append('imagePaths',"");
			}

			$.ajax({
				url: $rootScope.communityContextPath+"/uploadCommunity/"+$scope.communityId,
				type: "POST",
				data: fd,
				contentType:false,
				cache: false,
				processData:false,
				success: function(data){
					$scope.loading=false;
					$window.location.reload();
				}
			});	
		}
	}

	$scope.submitCommunitySponsorImages = function(element){
		if($scope.communitySponsorImage.length==5){
			Notification.error({message:"You can add up to five sponsors",replaceMessage: true});
			return false;
		}
		$scope.loading=true;
		$scope.selectedSponsorImage=element.files[0].name;
		var imageName=element.files[0];
		var imageNames="sponsorlogo_"+new Date().getTime();
		var fd = new FormData();  
		fd.append('uploadfile',imageName,imageNames);
		if(!$scope.communitySponsorImage){
			$scope.communitySponsorImage=[];
		}
		$scope.communitySponsorImage.push(imageNames);
		fd.append('imagePaths',$scope.communitySponsorImage);
		$.ajax({
			url: $rootScope.communityContextPath+"/uploadCommunitySponsors/"+$scope.communityId,
			type: "POST",
			data: fd,
			contentType:false,
			cache: false,
			processData:false,
			success: function(data){
				$scope.loading=false;
				$scope.$apply();
			}
		});	

	}
	$scope.deleteSponsorImage=function(ind){
		var imgDelete=$scope.communitySponsorImage[ind];
		var url = $rootScope.communityContextPath+"/communities/delSponsor/"+$scope.communityId+"/"+imgDelete;
		swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Delete",   closeOnConfirm: true }, function(){
			$scope.communitySponsorImage.splice(ind,1);
			var data = {imagePaths:$scope.communitySponsorImage}
			$http({
				url: url,
				method: "POST",
				params: data
			})
		});
	}
	$scope.selectedSponsorImage="";


	$scope.showBrowseDialogSponsors=function(){
		$("#file-fake").click();
	}



	$scope.showBrowseDialogCommunity=function(index){
		$("#proffile"+index).click();
	}
	$scope.imageSelected=[];
	$scope.showImageSelected=function(element){
		var index = angular.element(element).scope().$index;
		index=index+1;
		if($scope.imageSelected.indexOf(index)== -1){
			$scope.imageSelected.push(index);
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#communityImg'+index).attr('src', e.target.result);
		}
		reader.readAsDataURL(element.files[0]);
	}

	$scope.addActiveToTabs=function(tabid,heights){
		$('.tab').removeClass('active');
		$("#"+tabid+"tab").addClass('active');
		$('.tab-content').removeClass('active');
		$("#"+tabid).addClass('tab-content active');
		if ($(window).width() <= 992) {
			$(".tabHeightLeft").css("min-height",0);
		}else {
			var tabsHeights=heights-300;
			$(".tabHeightLeft").css("min-height",tabsHeights);
		}
	}

	$scope.deletePost = function(value,imgPath) {
		swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Remove",   closeOnConfirm: false }, function(){   
			var consumerDeleted=value;
			if(!imgPath){
				imgPath='noimage'; 
			}
			$http.post($rootScope.communityContextPath+"/communitymanager/"+consumerDeleted+"/"+imgPath+"/deletePost").success(function(data) {
				swal("Deleted!", "The post has been deleted successfully");
				$scope.posts=[];
				$scope.currentPage = 0;
				$scope.pages=[1,2,3];
				$scope.getPostsandEvents('news');
			}).error(function(data, status) {
				console.log(JSON.stringify(data))
				Notification.error(data.message);
			});
		});

	};

	$scope.deleteEvent = function(value,imgPath) {
		swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Remove",   closeOnConfirm: false}, function(){   
			var consumerDeleted=value;
			if(!imgPath){
				imgPath='noimage'; 
			}
			$http.post($rootScope.communityContextPath+"/communitymanager/"+consumerDeleted+"/"+imgPath+"/deleteEvent").success(function(data) {
				swal("Deleted!", "The event has been deleted successfully");
				$scope.posts=[];
				$scope.currentPage = 0;
				$scope.pages=[1,2,3];
				$scope.getPostsandEvents('news');
			}).error(function(data, status) {
				console.log(JSON.stringify(data))
				Notification.error(data.message);
			});
		});

	};

	$scope.commneedslist=[];
	$scope.getlistOfCommunityNeeds=function(tabid){
		$scope.addActiveToTabs(tabid,'430');
		$http.get($rootScope.communityContextPath+"/communityneeds/"+$scope.communityId).success(function(data){
			$scope.commneedslist=[];
			for(var index in data){
				var json = data[index];
				$scope.commneedslist.push(json);
			}
		}).error(function(data){
			Notification.error(data.message);
		});

		$scope.communityneedsdata={
				communityId:$scope.communityId
		};
	}
	$scope.submitNeedsData=function(tabname){
		if(!$scope.communityneedsdata.needName){
			Notification.error({message:"Community needs field is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.communityneedsdata.quantity){
			$scope.communityneedsdata.quantity=0;
		}
		$http.post($rootScope.communityContextPath+"/communityneeds/",$scope.communityneedsdata).success(function(data) {
			$scope.commneedslist=[];
			$scope.getlistOfCommunityNeeds("needs");
		});
	}

	$scope.deleteNeed=function(needid){
		$http.post($rootScope.communityContextPath+"/communityneeds/"+needid,$scope.communityneedsdata).success(function(data) {
			swal("Deleted!", "Successfully deleted", "success");
			$scope.commneedslist=[];
			$scope.getlistOfCommunityNeeds("needs");
		});
	}
	$scope.busipages=[1,2,3];
	$scope.busicurrentPage = 0;
	$scope.businesses=[];
	$scope.getBusinesses=function(tabid){
		$scope.addActiveToTabs(tabid,'850');
		$http.get(
				$rootScope.communityContextPath
						+ "/businesses/" + $scope.communityId+"/byCommunityId")
				.success(function(data) {
						$scope.businesses=data;
					var height = 0;
//						for(var index in data){
//							var json = data[index];
//							$scope.businesses.push(json);
//							$timeout(function() {
//							  height += parseInt($('.frow-business-box').height());
//							},0);
//						}
						$timeout(function() {
//							for (i = 0; i < 3; i++) { 
//								 height += parseInt($('.frow-box').height());	
//								 alert($('.frow-box').innerHeight());
//							}
							$('.frow-business-box').each(function(i, value){
							    height += parseInt($(this).height());
							});
							height += '';
							height=height+"px";
						$(".tabHeightLeft").css("min-height",height);
						},0);

				}).error(function(data) {
					console.log(JSON.stringify(data))
				})
	}
	$scope.numberOfPagesBusiness=function(){
		return Math.ceil($scope.businesses.length/$scope.pageSize);                
	}
	$scope.prevArrowBusiness=function(){
		if($scope.busicurrentPage==0 || $scope.busipages[0]<=1){
			return false;
		}else{
			$scope.busicurrentPage=$scope.busicurrentPage-1;
			$scope.pages.splice(0,3,$scope.busipages[0]-1,$scope.busipages[0],$scope.busipages[1]);
		}
		$timeout(function() {
			var height=0;
			$('.frow-business-box').each(function(i, value){
			    height += parseInt($(this).height());
			});
			height += '';
			height=height+"px";
		$(".tabHeightLeft").css("min-height",height);
	},0);
//		$timeout(function () {
//			var divHeight=$('.frowDivs').height();
//			$(".tabHeightLeft").css("min-height",divHeight+40);
//		});
	}
	$scope.nextArrowBusiness=function(){
		if($scope.busicurrentPage >= $scope.numberOfPagesBusiness()-1 || $scope.busipages[2]>=$scope.numberOfPagesBusiness()){
			return false;
		}else{
			$scope.currentPage=$scope.currentPage+1;
			$scope.pages.splice(0,3,$scope.busipages[1],$scope.busipages[2],$scope.busipages[2]+1);
		}
		$timeout(function() {
			var height=0;
			$('.frow-business-box').each(function(i, value){
			    height += parseInt($(this).height());
			});
			height += '';
			height=height+"px";
		$(".tabHeightLeft").css("min-height",height);
	},0);
//		$timeout(function () {
//			var divHeight=$('.frowDivs').height();
//			$(".tabHeightLeft").css("min-height",divHeight+40);
//		});
	}
	$scope.showBusinessofCurrentPg=function(page){
		if(page>$scope.numberOfPagesBusiness()){
			return false;
		}else{
			$scope.busicurrentPage=page-1;
		}
		$timeout(function() {
			var height=0;
			$('.frow-business-box').each(function(i, value){
			    height += parseInt($(this).height());
			});
			height += '';
			height=height+"px";
		$(".tabHeightLeft").css("min-height",height);
	},0);
	}
	$scope.businessInfo={
			emailAddress:""
				};
	$scope.update=false;
	$scope.createCommunityBusiness = function(){
		if(!$scope.businessInfo.businessName){
			Notification.error({message:"Business name is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.businessInfo.emailAddress && !$scope.businessInfo.phoneNumber){
			Notification.error({message:"Contact information is mandatory",replaceMessage: true});
			return false;
		}

		$scope.loading=true;
		var now = new Date();
		var fd = new FormData();
		var imageName=document.getElementById("busifile").files[0];
		if(imageName){
			var imageName1="business_"+now.getTime();
			$scope.businessInfo.communityImage=imageName1;
		}
		$http.post($rootScope.communityContextPath+"/businesses/"+consumerId,$scope.businessInfo).success(function(data){
			if(imageName){
				fd.append('uploadfile',imageName);
				fd.append('uploadfilename',imageName1);
				var foldername="businesses";
				$.ajax({
					url: $rootScope.communityContextPath+"/uploadFile/"+foldername,
					type: "POST",
					data: fd,
					contentType:false,
					cache: false,
					processData:false,
					success: function(data){		
						$scope.loading=false;
						swal("Thanks! Business information has been added successfully");
						$scope.busipages=[1,2,3];
						$scope.busicurrentPage = 0;
						$scope.businesses=[];
						$scope.getBusinesses('business');
						$("#popup-business").modal('hide');
						//$location.path('/dashboard');
					//	$window.location.href = '#/dashboard';
					//	$window.location.reload();
					//	$scope.$apply();
					}
				});	
			}else{
				$scope.loading=false;
				swal("Thanks! Business information has been added successfully");
				$scope.busipages=[1,2,3];
				$scope.busicurrentPage = 0;
				$scope.businesses=[];
				$scope.getBusinesses('business');
				$("#popup-business").modal('hide');
			//	$window.location.href = '#/dashboard';
			//	$window.location.reload();
				//$location.path('/dashboard');
			}
		});
	}
	$scope.createBusiness=function(){
		$scope.update=false;
		$scope.businessInfo="";
		$("#popup-business").modal('show');
	}
	$scope.updateBusiness=function(businessid){
		$http.get($rootScope.communityContextPath+"/businesses/"+businessid+"/byBusinessId").success(function(data){
		$scope.businessInfo=data;
		$scope.update=true;
		$("#popup-business").modal('show');
		});
	}
	$scope.updateCommunityBusiness = function(businessid){
		if($scope.businessInfo.businessName==""){
			Notification.error({message:"Business name is mandatory",replaceMessage: true});
			return false;
		} 
		if($scope.businessInfo.emailAddress=="" && ($scope.businessInfo.phoneNumber==null || $scope.businessInfo.phoneNumber=="")){
			Notification.error({message:"Contact information is mandatory",replaceMessage: true});
			return false;
		}
		if(!$scope.businessInfo.emailAddress && $scope.businessInfo.emailAddress!=""){
			Notification.error({message:"Invalid email address",replaceMessage: true});
			return false;
		}
		
		$scope.loading=true;
		var now = new Date();
		var fd = new FormData();
		var imageName=document.getElementById("busifile").files[0];
		if(imageName){
			var imageName1="business_"+now.getTime();
			$scope.businessInfo.communityImage=imageName1;
		}
		$http.post($rootScope.communityContextPath+"/businesses/"+$scope.businessInfo.id+"/updateBusiness",$scope.businessInfo).success(function(data){
			if(imageName){
				fd.append('uploadfile',imageName);
				fd.append('uploadfilename',imageName1);
				var foldername="businesses";
				$.ajax({
					url: $rootScope.communityContextPath+"/uploadFile/"+foldername,
					type: "POST",
					data: fd,
					contentType:false,
					cache: false,
					processData:false,
					success: function(data){		
						$scope.loading=false;
						swal("Thanks! Business information has been updated successfully");
						$scope.busipages=[1,2,3];
						$scope.busicurrentPage = 0;
						$scope.businesses=[];
						$scope.getBusinesses('business');
						$("#popup-business").modal('hide');
						//$location.path('/dashboard');
					//	$window.location.href = '#/dashboard';
					//	$window.location.reload();
					//	$scope.$apply();
					}
				});	
			}else{
				$scope.loading=false;
				swal("Thanks! Business information has been updated successfully");
				$scope.busipages=[1,2,3];
				$scope.busicurrentPage = 0;
				$scope.businesses=[];
				$scope.getBusinesses('business');
				$("#popup-business").modal('hide');
				//$location.path('/dashboard');
				//$window.location.href = '#/dashboard';
			//	$window.location.reload();
			}
		});
	}
	$scope.deleteBusiness=function(businessid,imgPath){
		swal({title: "Are you sure?", type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Remove",   closeOnConfirm: false }, function(){   
			if(!imgPath){
				imgPath='noimage'; 
			}
			$http.post($rootScope.communityContextPath+"/businesses/"+businessid+"/"+imgPath+"/deleteBusiness").success(function(data) {
				swal("Deleted!", "The business has been deleted successfully");
				$scope.businesses=[];
				$scope.busicurrentPage = 0;
				$scope.busipages=[1,2,3];
				$scope.getBusinesses('business');
			}).error(function(data, status) {
				console.log(JSON.stringify(data))
				Notification.error(data.message);
			});
		});
	}
	$scope.changeBusinessImg = function(input) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#businessImage').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}

	$("#busifile").change(function() {
		$scope.changeBusinessImg(this);
	});
	$scope.showBrowseDialogBusiness=function(){
		$("#busifile").click();
	}
} ]);

//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
angular.module('app').filter('startFrom', function() {
	return function(input, start) {
		start = +start; //parse to int
		return input.slice(start);
	}
});