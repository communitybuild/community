$(document).ready(function() {

	/***************** Waypoints ******************/

//	$('.wp1').waypoint(function() {
//		$('.wp1').addClass('animated fadeInUp');
//	}, {
//		offset: '75%'
//	});
//	$('.wp2').waypoint(function() {
//		$('.wp2').addClass('animated fadeInUp');
//	}, {
//		offset: '75%'
//	});
//	$('.wp3').waypoint(function() {
//		$('.wp3').addClass('animated fadeInRight');
//	}, {
//		offset: '75%'
//	});

	/***************** Initiate Flexslider ******************/
	
	/***************** Initiate Fancybox ******************/

//	$('.single_image').fancybox({
//		padding: 4,
//	});

	/***************** Tooltips ******************/
    $('[data-toggle="tooltip"]').tooltip();

	/***************** Nav Transformicon ******************/



	/***************** Header BG Scroll ******************/

	$(function() {
		$(window).scroll(function() {
			var scroll = $(window).scrollTop();
			if (scroll >= 20) {
				$('section.navigation').addClass('fixed');
				$('header').css({
					"border-bottom": "none",
					"padding": "20px 0"
				});
				$('header .member-actions').css({
					"top": "20px",
				});
				$('header .navicon').css({
					"top": "34px",
				});
				$('#indexPage header ul.primary-nav li a').css("color","#333");
				$('#indexPage header ul.member-actions li a').css("color","#333");
				$('#indexPage #communityLandingHero header ul.member-actions li a').css({"color":"#fff",'border-color':"#4d4d4d"});
				$('.indexPagelogo').attr("src","images/logo-color.png");
				$('#indexPage .logo img').css("height","45px");
				$("#indexPage .nav-toggle span").css("background","black");
			//	$('body').append('<style>#indexPage .nav-toggle span:before</style>');
			//	$('body').append('<style>#indexPage .nav-toggle span:after</style>');
			} else {
				$('section.navigation').removeClass('fixed');
				$('header').css({
					"border-bottom": "solid 1px rgba(255, 255, 255, 0.2)",
					"padding": "20px 0"
				});
				$('header .member-actions').css({
					"top": "20px",
				});
				$('header .navicon').css({
					"top": "48px",
				});
				$('.indexPagelogo').attr("src","images/logo-white.png");
				$('#indexPage header ul.primary-nav li a').css("color","#fff");
				$('#indexPage header ul.member-actions li a').css("color","#fff");
				$('#indexPage #communityLandingHero header ul.member-actions li a').css({'border-color':"#fff"});
				$('#plansandpricingpage .indexPagelogo').attr("src","images/logo-color.png");
				$('#indexPage #plansandpricingpage ul.primary-nav li a').css("color","#333");
				$('#indexPage #plansandpricingpage ul.member-actions li a').css("color","#333");
				$("#indexPage .nav-toggle span").css("background","#fff");
				$('body').append('<style>#indexPage .nav-toggle span:before</style>');
				$('body').append('<style>#indexPage .nav-toggle span:after</style>');
			}
		});
	});
	



});