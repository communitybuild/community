package com.community.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.community.controller.FileUploadController;
import com.community.model.Community;
import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;
import com.community.resource.ConsumerResource;
import com.community.service.AccountService;
import com.community.service.CommunityService;
import com.community.service.ConsumerService;
import com.community.util.HashingUtil;
import com.community.util.EmailSending;

/**
 * 
 * @author Sameer Shukla 
 * 
 * Controller for handling consumers in the system.
 * Links tag in the response represents hyperlinks to related entities of a Consumer.
 * 
 * 
 * 
 */
@RestController
@RequestMapping("/consumers")
public class ConsumerController extends BaseController {

	@Autowired
	private ConsumerService consumerService;

	@Autowired
	private CommunityService communityService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private FileUploadController fileUploadController;


	@Value("${emailTemplatesPath}")
	private String emailTemplatesPath;

	@Value("${mailFromAddress}")
	private String mailFromAddress;

	@Value("${communityMainPath}")
	private String communityMainPath;

	private ConsumerResource convertEntityToResource(Consumer consumer) {
		ConsumerResource resource = modelMapper.map(consumer, ConsumerResource.class);
		return resource;
	}
	private Consumer convertResourceToEntity(ConsumerResource consumerResource) {
		Consumer consumer = modelMapper.map(consumerResource, Consumer.class);
		return consumer;
	}
	//	private ConsumerCommunityResource convertEntityToResource(ConsumerCommunity community) {
	//		ConsumerCommunityResource resource = modelMapper.map(community, ConsumerCommunityResource.class);
	//		return resource;
	//	}
	/**
	 * Fetch all the Consumers in the System
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<ConsumerResource> getConsumers() throws Exception {
		Iterable<? extends Consumer> consumers = consumerService.getAllConsumers();
		List<ConsumerResource> resources = new ArrayList<ConsumerResource>();
		for (Consumer consumer : consumers) {
			ConsumerResource resource = convertEntityToResource(consumer);
			Link detail = linkTo(methodOn(ConsumerController.class).findByConsumerId(
					consumer.getId())).withSelfRel();
			resource.add(detail);
			resources.add(resource);
		}
		return resources;
	}

	/**
	 * Fetch Consumer by Id
	 */
	@RequestMapping(value = "/{consumerId}", method = RequestMethod.GET)
	public ConsumerResource findByConsumerId(@PathVariable Long consumerId) throws Exception {		
		Consumer consumer = consumerService.findByConsumerId(consumerId);
		if (consumer == null) {
			throw new IllegalArgumentException("Consumer with id " 
					+ consumerId + " does not exist in system");
		} else {
			List<ConsumerCommunity> community=this.consumerService.findCommunity(consumerId);
			if(community.isEmpty()){
				ConsumerResource resource = convertEntityToResource(consumer);
				resource.setEntityId(consumerId);
				resource.setCommunityType("notactive");
				return resource;
			}else{
				for(ConsumerCommunity consumerCommunity:community){
					Community community1=consumerCommunity.getCommunityID();
					ConsumerResource resource = convertEntityToResource(consumer);
					resource.setCommunityType(community1.getCommunityHiddenSections());
					resource.setEntityId(consumerId);
					return resource;
				}
			}
		}
		return null;
	}

	/**
	 * Fetch Consumer by email address
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ConsumerResource findByConsumerEmailAddress(@RequestParam String emailAddress) 
			throws Exception 
	{		
		Consumer consumer = consumerService.findByConsumerEmailAddress(emailAddress);
		if (consumer == null) {
			throw new IllegalArgumentException("Consumer with the email address '" 
					+ emailAddress + "' does not exist in system");
		} else {
			ConsumerResource resource = convertEntityToResource(consumer);
			Link detail = linkTo(ConsumerController.class).slash(
					consumer.getId()).withSelfRel();
			resource.add(detail);
			return resource;
		}		
	}	

	/**
	 * Create Consumer
	 * 
	 * @param consumer
	 * @return
	 * @throws Exception
	 * 
	 * Changes in a method by Aishwarya to insert entry in consumer_communities table also
	 */
	@RequestMapping(value="/register/{referralCode}", method = RequestMethod.POST)
	public HttpEntity<Consumer> create(@PathVariable String referralCode,@Validated @RequestBody 
			ConsumerResource consumerResource,HttpServletResponse response) throws Exception 
			{
		String emailAddress = consumerResource.getEmailAddress();
		Consumer existingConsumer = consumerService.findByConsumerEmailAddress(emailAddress);
		if (existingConsumer != null) {
			response.sendError(0, "Consumer with email address "+ emailAddress+ " already exists in system");
			return null;
			}
		Consumer savedEntity = consumerService.create(convertResourceToEntity(consumerResource));
		Community community=communityService.findByReferralCode(referralCode);
		consumerService.createEntryInConsumerCommunity(savedEntity.getId(), community.getId());
		ConsumerResource resource = convertEntityToResource(savedEntity);
		Link detail = linkTo(methodOn(ConsumerController.class).findByConsumerId(
				savedEntity.getId())).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<Consumer>(savedEntity, HttpStatus.CREATED);
			}


	/**
	 * Update Consumer
	 * 
	 * @param consumer id, updated consumer details
	 * @return
	 * @throws Exception
	 * 
	 * Changes by Aishwarya to update password in account table as well as in consumer table if changed by user
	 */
	@RequestMapping(value = "/{consumerId}", method = RequestMethod.POST)
	public HttpEntity<ConsumerResource> update(@PathVariable Long consumerId,@Validated @RequestBody ConsumerResource consumerResource) throws Exception 
	{
		Consumer existingConsumer = consumerService.findByConsumerId(consumerId);
		if (!StringUtils.equals(consumerResource.getPassword(), existingConsumer.getPassword())) {
			String hashedPassword = HashingUtil.convertToMd5HashString(consumerResource.getPassword());
			consumerResource.setPassword(hashedPassword);
			String password=consumerResource.getPassword();
			String userName1=existingConsumer.getEmailAddress();
			accountService.updatePasswordByUserName(password, userName1);
		}
//		if (!StringUtils.equals(consumerResource.getEmailAddress(), existingConsumer.getEmailAddress())) {
//			String email=consumerResource.getEmailAddress();
//			String userName=existingConsumer.getEmailAddress();
//			accountService.updateEmailByUserName(email, userName);
//		}
		Consumer consumer=consumerService.update(consumerId,convertResourceToEntity(consumerResource));
		ConsumerResource resource=convertEntityToResource(consumer);
		return new ResponseEntity<ConsumerResource>(resource,HttpStatus.OK);
	}

	/**
	 * Delete Consumer
	 * 
	 * @param consumer id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{consumerId}", method = RequestMethod.DELETE)
	public HttpEntity<String> delete(@PathVariable Long consumerId) throws Exception {
		String result = consumerService.delete(consumerId);
		return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
	}

	/**
	 * Get the associated communities for a particular Consumer
	 * 
	 * @param consumer id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{consumerId}/communities", method = RequestMethod.GET)
	public HttpEntity<ConsumerResource> getConsumerAssociatedCommunities(
			@PathVariable Long consumerId) throws Exception 
			{
		Consumer consumer = consumerService.findByConsumerId(consumerId);
		if (consumer == null) {
			throw new IllegalArgumentException("Consumer with id " 
					+ consumerId + " does not exist in system");
		} else {
			ConsumerResource resource = convertEntityToResource(consumer);			
			Link detail = linkTo(methodOn(ConsumerController.class).getConsumerAssociatedCommunities(
					consumerId)).withRel("Associated Communities");
			resource.add(detail);	
			return new ResponseEntity<ConsumerResource>(resource, HttpStatus.OK);
		}
			}

	/**
	 * Associate Consumer with a particular community
	 * 
	 * @param consumer id, community id
	 * @return
	 * @throws Exception
	 */
	//	@RequestMapping(value = "/{consumerId}/communities/{communityId}", method = RequestMethod.POST)
	//	public HttpEntity<ConsumerResource> associateConsumerToCommunity(@PathVariable Long consumerId, 
	//			@PathVariable Long communityId) throws Exception 
	//	{
	//		Consumer consumer = consumerService.findByConsumerId(consumerId);
	//		if (consumer == null) {
	//			throw new IllegalArgumentException("Consumer with id " 
	//					+ consumerId + " does not exist in system");
	//		} else {
	//			Community community = communityService.findByCommunityId(communityId);
	//			if (community == null) {
	//				throw new IllegalArgumentException("Community with id " 
	//						+ communityId + " does not exist in system");
	//			} else {
	//				Consumer savedEntity = consumerService.associateConsumerToCommunity(consumer, community);
	//				ConsumerResource resource = convertEntityToResource(savedEntity);			
	//				Link detail = linkTo(methodOn(ConsumerController.class).getConsumerAssociatedCommunities(
	//						consumerId)).withRel("Associated Communities");
	//				resource.add(detail);	
	//				return new ResponseEntity<ConsumerResource>(resource, HttpStatus.OK);
	//			}
	//		}
	//	}

	/**
	 * Remove Consumer from a particular community
	 * 
	 * @param consumer id, community id
	 * @return
	 * @throws Exception
	 */
	//	@RequestMapping(value = "/{consumerId}/communities/{communityId}", method = RequestMethod.DELETE)
	//	public HttpEntity<ConsumerResource> removeConsumerFromCommunity(@PathVariable Long consumerId, 
	//			@PathVariable Long communityId) throws Exception 
	//	{
	//		Consumer consumer = consumerService.findByConsumerId(consumerId);
	//		if (consumer == null) {
	//			throw new IllegalArgumentException("Consumer with id " 
	//					+ consumerId + " does not exist in system");
	//		} else {
	//			Community community = communityService.findByCommunityId(communityId);
	//			if (community == null) {
	//				throw new IllegalArgumentException("Community with id " 
	//						+ communityId + " does not exist in system");
	//			} else {
	//				Consumer savedEntity = consumerService.removeConsumerFromCommunity(consumer, community);
	//				ConsumerResource resource = convertEntityToResource(savedEntity);			
	//				Link detail = linkTo(methodOn(ConsumerController.class).getConsumerAssociatedCommunities(
	//						consumerId)).withRel("Associated Communities");
	//				resource.add(detail);	
	//				return new ResponseEntity<ConsumerResource>(resource, HttpStatus.OK);
	//			}
	//		}
	//	}	
	//	Code by Aishwarya to fetch members of particular community
	@RequestMapping(value = "/{consumerId}/memberlist/{communityId}", method = RequestMethod.GET)
	public List<ConsumerResource> findConsumersOfCommunity(@PathVariable Long consumerId,@PathVariable Long communityId) throws Exception 
	{
		List<ConsumerResource> resources = new ArrayList<ConsumerResource>();
		List<ConsumerCommunity> consumer = consumerService.findConsumersOfCommunity(communityId);
		for (ConsumerCommunity consumer1 : consumer) {
			Long consumerid=consumer1.getConsumerID();
			Consumer consumer11 = consumerService.findByConsumerId(consumerid);
			ConsumerResource resource = convertEntityToResource(consumer11);
			resource.setEntityId(consumer11.getId());
			resources.add(resource);
		}		
		return resources;
	}
	//	Code by Aishwarya to remove member of particular community
	@RequestMapping(value="/removeConsumer/{consumerId}/{communityId}", method = RequestMethod.POST)
	public void removeMemberfromCommunity(@PathVariable Long consumerId,@PathVariable Long communityId) throws Exception 
	{
		consumerService.removeMemberfromCommunity(consumerId,communityId);
		return;
	}



	//Code by Aishwarya to fetch community name by consumer id
	@RequestMapping(value="/community/{consumerId}", method = RequestMethod.GET)
	public List<String> findCommunityNameByConsumerId(@PathVariable Long consumerId) throws Exception 
	{
		List<String> concom=this.consumerService.fetchCommunityNameByConsumerId(consumerId);
		return concom;
	}
	//Code by Aishwarya to send mail after registration
	@RequestMapping(value="/mailSendRegister/{consumerId}", method = RequestMethod.POST)
	public void mailSendingAfterRegistration(@PathVariable Long consumerId) throws Exception 
	{
		Consumer consumer=consumerService.findByConsumerId(consumerId);
		List<ConsumerCommunity> concom=this.consumerService.findCommunitiesByConsumerId(consumerId);
		for(ConsumerCommunity concom1:concom){
			Community community=concom1.getCommunityID();
			Map<String, String> input = new HashMap<String, String>();
			input.put("username", consumer.getFirstName());
			input.put("communityName", community.getCommunityName());
			//input.put("communityMailUrl", communityMainPath);
			String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"Sign Up Template.html",input);
			EmailSending email1=new EmailSending();
			email1.emailSending(consumer.getEmailAddress(), htmlText);
		}

		return;
	}

	//Code by Aishwarya to fetch communities for consumer
	@RequestMapping(value="/communities/{consumerId}", method = RequestMethod.GET)
	public List<ConsumerCommunity> findCommunitiesByConsumerId(@PathVariable Long consumerId) throws Exception 
	{
		List<ConsumerCommunity> concom=this.consumerService.findCommunitiesByConsumerId(consumerId);
		return concom;
	}
	//Code by Aishwarya to invite a person to the community
//	@RequestMapping(value="/invitetoCommunity/{consumerId}/{consumerName}",method = RequestMethod.POST)
//	public Void inviteAConsumertoCommunity(@PathVariable Long consumerId,@PathVariable String consumerName,
//			@Validated @RequestBody String email) throws Exception {	
//		List<ConsumerCommunity> concom=this.consumerService.findCommunitiesByConsumerId(consumerId);
//		for(ConsumerCommunity concom1:concom){
//			Community community=concom1.getCommunityID();
//
//			Map<String, String> input = new HashMap<String, String>();
//			input.put("username", consumerName);
//			input.put("referral", community.getReferralCode());
//			input.put("communityMainPath", communityMainPath);   
//			String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"InviteToCommunityTemplate.html",input);
//			EmailSending email1=new EmailSending();
//			email1.emailSending(email, htmlText);
//		}
//		return null;  
//	}
	@RequestMapping(value="/invitetoCommunity/{communityId}/{consumerName}",method = RequestMethod.POST)
	public Void inviteAConsumertoCommunity(@PathVariable Long communityId,@PathVariable String consumerName,
			@Validated @RequestBody String email) throws Exception {
	     	Community community=communityService.findByCommunityId(communityId);
			Map<String, String> input = new HashMap<String, String>();
			input.put("username", consumerName);
			input.put("referral", community.getReferralCode());
			input.put("communityName", community.getCommunityName());
			input.put("communityMainPath", communityMainPath);   
			String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"InviteToCommunityTemplate.html",input);
			EmailSending email1=new EmailSending();
			email1.emailSending(email, htmlText);
		return null;  
	}
	//Code by Aishwarya for contact us functionality 
	@RequestMapping(value="/contactUs/{msg}",method = RequestMethod.POST)
	public Void contactUsFunction(@PathVariable String msg,@Validated @RequestBody String emailid) throws Exception {
		System.out.println("emailid"+emailid);
		Map<String, String> input = new HashMap<String, String>();
		input.put("emailid", emailid);
		input.put("message", msg);
		String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"contactUsTemplate.html",input);
		EmailSending email1=new EmailSending();
		email1.emailSending(mailFromAddress, htmlText);
		return null;  
	}

	//Code by Aishwarya for contact us form functionality 
	@RequestMapping(value="/contactUsForm",method = RequestMethod.POST)
	public Void contactUsFormFunction(@Validated @RequestBody ConsumerResource consumer) throws Exception {


		Map<String, String> input = new HashMap<String, String>();
		input.put("lastName", consumer.getLastName());
		input.put("firstName", consumer.getFirstName());
		input.put("emailid", consumer.getEmailAddress());
		input.put("type", consumer.getCommunityType());
		input.put("litMoreDescription", consumer.getSsn());
		String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"contactUsMarketingTemplate.html",input);
		EmailSending email1=new EmailSending();
		email1.emailSending(mailFromAddress, htmlText);
		return null;  
	}

	@RequestMapping(value = "/{consumerId}/memberlist/", method = RequestMethod.GET)
	public List<ConsumerResource> findConsumersOfCommunitybyConsumerId(@PathVariable Long consumerId) throws Exception 
	{
		List<ConsumerResource> resources = new ArrayList<ConsumerResource>();
		List<ConsumerCommunity> community=this.consumerService.findCommunity(consumerId);
		for(ConsumerCommunity consumerCommunity:community){
			Community community1=consumerCommunity.getCommunityID();
			List<ConsumerCommunity> consumer = consumerService.findConsumersOfCommunity(community1.getId());
			for (ConsumerCommunity consumer1 : consumer) {
				Long consumerid=consumer1.getConsumerID();
				Consumer consumer11 = consumerService.findByConsumerId(consumerid);
				ConsumerResource resource = convertEntityToResource(consumer11);
				resource.setEntityId(consumer11.getId());
				resources.add(resource);
			}		
		}
		return resources;
	}

	@RequestMapping(value="/forgotPassword/", method = RequestMethod.POST)
	public int mailSendingforForgotPassword(@Validated @RequestBody String emailid) throws Exception 
	{
		Consumer existingConsumer = consumerService.findByConsumerEmailAddress(emailid);
		if (existingConsumer == null) {
			return 0;
		}	
		String randPassword=getRandomPassword();
		String hashedPassword = HashingUtil.convertToMd5HashString(randPassword);
		accountService.updatePasswordByUserName(hashedPassword, emailid);
		consumerService.updatePasswordByUserName(hashedPassword, emailid);
		Map<String, String> input = new HashMap<String, String>();
		input.put("passwordval", randPassword);
		String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"forgotPasswordTemplate.html",input);
		EmailSending email1=new EmailSending();
		email1.emailSending(emailid, htmlText);
		return 1;
	}
	
	public String getRandomPassword() {
        String SALTCHARS = "VINODYADAVCOMMUNITYFORUSADITIMADANE123456789!@#$%^&*_=+-/";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
	
}
