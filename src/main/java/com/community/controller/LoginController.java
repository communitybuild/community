package com.community.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.servlet.http.HttpServletResponse;

import com.community.util.HashingUtil; 
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Account;
import com.community.model.Consumer;
import com.community.repository.ConsumerRepository;
import com.community.resource.AccountResource;
import com.community.service.AccountService;


/**
 * 
 * Controller for handling user login in the system
 * 
 */
@RestController
@RequestMapping("/login")
public class LoginController extends BaseController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private ConsumerRepository consumerRepository;

	private AccountResource convertEntityToResource(Account account) {
		AccountResource resource = modelMapper.map(account, AccountResource.class);
		return resource;
	}

	/**
	 * Functionality for user login
	 * 
	 * @param account
	 *            - Username & password
	 * @return Account returned from system
	 * @throws Exception
	 *             in case of errors
	 */
	@RequestMapping(method = RequestMethod.POST)
	public HttpEntity<AccountResource> login(@Validated @RequestBody AccountResource accountResource, HttpServletResponse response) throws Exception {
		try {
			String username = accountResource.getUsername();
			String password = accountResource.getPassword();
//			if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
//				response.sendError(0, "Username or password cannot be blank");
//				return null;
//					}
			Account account = accountService.findByUsername(username);
			if (account == null || !StringUtils.equals(HashingUtil.getHash(password), account.getPassword())) {
				response.sendError(0, "Incorrect username or password");
				return null;
			}
//			if () {
//				response.sendError(0, "Incorrect username or password");
//				return null;
//			}
			Consumer consumer = consumerRepository.findByEmailAddressLike(username);
			AccountResource resource = convertEntityToResource(account);
			resource.setConsumerId(consumer.getId());
			resource.setConsumerName(consumer.getFirstName());
			Link detail = linkTo(methodOn(AccountController.class).findByAccountId(account.getId())).withSelfRel();
			resource.add(detail);
			return new ResponseEntity<AccountResource>(resource, HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
		}
		return null;
	}
	

}
