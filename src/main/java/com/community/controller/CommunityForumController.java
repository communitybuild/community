package com.community.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Community;
import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;
import com.community.model.Forum;
import com.community.model.ForumReplies;
import com.community.resource.ForumRepliesResource;
import com.community.resource.ForumResource;
import com.community.service.ConsumerService;
import com.community.service.ForumService;




/**
 * 
 * Controller for discussion forum of Commun.ity
 * 
 */
@RestController
@RequestMapping("/communityforum")
public class CommunityForumController extends BaseController {

	@Autowired
	private ForumService forumService;

	@Autowired
	private ConsumerService consumerService;

	@Value("${ForumDirectoryPath}")
	private String ForumDirectoryPath;

	private ForumResource convertEntityToResource(Forum forum) {
		ForumResource resource = modelMapper.map(forum, ForumResource.class);
		return resource;
	}

	private ForumRepliesResource convertEntityToResource(ForumReplies topicreplies1) {
		ForumRepliesResource resource = modelMapper.map(topicreplies1, ForumRepliesResource.class);
		return resource;
	}

	@RequestMapping(value="/{consumerId}",method = RequestMethod.POST)
	public HttpEntity<ForumResource> createForumTopic(@PathVariable Long consumerId,@Validated @RequestBody ForumResource forumResource) throws Exception {
		try {
			Forum forum = new Forum();
			Consumer consumer = this.consumerService.findByConsumerId(consumerId);
			List<ConsumerCommunity> community=this.consumerService.findCommunity(consumerId);
			for(ConsumerCommunity consumerCommunity:community){
				Community community1=consumerCommunity.getCommunityID();
				forum.setTopicTitle(forumResource.getTopicTitle());
				forum.setTopicDescription(forumResource.getTopicDescription());
				forum.setTopicAttachment(forumResource.getTopicAttachment());
				forum.setConsumerId(consumer);
				forum.setCommunityId(community1.getId());
				forumService.createForumTopic(forum);
				return new ResponseEntity<ForumResource>(org.springframework.http.HttpStatus.OK);
			}} catch (Exception ex) {
				ex.printStackTrace();
			}
		return null;
	}

	@RequestMapping(value = "/{consumerId}", method = RequestMethod.GET)
	public List<ForumResource> fetchTopics(@PathVariable Long consumerId) throws Exception {
		List<ForumResource> resources = new ArrayList<ForumResource>();
		List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
		for (ConsumerCommunity concom1 : concom) {
			Community community=concom1.getCommunityID();
			List<Forum> forumlist =forumService.findTopicsByCommunityId(community.getId());
			for (Forum forumlist1 : forumlist) {
				ForumResource resource = convertEntityToResource(forumlist1);
				resource.setEntityId(forumlist1.getId());
				resource.setCreateDate(forumlist1.getCreateDate());
				resources.add(resource);
			}
		}
		return resources;
	}

	@RequestMapping(value = "/{topicId}/topicData", method = RequestMethod.GET)
	public List<ForumResource> fetchTopicByTopicId(@PathVariable Long topicId) throws Exception {
		List<ForumResource> resources = new ArrayList<ForumResource>();
		Forum forumlist=forumService.fetchTopicByTopicId(topicId);
		//	for (Forum forumlist1 : forumlist) {
		ForumResource resource = convertEntityToResource(forumlist);
		List<ForumReplies> topicreplies =forumService.fetchTopicRepliesByTopicId(topicId);
		resource.setForumReplies(topicreplies);
		resources.add(resource);
		return resources;
	}

	@RequestMapping(value="/{consumerId}/reply",method = RequestMethod.POST)
	public HttpEntity<ForumResource> giveReply(@PathVariable Long consumerId,@Validated @RequestBody ForumRepliesResource forumResource) throws Exception {
		try {
			
			ForumReplies forum = new ForumReplies();
			Consumer consumer = this.consumerService.findByConsumerId(consumerId);
			forum.setConsumerId(consumer);
			forum.setTopicReply(forumResource.getTopicReply());
			forum.setTopicReplyAttachment(forumResource.getTopicReplyAttachment());
			forum.setTopicId(forumResource.getTopicId());
			forumService.giveReplytoTopic(forum);
			Long replyCnt=forumService.getReplyCount(forumResource.getTopicId());
			if(replyCnt==null){
				replyCnt=(long) 0;
			}
		    forumService.updateReplyCount(forumResource.getTopicId(),replyCnt+1);
		    return new ResponseEntity<ForumResource>(org.springframework.http.HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/{topicId}/topicReply", method = RequestMethod.GET)
	public List<ForumRepliesResource> fetchRepliesByTopicId(@PathVariable Long topicId) throws Exception {
		List<ForumRepliesResource> resources = new ArrayList<ForumRepliesResource>();
		List<ForumReplies> topicreplies=forumService.fetchTopicRepliesByTopicId(topicId);
		for (ForumReplies topicreplies1 : topicreplies) {
			ForumRepliesResource resource = convertEntityToResource(topicreplies1);
			resource.setCreateDate(topicreplies1.getCreateDate());
			resource.setEntityId(topicreplies1.getId());
			resources.add(resource);	
		}
		return resources;
	}

	@RequestMapping(value = "/{topicId}/deleteTopic", method = RequestMethod.POST)
	public void deleteTopic(@PathVariable Long topicId,@Validated @RequestBody(required = false) List<String> forumAttachments) throws Exception {
	for(String forumAttachments1: forumAttachments){
	 File file = new File(ForumDirectoryPath+forumAttachments1);
	    file.delete();
	}
	forumService.deleteTopic(topicId);
	forumService.deleteRepliesforTopic(topicId);
		return;
	}

	@RequestMapping(value = "/{replyId}/{topicId}/deleteReply", method = RequestMethod.POST)
	public void deleteReply(@PathVariable Long replyId,@PathVariable Long topicId,@Validated @RequestBody(required = false) String replyAttachment) throws Exception {
		File file = new File(ForumDirectoryPath+replyAttachment);
		file.delete();
		forumService.deleteReply(replyId);
		Long replyCnt=forumService.getReplyCount(topicId);
	    forumService.updateReplyCount(topicId,replyCnt-1);
		return;
	}

}
