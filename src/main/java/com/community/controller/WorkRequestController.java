package com.community.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Activity;
import com.community.model.Community;
import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;
import com.community.model.Provider;
import com.community.model.WorkRequest;
import com.community.model.Chat;
import com.community.model.Ratings;
import com.community.resource.RatingsResource;
import com.community.resource.WorkRequestResource;
import com.community.service.ActivityService;
import com.community.service.ConsumerService;
import com.community.service.ProviderService;
import com.community.service.WorkRequestService;
import com.community.service.ChatService;
import com.community.util.EmailSending;
import com.community.controller.FileUploadController;

/**
 * 
 * Controller for handling work requests in the system. Links tag in the
 * response represents hyperlinks to related entities of an work request
 * 
 * TODO:Wrong Request Naming Conventions done by Abhijit, Nonsense code where
 * Controller itself expects db details rather than querying... - Sameer Shukla
 * 
 * Written all the methods which is of no use, simple use case has been missed
 * fetch workrequest of a consumer id....
 * 
 */
@RestController
@RequestMapping("/workrequests")
public class WorkRequestController extends BaseController {

	@Autowired
	private WorkRequestService workRequestService;

	@Autowired
	private ConsumerService consumerService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private ProviderService providerService;

	@Autowired
	private ChatService chatService;

	@Autowired
	private FileUploadController fileUploadController;
	
	@Value("${emailTemplatesPath}")
	private String emailTemplatesPath;
	
	@Value("${communityMainPath}")
	private String communityMainPath;
	
	private WorkRequestResource convertEntityToResource(WorkRequest workRequest) {
		WorkRequestResource resource = modelMapper.map(workRequest, WorkRequestResource.class);
		return resource;
	}
//	private WorkRequest convertResourceToEntity(WorkRequestResource workRequestResource) {
//		WorkRequest workRequest = modelMapper.map(workRequestResource, WorkRequest.class);
//		return workRequest;
//	}

	/**
	 * Fetch all the work requests in the System
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<WorkRequestResource> getAllWorkRequests() throws Exception {
		List<WorkRequest> workRequests = workRequestService.getAllWorkRequests();
		List<WorkRequestResource> resources = new ArrayList<WorkRequestResource>();
		
		for (WorkRequest workRequest : workRequests) {
			WorkRequestResource resource = convertEntityToResource(workRequest);
			resource.setEntityId(workRequest.getId());
			Link detail = linkTo(methodOn(WorkRequestController.class).findByworkRequestId(workRequest.getId()))
					.withSelfRel();
			resource.add(detail);
			resources.add(resource);
	
		}
		return resources;
	}

	/**
	 * Written By Sameer Shukla Fetch work request By Id
	 */
	@RequestMapping(value = "/provider/{providerId}", method = RequestMethod.GET)
	public void findWorkRequestOfProviderId(@PathVariable Long providerId) throws Exception {
		//return workRequestService.findByWorkRequestByProviderId(providerId);
	}

	/**
	 * Written By Sameer Shukla Fetch work request By ConsumerId
	 */
//	@RequestMapping(value = "/consumer/{consumerId}", method = RequestMethod.GET)
//	public List<WorkRequest> fetchWorkRequestForConsumerId(@PathVariable Long consumerId) throws Exception {
//		return workRequestService.findWorkRequestPostedByConsumer(consumerId);
//	}
	
	
	/**
	 * Fetch work request By Id
	 */
	@RequestMapping(value = "/{workRequestId}", method = RequestMethod.GET)
	public WorkRequestResource findByworkRequestId(@PathVariable Long workRequestId) throws Exception {
		WorkRequest workRequest = workRequestService.findByWorkRequestId(workRequestId);
		if (workRequest == null) {
			throw new IllegalArgumentException("WorkRequest with id " + workRequestId + " does not exist in system");
		}
		
		WorkRequestResource resource = convertEntityToResource(workRequest);
		Link detail = linkTo(WorkRequestController.class).slash(workRequest.getId()).withSelfRel();
		resource.add(detail);
		return resource;
	}

	/**
	 * Create workRequest
	 * 
	 * @param WorkRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/createRequest/{consumerId}", method = RequestMethod.POST)
	public HttpEntity<WorkRequestResource> createWorkRequest(
			@PathVariable Long consumerId,@Validated @RequestBody WorkRequestResource workRequestResource) throws Exception {
		try {
			Consumer consumer = this.consumerService.findByConsumerId(consumerId);
			List<ConsumerCommunity> community=this.consumerService.findCommunity(consumerId);
			for(ConsumerCommunity consumerCommunity:community){
			Community community1=consumerCommunity.getCommunityID();
			WorkRequest workRequest = new WorkRequest();
			workRequest.setActivity(workRequestResource.getActivity());
			workRequest.setConsumer(consumer);
			workRequest.setRequestDate(workRequestResource.getRequestDate());
			workRequest.setDescription(workRequestResource.getDescription());
			workRequest.setEstimatedCost(workRequestResource.getEstimatedCost());
			workRequest.setEstimatedHours(workRequestResource.getEstimatedHours());
			workRequest.setProviderId(workRequestResource.getProviderId());
//			if(workRequestResource.getProviderId()!=0){
//				workRequest.setProcessingStatus(1);
//			}
			workRequest.setCommunityId(community1.getId());
			WorkRequest savedEntity = workRequestService.create(workRequest);
			
			WorkRequestResource resource = convertEntityToResource(savedEntity);
			int requestCount=workRequestService.checkForFirstWorkRequest(consumerId);
			if(requestCount==1){
				Activity activity=workRequestResource.getActivity();
			     Map<String, String> input = new HashMap<String, String>();
			        input.put("username", consumer.getFirstName());
		            input.put("activity", activity.getActivityName());
		            String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"First Request Template.html",input);
			        EmailSending email1=new EmailSending();
			        email1.emailSending(consumer.getEmailAddress(), htmlText);
			}
			return new ResponseEntity<WorkRequestResource>(resource, org.springframework.http.HttpStatus.OK);
			}} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Delete workRequest
	 * 
	 * @param WorkRequest
	 *            id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{workRequestId}", method = RequestMethod.DELETE)
	public HttpEntity<String> deleteWorkRequest(@PathVariable Long workRequestId) throws Exception {
		String result = workRequestService.delete(workRequestId);
		return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
	}
	@RequestMapping(value = "/consumers/{consumerId}/{providerId}", method = RequestMethod.GET)
	public List<WorkRequestResource> fetchWorkRequestForCommunityId(@PathVariable Long consumerId,@PathVariable Long providerId) throws Exception {
	List<WorkRequestResource> resources = new ArrayList<WorkRequestResource>();
	List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
	String currentDate  = dateFormat.format(new Date());
	for (ConsumerCommunity community : concom) {
		Community community1=community.getCommunityID();
		System.out.println("community1"+community1);
		List<WorkRequest> workRequests = workRequestService.getAllWorkRequestsByCommunityId(community1.getId(),consumerId,currentDate,providerId);
		for (WorkRequest workRequest : workRequests) {
			WorkRequestResource resource = convertEntityToResource(workRequest);
			resource.setEntityId(workRequest.getId());
			Link detail = linkTo(methodOn(WorkRequestController.class).findByworkRequestId(workRequest.getId()))
					.withSelfRel();
			resource.add(detail);
			resources.add(resource);
		}
	}
		return resources;
	}

	@RequestMapping(value="/{workRequestId}/{providerId}/",method = RequestMethod.POST)
	public void updateProcessStatusRequest(@PathVariable Long workRequestId,@PathVariable Long providerId) throws Exception {
		workRequestService.updateProcessStatusRequest(workRequestId,providerId);
		return;
	}
	
	@RequestMapping(value = "/requesthistory/{providerId}/{consumerId}", method = RequestMethod.GET)
    public List<WorkRequest> findWorkRequestHistoryOfProviderId(@PathVariable Long providerId,@PathVariable Long consumerId) throws Exception {
		List<WorkRequest> resources = new ArrayList<WorkRequest>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
		String currentDate  = dateFormat.format(new Date());
		List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
		for (ConsumerCommunity community : concom) {
			Community community1=community.getCommunityID();
			List<WorkRequest> workRequest=workRequestService.findByWorkRequestHistoryByProviderId(providerId,currentDate,community1.getId());
		for(WorkRequest workRequest1:workRequest){
			resources.add(workRequest1);
		}
		}
		return resources;
    }
	
	@RequestMapping(value="/markComplete/{workRequestId}",method = RequestMethod.POST)
	public Long updateProcessStatusToComplete(@PathVariable Long workRequestId,
			@Validated @RequestBody WorkRequest workRequest) throws Exception {				
		workRequestService.UpdateProcessStatusToComplete(workRequestId, workRequest.getActualHours());
		return workRequestId;
		
	}
	@RequestMapping(value = "/provider/{consumerId}/{providerId}", method = RequestMethod.GET)
	public List<WorkRequestResource> findWorkRequestOfProviderId1(@PathVariable Long consumerId,@PathVariable Long providerId) throws Exception {
		List<WorkRequestResource> resources = new ArrayList<WorkRequestResource>();
	    List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
	for (ConsumerCommunity concom1 : concom) {
		Community community=concom1.getCommunityID();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
		String currentDate  = dateFormat.format(new Date());
		List<WorkRequest> workRequest =workRequestService.findByWorkRequestByProviderId(providerId,currentDate,community.getId());
	    for (WorkRequest workRequest2 : workRequest) {
			WorkRequestResource resource = convertEntityToResource(workRequest2);
			//start//Fetch chat content
			Consumer consumer = workRequest2.getConsumer();
			Long clientId=consumer.getId();
			List<String> communityName=consumerService.fetchCommunityNameByConsumerId(clientId);
			resource.setCommunityName(communityName);
			resource.setEntityId(workRequest2.getId());
			Long chatCount=chatService.getChatCount(workRequest2.getId(),consumerId);
          //  List<Chat> chatresource = chatService.findConsumersChat(consumerId, clientId);			
			 List<Chat> chatresource = chatService.findConsumersChat(workRequest2.getId());			
            resource.setChat(chatresource);
            resource.setChatCount(chatCount);
			Link detail = linkTo(methodOn(WorkRequestController.class).findByworkRequestId(workRequest2.getId())).withSelfRel();
			resource.add(detail);		
			resources.add(resource);
		}
	}
		return resources;
	}
	/**
	 * Written By Aishwarya to fetch work request By consumerId in My orders
	 */
	@RequestMapping(value = "/consumer/{consumerId}", method = RequestMethod.GET)
	public List<WorkRequestResource> fetchWorkRequestForConsumerId(@PathVariable Long consumerId) throws Exception {
		System.out.println("in fetchreqs");
		List<WorkRequestResource> resources = new ArrayList<WorkRequestResource>();
		   List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
			for (ConsumerCommunity concom1 : concom) {
				Community community=concom1.getCommunityID();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
				String currentDate  = dateFormat.format(new Date());
				List<WorkRequest> workRequest =workRequestService.findWorkRequestPostedByConsumer(consumerId,community.getId(),currentDate);
			
						for (WorkRequest workRequest2 : workRequest) {
			WorkRequestResource resource = convertEntityToResource(workRequest2);
			System.out.println("in workRequest"+workRequest2.getProviderId());
			if(workRequest2.getProviderId()!=0){
			Provider provider=providerService.findByProviderIdOnly(workRequest2.getProviderId());
			resource.setConsumer(provider.getConsumer());
			Consumer consumer = provider.getConsumer();
			Long helperId=consumer.getId();
			List<String> communityName=consumerService.fetchCommunityNameByConsumerId(helperId);
			resource.setCommunityName(communityName);
			}else{
				Consumer consumer=null;
				resource.setConsumer(consumer);	
			}
			
			resource.setEntityId(workRequest2.getId());
		    //Fetch chat content	
			Long chatCount=chatService.getChatCount(workRequest2.getId(),consumerId);
			List<Chat> chatresource = chatService.findConsumersChat(resource.getEntityId());	
            resource.setChat(chatresource);	
            resource.setChatCount(chatCount);
			resources.add(resource);
		}
			}
	   return resources;
	}
	
	@RequestMapping(value = "myorders/{workRequestId}", method = RequestMethod.POST)
	public HttpEntity<WorkRequestResource> update(@PathVariable Long workRequestId,@Validated @RequestBody WorkRequest workRequest) throws Exception 
	{
		workRequestService.update(workRequestId,workRequest);
		return new ResponseEntity<WorkRequestResource>(HttpStatus.OK);
	}
	

	
//	Common function by Aishwarya to send mail after work request has been accepted by helper and  when work request has been completed by helper
	@RequestMapping(value="/reqMailSending/{workRequestId}/{providerId}/{providerName}/{functionName}",method = RequestMethod.POST)
	public void mailSendForRequestCompletion(@PathVariable Long workRequestId,@PathVariable Long providerId,@PathVariable String providerName,@PathVariable String functionName) throws Exception {				
	    Object username = null;
	    Object email = null;
	    Object activityId=null;
	    String url = null;
		List<Object[]> emailAddr=workRequestService.getEmailOfConsumerByWorkRequest(workRequestId);
        for(Object[] emailAddr1:emailAddr){
        	username=emailAddr1[0];
        	email=emailAddr1[1];
        	activityId=emailAddr1[2];
	 }
    	String activity=activityService.findByActivityId(activityId);
        Map<String, String> input = new HashMap<String, String>();
        input.put("username", (String) username);
        if (StringUtils.equals(functionName, "reqHistory")) {
        	url="Task Complete Template.html";
        	input.put("activity",activity);
        }else if (StringUtils.equals(functionName, "openReq")) {
        	url="Tasker Accepted Template.html";
        	String provider=providerService.findProviderNameByProviderId(providerId);
            input.put("providerFirstName", providerName);
            input.put("provideLastName", provider);
            input.put("communityMainPath", communityMainPath);
            
        } 
    String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+url,input);
    EmailSending email1=new EmailSending();
    System.out.println("email"+email);
    email1.emailSending(email, htmlText);
		
		return;
		
	}
	@RequestMapping(value="/{workRequestId}/declineReq",method = RequestMethod.POST)
	public void declineWorkRequest(@PathVariable Long workRequestId) throws Exception {
		workRequestService.declineWorkRequest(workRequestId);
		return;
	}
	@RequestMapping(value="/declineReqMailSend/{providerId}/{providerName}",method = RequestMethod.POST)
	public void mailSendForRequestDecline(@PathVariable Long providerId,@PathVariable String providerName,@Validated @RequestBody WorkRequest workRequest) throws Exception {				
  //  	String activity=activityService.findByActivityId(activityId);
		Consumer consumer=workRequest.getConsumer();
		Activity activity=workRequest.getActivity();
		System.out.println("consumerIdddd"+consumer.getEmailAddress());
		System.out.println("activityyy"+activity.getActivityName());
    Map<String, String> input = new HashMap<String, String>();
    input.put("username", consumer.getFirstName());
    input.put("activity", activity.getActivityName());
    String providerData=providerService.findProviderNameByProviderId(providerId);
    input.put("providerFirstName", providerName);
    input.put("providerLastName", providerData);
    String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"declineRequestTemplate.html",input);
    EmailSending email1=new EmailSending();
    email1.emailSending(consumer.getEmailAddress(), htmlText);
		
		return;
		
	}
	
	@RequestMapping(value = "/ratingModal/{consumerId}", method = RequestMethod.GET)
	public WorkRequestResource checkForShowingRatingModal(@PathVariable Long consumerId) throws Exception {
	Long workId=workRequestService.getLastWorkrequest(consumerId);
	if(workId==null){
		return null;
	}
		int flag=workRequestService.checkForShowingRatingModal(workId);
		if(flag==0){
		WorkRequest workrequest=workRequestService.findByWorkRequestId(workId);
			WorkRequestResource resource = convertEntityToResource(workrequest);
			Provider provider=providerService.findByProviderIdOnly(workrequest.getProviderId());
			resource.setConsumer(provider.getConsumer());	
			resource.setEntityId(workrequest.getId());
		return resource;

		}else
		return null;
			}
	/**
	 * Written By Aishwarya to insert reviews for particular workrequest and provider
	 */
	@RequestMapping(value="/reviews",method = RequestMethod.POST)
	public ResponseEntity<RatingsResource> insertReviews(@Validated @RequestBody Ratings rating) throws Exception {				
	workRequestService.insertReviews(rating);
	Float aggr=workRequestService.getReviewsAggregate(rating.getProviderId());
	System.out.println("aggr"+aggr);
	System.out.println("provider"+rating.getProviderId());
	providerService.insertReviewsAggregate(rating.getProviderId(),aggr);
		return new ResponseEntity<RatingsResource>(HttpStatus.OK);
	}
}
