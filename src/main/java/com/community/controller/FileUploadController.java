package com.community.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map.Entry;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;





import com.community.service.CommunityService;

/**
 * 
 * @author Sameer Shukla
 * Changes in code by Aishwarya on 14th dec 2016
 *
 */
@RestController
public class FileUploadController {
    @Autowired
	private CommunityService communityService;
    
	@Value("${profilePicsDirectoryPath}")
	private String profilePicsDirectoryPath;
	
	@Value("${PostsDirectoryPath}")
	private String PostsDirectoryPath;
	
	@Value("${EventsDirectoryPath}")
	private String EventsDirectoryPath;
	
	@Value("${BusinessesDirectoryPath}")
	private String BusinessesDirectoryPath;
	
	@Value("${ForumDirectoryPath}")
	private String ForumDirectoryPath;
	
	@RequestMapping(value = "/uploadFile/{folderName}", method = RequestMethod.POST)
	public void uploadFile(@RequestParam("uploadfile") MultipartFile uploadfile,@RequestParam("uploadfilename") String uploadfilename,@PathVariable String folderName) {
    try {
			// Get the filename and build the local file path (be sure that the
			// application have write permissions on such directory)
        	String directory = null;
			//String filename = uploadfile.getOriginalFilename();
   			if(StringUtils.equals(folderName, "posts")){
			directory = PostsDirectoryPath;
			}else if(StringUtils.equals(folderName, "events")){
			directory = EventsDirectoryPath;
			}else if(StringUtils.equals(folderName, "forum_images")){
				directory = ForumDirectoryPath;
			}else{
				directory = BusinessesDirectoryPath;
			}
			String filepath = Paths.get(directory, uploadfilename).toString();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
			stream.write(uploadfile.getBytes());
			stream.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	} 
	@RequestMapping(value = "/uploadProfileFile/{OldImg}", method = RequestMethod.POST)
	public void uploadProfileFile(@RequestParam("uploadfile") MultipartFile uploadfile,@RequestParam("uploadfilename") String uploadfilename,@PathVariable String OldImg) {
    try {
    	    File file = new File(profilePicsDirectoryPath+OldImg);
    	    file.delete();
			//String filename = uploadfile.getOriginalFilename();
			String directory = profilePicsDirectoryPath;
			String filepath = Paths.get(directory,uploadfilename).toString();
            // Save the file locally
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
			stream.write(uploadfile.getBytes());
			stream.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	} 
	@RequestMapping(value = "/uploadCommunity/{communityId}", method = RequestMethod.POST)
	public void uploadCommunityImages(@RequestParam("uploadfile") MultipartFile[] uploadfile,String[] imagePaths,@PathVariable Long communityId) {
    try {
     	String imageValues=StringUtils.join(imagePaths, ','); 
     	if(imageValues!=""){
    	communityService.insertCommunityImages(imageValues,communityId);
     	}
    	File theDir = new File(profilePicsDirectoryPath+"Community"+communityId);
    	if (!theDir.exists()) {
    		theDir.setExecutable(true, false);
    		theDir.setReadable(true, false);
    		theDir.setWritable(true, false);
    	        theDir.mkdir();     
    	}else{
    		System.out.println("already exists");
    	}
    	   for(MultipartFile uploadedFile : uploadfile) {
			String filename = uploadedFile.getOriginalFilename();
			System.out.println("--- Upload File Name ---"+filename);
			String directory = profilePicsDirectoryPath+"Community"+communityId;
			String filepath = Paths.get(directory, filename).toString();
            // Save the file locally
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
			stream.write(uploadedFile.getBytes());
			stream.close();

    	   }
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	} 
//	@RequestMapping(value = "/sendMail", method = RequestMethod.POST)
//	public void sendMail() throws FileNotFoundException, IOException, ParseException {
//		
//		      // Recipient's email ID needs to be mentioned.
//		      String to = "aishwarya.dhopate@anveshak.com";
//
//		      // Sender's email ID needs to be mentioned
//		      String from = "anveshak94@gmail.com";
//
//		      // Assuming you are sending email from localhost
//		      String host = "smtp.gmail.com";
//
//		      // Get system properties
//		      Properties properties = System.getProperties();
//
//		      // Setup mail server
//		      properties.setProperty("mail.transport.protocol", "smtp");
//		      properties.setProperty("mail.host", host);
//		      properties.put("mail.smtp.auth", "true");
//		      properties.put("mail.smtp.port", "465");
//		      properties.put("mail.smtp.socketFactory.port", "465");
//		      properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
//		      // Get the default Session object.
//		      Session session = Session.getDefaultInstance(properties,new javax.mail.Authenticator() 
//	          {
//	               protected PasswordAuthentication getPasswordAuthentication()
//	               { return new PasswordAuthentication("anveshak94@gmail.com","aishwarya94");     }
//	          });
//
//		      try {
//		         // Create a default MimeMessage object.
//		         MimeMessage message = new MimeMessage(session);
//		         System.out.println("Mime Message obj created");
//		         message.setFrom(new InternetAddress(from));
//                 // Set To: header field of the header.
//		         message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
//		         	System.out.println("Receipient added..."+message.getRecipients(Message.RecipientType.TO));
//		         // Set Subject: header field
//		         message.setSubject("This is the Subject Line!");
//		         	System.out.println("Ready to send..");
//		            Multipart multipart = new MimeMultipart();
//			        MimeBodyPart messageBodyPart = new MimeBodyPart();
//			        //Set key values
//		            Map<String, String> input = new HashMap<String, String>();
//		               input.put("name", "Aishwarya");
//		             //HTML mail content
//		            String htmlText = readEmailFromHtml("src/main/resources/static/MailChimpTemplates/Sign Up Template.html",input);
//		            messageBodyPart.setContent(htmlText, "text/html");
//		            multipart.addBodyPart(messageBodyPart); 
//		            message.setContent(multipart);
//		         Transport.send(message);
//		         System.out.println("Sent message successfully....");
//		      }catch (MessagingException mex) {
//		         mex.printStackTrace();
//		      }
//		
//	}
	
	//Method to replace the values for keys
	protected String readEmailFromHtml(String filePath, Map<String, String> input)
	{
	    String msg = readContentFromFile(filePath);
	    try
	    {
	    Set<Entry<String, String>> entries = input.entrySet();
	    for(Map.Entry<String, String> entry : entries) {
	        msg = msg.replace(entry.getKey().trim(), entry.getValue().trim());
	    }
	    }
	    catch(Exception exception)
	    {
	        exception.printStackTrace();
	    }
	    return msg;
	}
	String readContentFromFile(String fileName)
	{
	    StringBuffer contents = new StringBuffer();
	    
	    try {
	      //use buffering, reading one line at a time
	      BufferedReader reader =  new BufferedReader(new FileReader(fileName));
	      try {
	        String line = null; 
	        while (( line = reader.readLine()) != null){
	          contents.append(line);
	          contents.append(System.getProperty("line.separator"));
	        }
	      }
	      finally {
	          reader.close();
	      }
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
	    return contents.toString();
	}
	
	@RequestMapping(value = "/uploadCommunitySponsors/{communityId}", method = RequestMethod.POST)
	public void uploadCommunitySponsorsImages(@RequestParam("uploadfile") MultipartFile[] uploadfile,@RequestParam(required = false) String[] imagePaths,@PathVariable Long communityId) {
    try {
    	
     	String imageValues=StringUtils.join(imagePaths, ','); 
     	if(imageValues!=""){
    	communityService.insertCommunitySponsorImages(imageValues,communityId);
     	}
    	File theDir = new File(profilePicsDirectoryPath+"CommunitySponsors"+communityId);
    	if (!theDir.exists()) {
    		theDir.setExecutable(true, false);
    		theDir.setReadable(true, false);
    		theDir.setWritable(true, false);
    	        theDir.mkdir();     
    	}else{
    		System.out.println("already exists");
    	}
    	   for(MultipartFile uploadedFile : uploadfile) {
			String filename = uploadedFile.getOriginalFilename();
			System.out.println("--- Upload File Name ---"+filename);
			String directory = profilePicsDirectoryPath+"CommunitySponsors"+communityId;
			String filepath = Paths.get(directory, filename).toString();
            // Save the file locally
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
			stream.write(uploadedFile.getBytes());
			stream.close();

    	   }
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
}
