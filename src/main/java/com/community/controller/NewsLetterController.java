package com.community.controller;

/**
 * 
 * Created by Aishwarya Dhopate
 * 
 * For sending weekly newsletters
 * 
 */

//import com.community.model.Chat;
//import com.community.resource.ChatResource;
//import com.community.service.WorkRequestService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.community.model.Community;
import com.community.model.CommunityManagerEvent;
import com.community.model.CommunityManagerPosts;
import com.community.model.CommunityNeeds;
import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;
import com.community.service.CommunityManagerService;
import com.community.service.CommunityNeedsService;
import com.community.service.CommunityService;
import com.community.service.ConsumerService;
import com.community.util.EmailSending;

@Component
public class NewsLetterController extends BaseController{
	@Autowired
	private CommunityService communityService;

	@Autowired
	private ConsumerService consumerService;

	@Autowired
	private CommunityManagerService communitymanagerService;

	@Autowired
	private CommunityNeedsService communityNeedsService;

	@Autowired
	private FileUploadController fileUploadController;

	@Value("${emailTemplatesPath}")
	private String emailTemplatesPath;

	@Value("${communityMainPath}")
	private String communityMainPath;
	
	@Value("${communityMainPathWithoutHash}")
	private String communityMainPathWithoutHash;

	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"MM/dd/yyyy HH:mm:ss");

	//	@Scheduled(fixedRate = 10000)
	//	public void performTask() {
	//
	//		System.out.println("Regular task performed at "
	//				+ dateFormat.format(new Date()));
	//
	//	}
	//
	//	@Scheduled(initialDelay = 1000, fixedRate = 10000)
	//	public void performDelayedTask() {
	//
	//		System.out.println("Delayed Regular task performed at "
	//				+ dateFormat.format(new Date()));
	//
	//	}

	//@Scheduled(cron="0 0/20 15 * * WED")
	@Scheduled(cron="0 0/40 * * * *")
	public void performTaskUsingCron() throws Exception {
		System.out.println("Regular task performed using Cron at "
				+ dateFormat.format(new Date()));
		//get communities
		List<Community> community=communityService.getAllCommunities();
		String commonData="<table border='0' cellpadding='0' cellspacing='0' width='600' id='templateBody'>"+
                "<tr><td valign='top' class='bodyContainer' style='padding-top:10px; padding-bottom:8px;'>"+
			       "<table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width:100%;'>"+
                "<tbody class='mcnTextBlockOuter'><tr><td valign='top' class='mcnTextBlockInner' style='padding-top:9px;'>"+
                "<table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%; min-width:100%;' width='100%' class='mcnTextContentContainer'>"+
                "<tbody><tr><td valign='top' class='mcnTextContent' style='padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;'>";
		for (Community community1 : community) {
			//get posts
			List<CommunityManagerPosts> communitypost = communitymanagerService.getPostsForNewsLetter(community1.getId());
			String posts="";
			if(communitypost.size()!=0){
			posts="<h3 style='text-align: left;'>News</h3></td></tr></tbody></table></td></tr></tbody>";
			for (CommunityManagerPosts communitypost1 : communitypost) {
				String postImage;
				if(communitypost1.getImage()==null || communitypost1.getImage().trim().isEmpty()){
					postImage="http://ec2-54-191-202-240.us-west-2.compute.amazonaws.com:8080/upload/posts/news.png";
				}else{
					communitypost1.setImage("image_post_1506493770122");
					postImage="http://ec2-54-191-202-240.us-west-2.compute.amazonaws.com:8080/upload/posts/"+communitypost1.getImage();
				}
				posts=posts+"<table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnCaptionBlock'>"+
						"<tbody class='mcnCaptionBlockOuter'><tr><td class='mcnCaptionBlockInner' valign='top' style='padding:9px;'>"+
						"<table border='0' cellpadding='0' cellspacing='0' class='mcnCaptionRightContentOuter' width='100%'>"+
						"<tbody><tr><td valign='top' class='mcnCaptionRightContentInner' style='padding:0 9px;'>"+
						"<table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnCaptionRightImageContentContainer'>"+
						"<tbody><tr><td class='mcnCaptionRightImageContent' valign='top'>"+
						"<img alt='' src='"+postImage+"' width='150' style='max-width:150px;' class='mcnImage'>"+
						"</td></tr></tbody></table><table class='mcnCaptionRightTextContentContainer' align='right' border='0' cellpadding='0' cellspacing='0' width='351'>"+
						"<tbody><tr><td valign='top' class='mcnTextContent'>"+
						communitypost1.getDescription()+
						"</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>";
			}
			posts=commonData+posts+"<table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnButtonBlock' style='min-width:100%;'>"+
                    "<tbody class='mcnButtonBlockOuter'><tr><td style='padding-top:10px; padding-right:20px; padding-bottom:18px; padding-left:18px;' valign='top' align='right' class='mcnButtonBlockInner'>"+
                    "<table border='0' cellpadding='0' cellspacing='0' class='mcnButtonContentContainer' style='border-collapse: separate !important;background-color: #FAA31B;'>"+
                    "<tbody><tr><td align='center' valign='middle' class='mcnButtonContent' style='font-family: Arial; font-size: 14px; padding:8px 14px;'>"+
                    "<a class='mcnButton' title='Read More' href='"+communityMainPath+"/dashboard' target='_self' style='letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;'>Read More</a>"+
                    "</td></tr></tbody></table></td></tr></tbody></table></td></tr></table>";
			}
			//get events
			List<CommunityManagerEvent> communityevent = communitymanagerService.getEventsForNewsLetter(community1.getId());
			String events="";
			for (CommunityManagerEvent communityevent1 : communityevent) {
				String eventImage;
				if(communityevent1.getEventImg()==null || communityevent1.getEventImg().trim().isEmpty()){
					eventImage="http://ec2-54-191-202-240.us-west-2.compute.amazonaws.com:8080/upload/events/events.png";
				}else{
					//communityevent1.setEventImg(eventImg);
					eventImage="http://ec2-54-191-202-240.us-west-2.compute.amazonaws.com:8080/upload/events/"+communityevent1.getEventImg();
				}
				events=events+"<td align='left' valign='top' class='columnsContainer' width='50%' style='padding-right:10px;'>"+
				"<table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateLeftColumn'>"+
				"<tr><td align='center' valign='top' style='padding-top:8px; padding-bottom:8px;'>"+
				"<table border='0' cellpadding='0' cellspacing='0' width='100%' class='templateColumn'>"+
				"<tr><td valign='top' class='leftColumnContainer'><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnCaptionBlock'>"+
				"<tbody class='mcnCaptionBlockOuter'><tr><td class='mcnCaptionBlockInner' valign='top' style='padding:9px;'>"+
				"<table align='left' border='0' cellpadding='0' cellspacing='0' class='mcnCaptionBottomContent' width='false'>"+
				"<tbody><tr><td class='mcnCaptionBottomImageContent' align='left' valign='top' style='padding:0 9px 9px 9px;'>"+
				"<img alt='' src='"+eventImage+"' width='250' style='max-width:250px;' class='mcnImage'>"+
				"</td></tr><tr><td class='mcnTextContent' valign='top' style='padding:0 9px 0 9px;' width='250'>"+
				"Event: "+communityevent1.getTitle()+"<br>Date: "+communityevent1.getEventDate()+"<br>Time: "+communityevent1.getStartTime()+" to "+communityevent1.getEndTime()+"<br>"+
				"Location: "+communityevent1.getLocation()+"<br></td></tr></tbody></table></td></tr></tbody></table><table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnButtonBlock' style='min-width:100%;'>"+
				"<tbody class='mcnButtonBlockOuter'><tr><td style='padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;' valign='top' align='left' class='mcnButtonBlockInner'>"+
				"<table border='0' cellpadding='0' cellspacing='0' class='mcnButtonContentContainer' style='border-collapse: separate !important;background-color: #FAA31B;'>"+
				"<tbody><tr><td align='center' valign='middle' class='mcnButtonContent' style='font-family: Arial; font-size: 14px; padding:8px 14px;'>"+
				"<a class='mcnButton' title='RSVP' href='"+communityMainPath+"/communityEvents' target='_self' style='letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;'>RSVP</a>"+
				"</td></tr></tbody></table></td></tr></tbody></table></td></tr></table></td></tr></table></td>";
			}
			//get needs
			List<CommunityNeeds> arrlist = new ArrayList<CommunityNeeds>();
			List<CommunityNeeds> communityneeds=communityNeedsService.getListofAllNeeds(community1.getId());
			for (CommunityNeeds communityneeds2 : communityneeds) {
				Long cnt=communityNeedsService.getSignupsCount(communityneeds2.getId());
				if(cnt==null){
					cnt=(long) 0;
				}
				if(communityneeds2.getQuantity()>cnt || (communityneeds2.getQuantity()<=1 && cnt>=1)){
					communityneeds2.setId(cnt);
					arrlist.add(communityneeds2);
				}
				if(arrlist.size()==2){
					break;
				}
			}
			String needs="";
			boolean needsFlag;
			if(community1.getCommunityHiddenSections()!=null){
				needsFlag=community1.getCommunityHiddenSections().contains("Needs");
			}else{
				needsFlag=false;
			}
			if(arrlist.size()!=0 && needsFlag == false){
			         needs="<h3 style='text-align: left;'>Needs</h3></td></tr></tbody></table></td></tr></tbody>";
			for (CommunityNeeds communityneeds1 : arrlist) {
				String slotAvailability="";
				if(communityneeds1.getQuantity()<=1){
					slotAvailability="Slot Available";
				}else if(communityneeds1.getQuantity()>1 && communityneeds1.getId()==0){
					slotAvailability="All slots available";
				}else{
					slotAvailability=communityneeds1.getId()+" of "+communityneeds1.getQuantity()+" slots filled";
				}
				needs=needs+"<table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnCaptionBlock'>"+
						"<tbody class='mcnCaptionBlockOuter'>"+
						"<tr><td class='mcnCaptionBlockInner' valign='top' style='padding:9px;'>"+
						"<table border='0' cellpadding='0' cellspacing='0' class='mcnCaptionRightContentOuter' width='100%'>"+
						"<tbody><tr><td valign='top' class='mcnCaptionRightContentInner' style='padding:0 9px;'>"+
						"<table class='mcnCaptionRightTextContentContainer' align='left' border='0' cellpadding='0' cellspacing='0' width='170'>"+
						"<tbody><tr><td valign='top' class='mcnTextContent'>"+
						communityneeds1.getNeedName()+" ("+communityneeds1.getQuantity()+")"+
						"</td></tr></tbody></table><table class='mcnCaptionRightTextContentContainer' align='right' border='0' cellpadding='0' cellspacing='0' width='130'>"+
						"<tbody><tr><td valign='top' class='mcnTextContent'>"+
						slotAvailability+
						"</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>";
			}
			needs=commonData+needs+"<table border='0' cellpadding='0' cellspacing='0' width='100%' class='mcnButtonBlock' style='min-width:100%;'>"+
                    "<tbody class='mcnButtonBlockOuter'><tr><td style='padding-top:10px; padding-right:20px; padding-bottom:18px; padding-left:18px;' valign='top' align='right' class='mcnButtonBlockInner'>"+
                    "<table border='0' cellpadding='0' cellspacing='0' class='mcnButtonContentContainer' style='border-collapse: separate !important;background-color: #FAA31B;'>"+
                    "<tbody><tr><td align='center' valign='middle' class='mcnButtonContent' style='font-family: Arial; font-size: 14px; padding:8px 14px;'>"+
                    "<a class='mcnButton' title='Sign up now' href='"+communityMainPath+"/communityNeeds' target='_self' style='letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;'>Sign up now</a>"+
                    "</td></tr></tbody></table></td></tr></tbody></table></td></tr></table>";
			}
			if(posts!="" || events!="" || needs!=""){
			Map<String, String> input = new HashMap<String, String>();
			input.put("newsFeedPosts",posts);
			input.put("newsFeedEvents",events);
			input.put("communityNeeds",needs);
			System.out.println("communityName"+community1.getCommunityName());
			input.put("communityName",community1.getCommunityName());
			input.put("communityMainPath",communityMainPath);

			String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"weeklyNewsletterTemplate.html",input);
			EmailSending email1=new EmailSending();
			//			List<ConsumerCommunity> consumer = consumerService.findConsumersOfCommunity(community1.getId());
			//			for (ConsumerCommunity consumer1 : consumer) {
			//				Long consumerid=consumer1.getConsumerID();
			//				Consumer consumer11 = consumerService.findByConsumerId(consumerid);
			//				System.out.println("consumer"+consumer11.getEmailAddress());
			//email1.emailSending("aishwarya.dhopate@anveshak.com", htmlText);
			email1.emailSending("aishwarya.dhopate@anveshak.com", htmlText);
			//}
			}
		}
	}



}
