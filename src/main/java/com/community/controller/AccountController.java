package com.community.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Account;
import com.community.model.Role;
import com.community.resource.AccountResource;
import com.community.resource.RoleResource;
import com.community.service.AccountService;

/**
 * 
 * Controller for handling user accounts in the system
 * 
 */
@RestController
@RequestMapping("/accounts")
public class AccountController extends BaseController {

	@Autowired
	private AccountService accountService;

	private AccountResource convertEntityToResource(Account account) {
		AccountResource resource = modelMapper.map(account, AccountResource.class);
		return resource;
	}

	private Role convertResourceToEntity(RoleResource roleResource) {
		Role role = modelMapper.map(roleResource, Role.class);
		return role;
	}	
	
	/**
	 * Fetch all the Accounts in the System
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<AccountResource> getAllAccounts() throws Exception {
		List<Account> accounts = accountService.getAllAccounts();
		List<AccountResource> resources = new ArrayList<AccountResource>();
		for (Account account : accounts) {
			AccountResource resource = convertEntityToResource(account);
			Link detail = linkTo(methodOn(AccountController.class).findByAccountId(
					account.getId())).withSelfRel();
			resource.add(detail);
			resources.add(resource);
		}
		return resources;
	}

	/**
	 * Fetch Account by Id
	 */
	@RequestMapping(value = "/{accountId}", method = RequestMethod.GET)
	public AccountResource findByAccountId(@PathVariable Long accountId) throws Exception {		
		Account account = accountService.findByAccountId(accountId);
		if (account == null) {
			throw new IllegalArgumentException("Account with id " 
					+ accountId + " does not exist in system");
		}
		AccountResource resource = convertEntityToResource(account);
		Link detail = linkTo(AccountController.class).slash(
				account.getId()).withSelfRel();
		resource.add(detail);
		return resource;
	}
	
	/**
	 * Fetch Account by email address
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public AccountResource findByAccountEmailAddress(@RequestParam 
			String emailAddress) throws Exception 
	{		
		Account account = accountService.findByUsername(emailAddress);
		if (account == null) {
			throw new IllegalArgumentException("Account with the email address '" 
					+ emailAddress + "' does not exist in system");
		}
		AccountResource resource = convertEntityToResource(account);
		Link detail = linkTo(AccountController.class).slash(account.getId()).withSelfRel();
		resource.add(detail);
		return resource;
	}	

	/**
	 * Create Account
	 * 
	 * @param account
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public HttpEntity<AccountResource> create(@Validated @RequestBody 
			AccountResource accountResource) throws Exception 
	{
		Account savedEntity = accountService.create(accountResource.getUsername(), 
				accountResource.getPassword());
		AccountResource resource = convertEntityToResource(savedEntity);
		Link detail = linkTo(methodOn(AccountController.class).findByAccountId(
				savedEntity.getId())).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<AccountResource>(resource, HttpStatus.CREATED);
	}
	

	/**
	 * Delete Account
	 * 
	 * @param Account username
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public HttpEntity<String> delete(@RequestParam String emailAddress) throws Exception {
		String result = accountService.delete(emailAddress);
		return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
	}
	
	/**
	 * Get the associated roles for a particular Account
	 * 
	 * @param Account id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{accountId}/roles", method = RequestMethod.GET)
	public HttpEntity<AccountResource> getAccountAssociatedRoles(@PathVariable 
			Long accountId) throws Exception 
	{
		Account account = accountService.findByAccountId(accountId);
		if (account == null) {
			throw new IllegalArgumentException("Account with id " 
					+ accountId + " does not exist in system");
		}
		AccountResource resource = convertEntityToResource(account);			
		Link detail = linkTo(methodOn(AccountController.class).getAccountAssociatedRoles(
				accountId)).withRel("Associated roles");
		resource.add(detail);	
		return new ResponseEntity<AccountResource>(resource, HttpStatus.OK);
	}
	
	/**
	 * Add role to a particular account
	 * 
	 * @param Account id, role to add
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/{accountId}/addrole", method = RequestMethod.POST)
	public HttpEntity<AccountResource> addRoletoAccount(@PathVariable Long accountId, 
			@RequestBody RoleResource roleResource) throws Exception 
	{
		Account account = accountService.findByAccountId(accountId);
		if (account == null) {
			throw new IllegalArgumentException("Account with id " 
					+ accountId + " does not exist in system");
		} else {
			Role role = convertResourceToEntity(roleResource);
			Set<Role> roles = new HashSet<Role>();
			roles.add(role);
			Account savedEntity = accountService.addRolestoAccount(account, roles);
			AccountResource resource = convertEntityToResource(savedEntity);			
			Link detail = linkTo(methodOn(AccountController.class).getAccountAssociatedRoles(
					accountId)).withRel("Associated roles");
			resource.add(detail);	
			return new ResponseEntity<AccountResource>(resource, HttpStatus.OK);
		}
	}
	
	/**
	 * Remove role from a particular account
	 * 
	 * @param Account id, role to be removed
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/{accountId}/removerole", method = RequestMethod.POST)
	public HttpEntity<AccountResource> removeRoleFromAccount(@PathVariable Long accountId, 
			@RequestBody RoleResource roleResource) throws Exception 
	{
		Account account = accountService.findByAccountId(accountId);
		if (account == null) {
			throw new IllegalArgumentException("Account with id " 
					+ accountId + " does not exist in system");
		} else {
			Role role = convertResourceToEntity(roleResource);
			Set<Role> roles = new HashSet<Role>();
			roles.add(role);
			Account savedEntity = accountService.removeRolesFromAccount(account, roles);
			AccountResource resource = convertEntityToResource(savedEntity);			
			Link detail = linkTo(methodOn(AccountController.class).getAccountAssociatedRoles(
					accountId)).withRel("Associated roles");
			resource.add(detail);	
			return new ResponseEntity<AccountResource>(resource, HttpStatus.OK);
		}
	}	
	
}
