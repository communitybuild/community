package com.community.controller;

/**
 * 
 * @author Aishwarya Dhopate
 * 
 */


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Community;
import com.community.model.CommunityBusinesses;
import com.community.model.ConsumerCommunity;
import com.community.resource.CommunityBusinessesResource;
import com.community.service.CommunityBusinessesService;
import com.community.service.ConsumerService;


@RestController
@RequestMapping("/businesses")
public class CommunityBusinessesController extends BaseController{
	
	@Autowired
	private CommunityBusinessesService communityBusinessService;
	@Autowired
	private ConsumerService consumerService;
	
	@Value("${BusinessesDirectoryPath}")
	private String BusinessesDirectoryPath;
	
	private CommunityBusinessesResource convertEntityToResource(CommunityBusinesses busi) {
		CommunityBusinessesResource resource = modelMapper.map(busi, CommunityBusinessesResource.class);
		return resource;
	}

	@RequestMapping(value="{consumerId}",method = RequestMethod.POST)
	public ResponseEntity<CommunityBusinesses> createBusiness(@Validated @RequestBody CommunityBusinesses businessInfo,@PathVariable Long consumerId) throws Exception {
		List<ConsumerCommunity> community=this.consumerService.findCommunity(consumerId);
		for(ConsumerCommunity consumerCommunity:community){
		Community community1=consumerCommunity.getCommunityID();
		communityBusinessService.create(businessInfo,community1.getId(),consumerId);
		}
		return new ResponseEntity<CommunityBusinesses>(HttpStatus.CREATED);
		
	}
	
	@RequestMapping(value = "/{consumerId}", method = RequestMethod.GET)
	public List<CommunityBusinessesResource> showBusinesses(@PathVariable Long consumerId) throws Exception {
		List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
		  List<CommunityBusinessesResource> resources = new ArrayList<CommunityBusinessesResource>();
		for (ConsumerCommunity community : concom) {
			Community community1=community.getCommunityID();
		List<CommunityBusinesses> busi = communityBusinessService.showBusinesses(community1.getId());
      
	for (CommunityBusinesses busi1 : busi) {
		CommunityBusinessesResource resource = convertEntityToResource(busi1);
			resource.setEntityId(busi1.getId());
			resources.add(resource);
		}
		}
		return resources;
	}
	
//	@RequestMapping(value = "/{consumerId}/consumerBusiness", method = RequestMethod.GET)
//	public CommunityBusinessesResource showBusinessOfConsumer(@PathVariable Long consumerId) {
//		CommunityBusinesses business = communityBusinessService.findBusinessConsumerId(consumerId);
//		if(business==null)
//			return null;
//		else{
//			CommunityBusinessesResource resource = convertEntityToResource(business);
//			resource.setEntityId(business.getId());
//		return resource;
//		}
//	}
	
	@RequestMapping(value="{BusinessId}/updateBusiness",method = RequestMethod.POST)
	public ResponseEntity<CommunityBusinesses> updateBusiness(@Validated @RequestBody CommunityBusinesses businessInfo,@PathVariable Long BusinessId) throws Exception {
		communityBusinessService.update(businessInfo,BusinessId);
		return new ResponseEntity<CommunityBusinesses>(HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/{communityId}/byCommunityId", method = RequestMethod.GET)
	public List<CommunityBusinessesResource> showBusinessesByCommunity(@PathVariable Long communityId) throws Exception {
		List<CommunityBusinessesResource> resources = new ArrayList<CommunityBusinessesResource>();
		List<CommunityBusinesses> busi = communityBusinessService.showBusinesses(communityId);
		for (CommunityBusinesses busi1 : busi) {
			CommunityBusinessesResource resource = convertEntityToResource(busi1);
			resource.setEntityId(busi1.getId());
			resource.setCreateDate(busi1.getCreateDate());
			resources.add(resource);
		}

		return resources;
	}
	
	@RequestMapping(value = "/{businessId}/byBusinessId", method = RequestMethod.GET)
	public CommunityBusinessesResource showBusinessOfConsumer(@PathVariable Long businessId) {
		CommunityBusinesses business = communityBusinessService.findBusinessBusinessId(businessId);
		if(business==null)
			return null;
		else{
			CommunityBusinessesResource resource = convertEntityToResource(business);
			resource.setEntityId(business.getId());
		return resource;
		}
	}
	
	@RequestMapping(value="/{businessId}/{oldImg}/deleteBusiness",method = RequestMethod.POST)
	public int deleteBusiness(@PathVariable Long businessId,@PathVariable String oldImg) throws Exception {
		if(oldImg!="noimage"){
		 File file = new File(BusinessesDirectoryPath+oldImg);
		 if(file.exists()){
 	    file.delete();
		 }
		}
			return communityBusinessService.deleteBusiness(businessId) ;
	}
}
