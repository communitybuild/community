package com.community.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Activity;
import com.community.model.Community;
import com.community.model.ConsumerCommunity;
import com.community.model.Provider;
import com.community.resource.ProviderResource;
import com.community.service.ActivityService;
import com.community.service.ConsumerService;
import com.community.service.ProviderService;

/**
 * 
 * Controller for handling Providers in the system.
 * Links tag in the response represents hyperlinks to related entities of a provider
 * 
 * Sameer Shukla - Poor Rest URL convention done by Abhijit.
 * 
 * Everything is wrong in this controller, no method is given for get all providers which cant be fetched without consumer id, total nonsense
 * 
 */
@RestController
@RequestMapping("/consumers/{consumerId}/providers")
public class ProviderController extends BaseController {

	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private ActivityService activityService;
	
	@Autowired
	private ConsumerService consumerService;	


	private ProviderResource convertEntityToResource(Provider provider) {
		ProviderResource resource = modelMapper.map(provider, ProviderResource.class);
		return resource;
	}

	/**
	 * Fetch the Provider for the given consumer
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Provider getProviderForConsumer(@PathVariable 
			Long consumerId) throws Exception 
	{
		Provider provider = providerService.getProviderForConsumer(consumerId);
		if (provider == null) {
			return null;
		}
		List<ConsumerCommunity> community=this.consumerService.findCommunity(consumerId);
		if(community.isEmpty()){
			provider.setDescription("notactive");
		}
		for(ConsumerCommunity consumerCommunity:community){
		Community community1=consumerCommunity.getCommunityID();
		provider.setDescription(community1.getCommunityHiddenSections());
		return provider;
		}
		return provider;
	}

	/**
	 * Fetch Provider using consumer Id & provider Id
	 */
	@RequestMapping(value = "/{providerId}", method = RequestMethod.GET)
	public ProviderResource findProvider(@PathVariable Long consumerId, 
			@PathVariable Long providerId) throws Exception 
	{	
		Provider provider = providerService.findByProviderId(consumerId, providerId);		
		if (provider == null) {
			throw new IllegalArgumentException("Provider with given Id does not exist");
		}
		provider.getConsumer();
		ProviderResource resource = this.convertEntityToResource(provider);
		Link detail = linkTo(methodOn(ProviderController.class).findProvider(
				consumerId, providerId)).withSelfRel();
		resource.add(detail);
		return resource;
	}
	
	/**
	 * Fetch Provider by ssn
	 */
	/*@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ProviderResource findByProviderSsn(@PathVariable Long consumerId, @RequestParam String ssn) throws Exception {		
		Provider provider = providerService.findByProviderEmailAddress(emailAddress);
		ProviderResource resource = convertEntityToResource();
		if (provider == null) {
			throw new IllegalArgumentException("Provider with the given ssn does not exist");
		} else {
			resource = this.convertEntityToResource(provider);
			Link detail = linkTo(ProviderController.class).slash(provider.getId()).withSelfRel();
			resource.add(detail);
		}
		return resource;
	}*/	

	/**
	 * Create Provider
	 * 
	 * @param Provider
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public HttpEntity<ProviderResource> create(@PathVariable Long consumerId, 
			@Validated @RequestBody Provider provider) throws Exception 
	{
		try{
			Provider savedEntity = providerService.create(consumerId, provider);
			ProviderResource resource = convertEntityToResource(savedEntity);
			Link detail = linkTo(methodOn(ProviderController.class).findProvider(
					consumerId, savedEntity.getId())).withSelfRel();
			resource.add(detail);
			return new ResponseEntity<ProviderResource>(resource, HttpStatus.CREATED);
		}catch(Exception ex){
			ex.printStackTrace();
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Update Provider
	 * 
	 * @param Provider id, updated Provider details
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{providerId}", method = RequestMethod.POST)
	public HttpEntity<ProviderResource> update(@PathVariable Long consumerId, 
			@PathVariable Long providerId, @Validated @RequestBody Provider provider) throws Exception 
	{
		Provider savedEntity = providerService.update(consumerId, providerId, provider);
		ProviderResource resource = convertEntityToResource(savedEntity);			
		Link detail = linkTo(methodOn(ProviderController.class).findProvider(
				consumerId, savedEntity.getId())).withSelfRel();
		resource.add(detail);	
		return new ResponseEntity<ProviderResource>(resource, HttpStatus.OK);
	}
	
	/**
	 * Delete Provider
	 * 
	 * @param Provider id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{providerId}", method = RequestMethod.DELETE)
	public HttpEntity<String> delete(@PathVariable Long consumerId, 
			@PathVariable Long providerId) throws Exception 
	{
		String result = providerService.delete(consumerId, providerId);
		return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
	}	

	/**
	 * Get the associated activities for a particular Provider
	 * 
	 * @param Provider id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{providerId}/activities", method = RequestMethod.GET)
	public HttpEntity<ProviderResource> getProviderActivities(@PathVariable Long consumerId, 
			@PathVariable Long providerId) throws Exception 
	{
		Provider provider = providerService.findByProviderId(consumerId, providerId);
		if (provider == null) {
			throw new IllegalArgumentException("Provider with id " 
					+ providerId +" does not exist in system");
		} else {
			ProviderResource resource = convertEntityToResource(provider);			
			Link detail = linkTo(methodOn(ProviderController.class).getProviderActivities(
					consumerId, providerId)).withRel("Associated Activities");
			resource.add(detail);	
			return new ResponseEntity<ProviderResource>(resource, HttpStatus.OK);
		}
	}
	
	/**
	 * Add activity to a particular Provider
	 * 
	 * @param Provider id, activity id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{providerId}/activities/{activityId}", method = RequestMethod.POST)
	public HttpEntity<ProviderResource> addActivityToProvider(@PathVariable Long consumerId, 
			@PathVariable Long providerId, @PathVariable Long activityId) throws Exception 
	{
		Provider provider = providerService.findByProviderId(consumerId, providerId);
		if (provider == null) {
			throw new IllegalArgumentException("Provider with id " 
					+ providerId +" does not exist in system");
		} else {
			Activity activity = activityService.findByActivityId(activityId);
			if (activity == null) {
				throw new IllegalArgumentException("Activity with id " 
						+ activityId + " does not exist in system");
			} else {
				Provider savedEntity = providerService.addActivityToProvider(activity, provider);
				ProviderResource resource = convertEntityToResource(savedEntity);			
				Link detail = linkTo(methodOn(ProviderController.class).getProviderActivities(
						consumerId, providerId)).withRel("Associated Activities");
				resource.add(detail);	
				return new ResponseEntity<ProviderResource>(resource, HttpStatus.OK);
			}
		}
	}
	
	/**
	 * Remove activity from a particular Provider
	 * 
	 * @param Provider id, activity id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{providerId}/activities/{activityId}", method = RequestMethod.DELETE)
	public HttpEntity<ProviderResource> removeActivityFromProvider(@PathVariable Long consumerId, 
			@PathVariable Long providerId, @PathVariable Long activityId) throws Exception 
	{
		Provider provider = providerService.findByProviderId(consumerId, providerId);
		if (provider == null) {
			throw new IllegalArgumentException("Provider with id " 
					+ providerId +" does not exist in system");
		} else {
			Activity activity = activityService.findByActivityId(activityId);
			if (activity == null) {
				throw new IllegalArgumentException("Activity with id " 
						+ activityId + " does not exist in system");
			} else {
				Provider savedEntity = providerService.removeActivityFromProvider(activity, provider);
				ProviderResource resource = convertEntityToResource(savedEntity);			
				Link detail = linkTo(methodOn(ProviderController.class).getProviderActivities(
						consumerId, providerId)).withRel("Associated Activities");
				resource.add(detail);	
				return new ResponseEntity<ProviderResource>(resource, HttpStatus.OK);
			}
		}
	}	
	
	/**
	 * Fetch Provider using provider Id
	 */
	@RequestMapping(value = "/{providerId}/provider", method = RequestMethod.GET)
	public Provider fetchProviderByproviderId(@PathVariable Long providerId) throws Exception 
	{
		Provider provider = providerService.findProviderByProviderId(providerId);		
		return provider;
	}
	@RequestMapping(value = "/{providerId}/updateProvider", method = RequestMethod.POST)
	public HttpEntity<Provider> updateProvider(@PathVariable Long providerId, @Validated @RequestBody Provider provider) throws Exception 
	{
		Provider savedEntity = providerService.updateProvider(providerId, provider);
	//	ProviderResource resource = convertEntityToResource(savedEntity);			
		return new ResponseEntity<Provider>(savedEntity, HttpStatus.OK);
	}
}
