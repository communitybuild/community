package com.community.controller;

/**
 * 
 * Created by Yogesh Attarde 
 * 
 * Controller for chat functinality in the system.
 * 
 * 
 * 
 */


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Chat;
import com.community.resource.ChatResource;
import com.community.service.ChatService;
import com.community.service.ProviderService;
import com.community.service.WorkRequestService;


@RestController
@RequestMapping("/chat")
public class ChatController extends BaseController{
	
	@Autowired
	private ChatService chatService;
	
	@Autowired
	private WorkRequestService workRequestService;
	
	@Autowired
	private ProviderService providerService;
	private ChatResource convertEntityToResource(Chat chat1) {
		ChatResource resource = modelMapper.map(chat1, ChatResource.class);
		return resource;
	}
	

	
	@RequestMapping(value="/save",method = RequestMethod.POST)
	public ResponseEntity<ChatResource> saveChat(@Validated @RequestBody Chat chat) throws Exception {
//		Long workRequestersConsumerId =ChatResource.getReceiverID(); 
//		Long consumerId=ChatResource.getSenderID();
//		String chatContent=ChatResource.getChatContent();
//		chatService.create(workRequestersConsumerId, consumerId, chatContent);
		chatService.create(chat);
		return new ResponseEntity<ChatResource>(HttpStatus.CREATED);
		
	}
	
	@RequestMapping(value = "/{workRequestId}", method = RequestMethod.GET)
	public List<ChatResource> getChatMessages(@PathVariable Long workRequestId) throws Exception {
	//	List<Chat> chat = chatService.findConsumersChat(senderId,receiverId);
		List<Chat> chat = chatService.findConsumersChat(workRequestId);
        List<ChatResource> resources = new ArrayList<ChatResource>();
	for (Chat chat1 : chat) {
		ChatResource resource = convertEntityToResource(chat1);
			//System.out.println("con"+resource.getChatContent());
			resource.setEntityId(chat1.getId());
			resources.add(resource);
		}
		return resources;
	
	}
	@RequestMapping(value="/markAsRead/{workid}/{consumerId}",method = RequestMethod.POST)
	public void markMsgsAsRead(@PathVariable Long workid,@PathVariable Long consumerId) throws Exception {
        chatService.markMsgsAsRead(workid,consumerId);
		return;
		
	}
//	@RequestMapping(value="/save/myorderschat",method = RequestMethod.POST)
//	public ResponseEntity<ChatResource> saveChatOfMyOrders(@Validated @RequestBody Chat chat) throws Exception {
//	Long receiverId=providerService.findConsumerByProviderId(chat.getReceiverID());
//	chat.setReceiverID(receiverId);
//		chatService.create(chat);
//		return new ResponseEntity<ChatResource>(HttpStatus.CREATED);
//		
//	}
}
