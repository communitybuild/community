package com.community.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Community;
import com.community.model.ConsumerCommunity;
import com.community.model.Provider;
import com.community.repository.ProviderRepository;
import com.community.service.ConsumerService;

/**
 * 
 * @author Sameer Shukla
 *
 *  No energy left to correct the mistakes done by Abhijit, writing my own controller.
 *  Bull shit Provider controller and mappings created.
 *  Changes in file by Aishwarya on 26th dec 16 to fetch top 5 helpers
 *   Changes in file by Aishwarya on 10th Jan 17 to map top 5 helpers with communityIds of consumer
 */
@RestController
@RequestMapping("/providers")
public class FeaturedHelperController {
	
	@Autowired
	private ProviderRepository providerRepository;
	@Autowired
	private ConsumerService consumerService;
	
	@RequestMapping(value="/{consumerId}",method = RequestMethod.GET)
	public List<Provider> providerList(@PathVariable Long consumerId) throws Exception 
	{
		List<Provider> resources = new ArrayList<Provider>();
		   List<ConsumerCommunity> concom= consumerService.findCommunity(consumerId);
				for (ConsumerCommunity concom1 : concom) {
					Community community=concom1.getCommunityID();
					List<Provider> providers=this.providerRepository.findTopFiveProviders(community.getId());
		for(Provider provider:providers){
					resources.add(provider);
		}
				}
				return resources;
				
	}

}
