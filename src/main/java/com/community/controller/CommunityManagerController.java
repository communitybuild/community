package com.community.controller;
/*
 * Author: Aishwarya Dhopate*/ 

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Community;
import com.community.model.CommunityManagerPosts;
import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;
import com.community.model.EventsRSVP;
import com.community.service.ConsumerService;
import com.community.resource.CommunityManagerPostsResource;
import com.community.resource.CommunityResource;
import com.community.resource.EventsRSVPResource;
import com.community.model.CommunityManagerEvent;
import com.community.resource.CommunityManagerEventResource;
import com.community.service.CommunityManagerService;
import com.community.service.CommunityService;



@RestController
@RequestMapping("/communitymanager")
public class CommunityManagerController extends BaseController {

	@Autowired
	private CommunityManagerService communitymanagerService;
	@Autowired
	private ConsumerService consumerService;
	@Autowired
	private CommunityService communityService;

	@Value("${PostsDirectoryPath}")
	private String PostsDirectoryPath;

	@Value("${EventsDirectoryPath}")
	private String EventsDirectoryPath;


	private CommunityManagerPostsResource convertEntityToResource(CommunityManagerPosts communitymanager) {
		CommunityManagerPostsResource resource = modelMapper.map(communitymanager, CommunityManagerPostsResource.class);
		return resource;
	}

	private CommunityManagerPostsResource convertEntityToResource(int communitymanager) {
		CommunityManagerPostsResource resource = modelMapper.map(communitymanager, CommunityManagerPostsResource.class);
		return resource;
	}
	private CommunityManagerEventResource convertEntityToResource(CommunityManagerEvent communitymanager) {
		CommunityManagerEventResource resource = modelMapper.map(communitymanager, CommunityManagerEventResource.class);
		return resource;
	}
	private CommunityManagerEventResource convertEntityToResourceEvent(int communitymanager) {
		CommunityManagerEventResource resource = modelMapper.map(communitymanager, CommunityManagerEventResource.class);
		return resource;
	}


	@RequestMapping(method = RequestMethod.GET)
	public List<CommunityManagerPostsResource> getAllCommunityManagerPosts() throws Exception {
		List<CommunityManagerPosts> communities = communitymanagerService.getAllCommunityManagerPosts();
		List<CommunityManagerPostsResource> resources = new ArrayList<CommunityManagerPostsResource>();
		for (CommunityManagerPosts communitymanager : communities) {

			CommunityManagerPostsResource resource = convertEntityToResource(communitymanager);
			resources.add(resource);
		}
		return resources;
	}
	@RequestMapping(value= "/posts/{consumerId}", method = RequestMethod.GET)
	public List<CommunityManagerPostsResource> getAllCommunityManagerPostsByCommunityId(@PathVariable Long consumerId) throws Exception {
		Consumer consumer = this.consumerService.findByConsumerId(consumerId);
		List<CommunityManagerPosts> communities = communitymanagerService.getAllCommunityManagerPostsByCommunityId(consumer.getManagerCommunity());
		List<CommunityManagerPostsResource> resources = new ArrayList<CommunityManagerPostsResource>();
		for (CommunityManagerPosts communitymanager : communities) {
			CommunityManagerPostsResource resource = convertEntityToResource(communitymanager);
			resource.setPostID(communitymanager.getId());
			resource.setManagerFName(consumer.getFirstName());
			resource.setManagerLName(consumer.getLastName());
			resource.setProfImg(consumer.getProfilePic());
			resource.setCreateDate(communitymanager.getCreateDate());
			resources.add(resource);
		}
		return resources;
	}
	@RequestMapping(value="/events/{consumerId}",method = RequestMethod.GET)
	public List<CommunityManagerEventResource> getAllCommunityManagerEvents(@PathVariable Long consumerId) throws Exception {
		Consumer consumer = this.consumerService.findByConsumerId(consumerId);
		List<CommunityManagerEvent> communities = communitymanagerService.getAllCommunityManagerEvents(consumer.getManagerCommunity());
		List<CommunityManagerEventResource> resources = new ArrayList<CommunityManagerEventResource>();
		for (CommunityManagerEvent communitymanager : communities) {
			CommunityManagerEventResource resource = convertEntityToResource(communitymanager);
			resource.setEventID(communitymanager.getId());
			resource.setManagerFName(consumer.getFirstName());
			resource.setManagerLName(consumer.getLastName());
			resource.setProfImg(consumer.getProfilePic());
			resource.setCreateDate(communitymanager.getCreateDate());

			resources.add(resource);
		}
		return resources;
	}
	@RequestMapping(value = "/{postId}", method = RequestMethod.GET)
	public CommunityManagerPostsResource findByCommunityManagerPostsId(@PathVariable Long postId) throws Exception {
		CommunityManagerPosts CommunityManagerPosts = communitymanagerService.findByCommunityManagerPostsId(postId);
		if (CommunityManagerPosts == null) {
			throw new IllegalArgumentException("Post with id " + postId + " does not exist in system");
		}
		CommunityManagerPostsResource resource = convertEntityToResource(CommunityManagerPosts);
		return resource;
	}
	@RequestMapping(value="/sharePost/{postId}",method = RequestMethod.POST)
	public ResponseEntity<CommunityManagerPostsResource> markPostAsShared(@PathVariable Long postId) throws Exception {				
		int savedEntity = communitymanagerService.markPostAsShared(postId);
		CommunityManagerPostsResource resource = convertEntityToResource(savedEntity);
		Link detail = linkTo(CommunityManagerController.class).slash(postId).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<CommunityManagerPostsResource>(resource,HttpStatus.OK);

	}
	@RequestMapping(value="/shareEvent/{eventId}",method = RequestMethod.POST)
	public ResponseEntity<CommunityManagerEventResource> markEventAsShared(@PathVariable Long eventId) throws Exception {				
		int savedEntity = communitymanagerService.markEventAsShared(eventId);
		CommunityManagerEventResource resource = convertEntityToResourceEvent(savedEntity);
		Link detail = linkTo(CommunityManagerController.class).slash(eventId).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<CommunityManagerEventResource>(resource,HttpStatus.OK);

	}
	@RequestMapping(value="/postEvent/", method = RequestMethod.POST)
	public HttpEntity<CommunityManagerEventResource> createEvent(@Validated @RequestBody CommunityManagerEvent communitymanagerevent) throws Exception {
		try {
			CommunityManagerEvent communityManagerEvent = new CommunityManagerEvent();
			communityManagerEvent.setCommunityID(communitymanagerevent.getCommunityID());
			communityManagerEvent.setConsumerID(communitymanagerevent.getConsumerID());
			communityManagerEvent.setDescription(communitymanagerevent.getDescription());
			communityManagerEvent.setEndTime(communitymanagerevent.getEndTime());
			communityManagerEvent.setStartTime(communitymanagerevent.getStartTime());
			communityManagerEvent.setEventDate(communitymanagerevent.getEventDate());
			communityManagerEvent.setEventImg(communitymanagerevent.getEventImg());
			communityManagerEvent.setLocation(communitymanagerevent.getLocation());
			communityManagerEvent.setTitle(communitymanagerevent.getTitle());
			communityManagerEvent.setSharedFlag(0);
			communitymanagerService.createEvent(communityManagerEvent);

			return new ResponseEntity<CommunityManagerEventResource>(org.springframework.http.HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="/createPost/", method = RequestMethod.POST)
	public HttpEntity<CommunityManagerPostsResource> createPost(@Validated @RequestBody CommunityManagerPosts communityManagerPosts) throws Exception {
		try {
			CommunityManagerPosts communityManagerpost = new CommunityManagerPosts();
			communityManagerpost.setCommunityID(communityManagerPosts.getCommunityID());
			communityManagerpost.setConsumerID(communityManagerPosts.getConsumerID());
			communityManagerpost.setDescription(communityManagerPosts.getDescription());
			communityManagerpost.setImage(communityManagerPosts.getImage());
			communityManagerpost.setSharedFlag(0);
			communitymanagerService.createPost(communityManagerpost);

			return new ResponseEntity<CommunityManagerPostsResource>(org.springframework.http.HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value= "/postsdashboard/{consumerId}", method = RequestMethod.GET)
	public List<CommunityManagerPostsResource> getAllCommunityManagerPostsOnDashboard(@PathVariable Long consumerId) throws Exception {
		List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
		List<CommunityManagerPostsResource> resources = new ArrayList<CommunityManagerPostsResource>();
		for (ConsumerCommunity concom1 : concom) {
			Community community=concom1.getCommunityID();
			List<CommunityManagerPosts> communities = communitymanagerService.getAllPostsByCommunityId(community.getId());
			for (CommunityManagerPosts communitymanager : communities) {
				CommunityManagerPostsResource resource = convertEntityToResource(communitymanager);
				resource.setPostID(communitymanager.getId());
				Consumer consumer = this.consumerService.findByConsumerId(communitymanager.getConsumerID());
				resource.setManagerFName(consumer.getFirstName());
				resource.setManagerLName(consumer.getLastName());
				resource.setProfImg(consumer.getProfilePic());
				resources.add(resource);
			}
		}
		return resources;
	}
	@RequestMapping(value="/eventsdashboard/{consumerId}",method = RequestMethod.GET)
	public List<CommunityManagerEventResource> getAllCommunityManagerEventsOnDashboard(@PathVariable Long consumerId) throws Exception {
		List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
		List<CommunityManagerEventResource> resources = new ArrayList<CommunityManagerEventResource>();
		for (ConsumerCommunity concom1 : concom) {
			Community community=concom1.getCommunityID();
			List<CommunityManagerEvent> communities = communitymanagerService.getAllEventsByCommunityId(community.getId());
			for (CommunityManagerEvent communitymanager : communities) {
				EventsRSVP eventsRsvpResource=communitymanagerService.fetchRsvpByEventsAndConsumer(communitymanager.getId(),consumerId);
				CommunityManagerEventResource resource = convertEntityToResource(communitymanager);
				resource.setEventID(communitymanager.getId());
				resource.setEventsrsvp(eventsRsvpResource);
				Consumer consumer = this.consumerService.findByConsumerId(communitymanager.getConsumerID());
				resource.setManagerFName(consumer.getFirstName());
				resource.setManagerLName(consumer.getLastName());
				resource.setProfImg(consumer.getProfilePic());
				resources.add(resource);
			}
		}
		return resources;
	}
	@RequestMapping(value="/rsvp", method = RequestMethod.POST)
	public HttpEntity<EventsRSVPResource> rsvpEvents(@Validated @RequestBody EventsRSVP eventsRSVP) throws Exception {
		try {
			eventsRSVP.setConsumerId(eventsRSVP.getConsumerId());
			eventsRSVP.setEventId(eventsRSVP.getEventId());
			eventsRSVP.setRsvp(eventsRSVP.getRsvp());
			communitymanagerService.rsvpEvents(eventsRSVP);
			return new ResponseEntity<EventsRSVPResource>(org.springframework.http.HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="/rsvpupdate", method = RequestMethod.POST)
	public HttpEntity<EventsRSVPResource> rsvpEventsUpdate(@Validated @RequestBody EventsRSVP eventsRSVP) throws Exception {
		try {
			Long consumerId=eventsRSVP.getConsumerId();
			Long eventId=eventsRSVP.getEventId();
			String rsvp=eventsRSVP.getRsvp();
			communitymanagerService.updateRsvpEvents(consumerId,eventId,rsvp);
			return new ResponseEntity<EventsRSVPResource>(org.springframework.http.HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="/managerrsvp/{communityId}",method = RequestMethod.GET)
	public List<Object> getEventsRSVPforManager(@PathVariable Long communityId) throws Exception {
		List<Object> concom= this.communitymanagerService.getEventsRSVPforManager(communityId);
		//   List<Object[]> resources = new ArrayList<Object[]>();
		return concom;
	}

	@RequestMapping(value="/{postId}/{oldImg}/deletePost",method = RequestMethod.POST)
	public int deletePost(@PathVariable Long postId,@PathVariable String oldImg) throws Exception {
		if(oldImg!="noimage"){
			File file = new File(PostsDirectoryPath+oldImg);
			if(file.exists()){
				file.delete();
			}
		}
		return communitymanagerService.deletePost(postId) ;
	}

	@RequestMapping(value="/{eventId}/{oldImg}/deleteEvent",method = RequestMethod.POST)
	public int deleteEvent(@PathVariable Long eventId,@PathVariable String oldImg) throws Exception {
		if(oldImg!="noimage"){
			File file = new File(EventsDirectoryPath+oldImg);
			if(file.exists()){
				file.delete();
			}
		}
		communitymanagerService.deleteEventsRSVP(eventId);
		communitymanagerService.deleteEvent(eventId);
		return 0;
	}

	//	Code by Aishwarya to update community settings	
	@RequestMapping(value="/settingsInfo/{consumerId}", method = RequestMethod.POST)
	public void updateCommunitySettingsInfo(@PathVariable Long consumerId,@Validated @RequestBody CommunityResource community) throws Exception 
	{
		if(community.getCommunityType()!=""){
			communityService.updateTypeCommunitySettingsInfo(community.getCommunityType(),community.getEntityId(),community.getCommunityHiddenSections(),community.getCommunityTitles());
		}
		//		if(email!=""){
		//		consumerService.updateEmailCommunitySettingsInfo(consumerId,email);
		//        }
		return;
	}
	
//	@RequestMapping(value="/{eventId}/eventInfo",method = RequestMethod.GET)
//	public CommunityManagerEventResource getEventInformation(@PathVariable Long eventId) throws Exception {
//		List<ConsumerCommunity> concom= this.consumerService.findCommunity(consumerId);
//		List<CommunityManagerEventResource> resources = new ArrayList<CommunityManagerEventResource>();
//		for (ConsumerCommunity concom1 : concom) {
//			Community community=concom1.getCommunityID();
//			List<CommunityManagerEvent> communities = communitymanagerService.getAllEventsByCommunityId(community.getId());
//			for (CommunityManagerEvent communitymanager : communities) {
//				EventsRSVP eventsRsvpResource=communitymanagerService.fetchRsvpByEventsAndConsumer(communitymanager.getId(),consumerId);
//				CommunityManagerEventResource resource = convertEntityToResource(communitymanager);
//				resource.setEventID(communitymanager.getId());
//				resource.setEventsrsvp(eventsRsvpResource);
//				Consumer consumer = this.consumerService.findByConsumerId(communitymanager.getConsumerID());
//				resource.setManagerFName(consumer.getFirstName());
//				resource.setManagerLName(consumer.getLastName());
//				resource.setProfImg(consumer.getProfilePic());
//				resources.add(resource);
//			}
//		}
//		return resources;
//	}



}
