package com.community.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Community;
import com.community.model.CommunityNeeds;
import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;
import com.community.model.SignupforNeeds;
import com.community.resource.CommunityNeedsResource;
import com.community.resource.SignupforNeedsResource;
import com.community.service.CommunityNeedsService;
import com.community.service.ConsumerService;
import com.community.util.EmailSending;

/**
 * 
 * Controller for handling Community needs
 * 
 */
@RestController
@RequestMapping("/communityneeds")
public class CommunityNeedsController extends BaseController {

	@Autowired
	private CommunityNeedsService communityNeedsService;

	@Autowired
	private ConsumerService consumerService;

	@Autowired
	private FileUploadController fileUploadController;

	@Value("${emailTemplatesPath}")
	private String emailTemplatesPath;

	@Value("${communityMainPath}")
	private String communityMainPath;

	private CommunityNeedsResource convertEntityToResource(CommunityNeeds communityNeeds) {
		CommunityNeedsResource resource = modelMapper.map(communityNeeds, CommunityNeedsResource.class);
		return resource;
	}

	@RequestMapping(method = RequestMethod.POST)
	public HttpEntity<CommunityNeedsResource> insertCommunityNeeds(@Validated @RequestBody CommunityNeeds CommunityNeeds) throws Exception {
		try {
			CommunityNeeds communityneeds = new CommunityNeeds();
			communityneeds.setNeedName(CommunityNeeds.getNeedName());
			communityneeds.setQuantity(CommunityNeeds.getQuantity());
			communityneeds.setCommunityId(CommunityNeeds.getCommunityId());
			communityneeds.setNeedComment(CommunityNeeds.getNeedComment());
			communityNeedsService.insertCommunityNeeds(communityneeds);
			return new ResponseEntity<CommunityNeedsResource>(org.springframework.http.HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value= "/{communityId}", method = RequestMethod.GET)
	public List<CommunityNeedsResource> getListofAllNeeds(@PathVariable Long communityId) throws Exception {
		List<CommunityNeeds> needs = communityNeedsService.getListofAllNeeds(communityId);
		List<CommunityNeedsResource> resources = new ArrayList<CommunityNeedsResource>();
		for (CommunityNeeds communityneed : needs) {
			CommunityNeedsResource resource = convertEntityToResource(communityneed);
			resource.setEntityId(communityneed.getId());
			resource.setCreateDate(communityneed.getCreateDate());
			resources.add(resource);
		}
		return resources;
	}

	@RequestMapping(value="/{needID}",method = RequestMethod.POST)
	public int deleteCommNeed(@PathVariable Long needID) throws Exception {
		communityNeedsService.deleteCommNeed(needID);
		communityNeedsService.deleteSignUpsforNeed(needID);
		return 0;
	}

	@RequestMapping(value= "/{consumerId}/needsListDash", method = RequestMethod.GET)
	public List<CommunityNeedsResource> getListofNeedsOnDashboard(@PathVariable Long consumerId) throws Exception {
		List<ConsumerCommunity> community=consumerService.findCommunity(consumerId);
		for(ConsumerCommunity consumerCommunity:community){
			Community community1=consumerCommunity.getCommunityID();
			List<CommunityNeeds> needs = communityNeedsService.getListofNeedsOnDashboard(community1.getId());
			List<CommunityNeedsResource> resources = new ArrayList<CommunityNeedsResource>();
			for (CommunityNeeds communityneed : needs) {
				Long cnt=communityNeedsService.getSignupsCount(communityneed.getId());
				CommunityNeedsResource resource = convertEntityToResource(communityneed);
				resource.setEntityId(communityneed.getId());
				resource.setCommunityId(cnt);
				resources.add(resource);
			}
			return resources;
		}
		return null;
	}

	@RequestMapping(value= "/{consumerId}/needsList", method = RequestMethod.GET)
	public List<CommunityNeedsResource> getListofNeeds(@PathVariable Long consumerId) throws Exception {
		List<ConsumerCommunity> community=consumerService.findCommunity(consumerId);
		for(ConsumerCommunity consumerCommunity:community){
			Community community1=consumerCommunity.getCommunityID();
			List<CommunityNeeds> needs = communityNeedsService.getListofAllNeeds(community1.getId());
			List<CommunityNeedsResource> resources = new ArrayList<CommunityNeedsResource>();
			for (CommunityNeeds communityneed : needs) {
				Long cnt=communityNeedsService.getSignupsCount(communityneed.getId());
				List<SignupforNeeds> signup = communityNeedsService.getListOfSignUps(communityneed.getId());
				CommunityNeedsResource resource = convertEntityToResource(communityneed);
				resource.setEntityId(communityneed.getId());
				resource.setCreateDate(communityneed.getCreateDate());
				resource.setSignups(signup);
				resource.setCommunityId(cnt);
				resources.add(resource);
			}
			return resources;
		}
		return null;

	}

	@RequestMapping(value="{consumerId}/createSignUps", method = RequestMethod.POST)
	public HttpEntity<SignupforNeedsResource> createSignUps(
			@PathVariable Long consumerId,@Validated @RequestBody SignupforNeedsResource signupforNeedsResource) throws Exception {
		try {
			Long signupCnt=communityNeedsService.getSignupCountByConsumerId(consumerId,signupforNeedsResource.getNeedId());
			if(signupCnt != null){
				signupCnt=signupCnt+signupforNeedsResource.getSignupQuantity();
				communityNeedsService.updateSignups(consumerId,signupforNeedsResource.getNeedId(),signupCnt);
			}else{
			Consumer consumer = this.consumerService.findByConsumerId(consumerId);
			SignupforNeeds signupforNeeds = new SignupforNeeds();
			signupforNeeds.setNeedId(signupforNeedsResource.getNeedId());
			signupforNeeds.setConsumerId(consumer);
			signupforNeeds.setSignupQuantity(signupforNeedsResource.getSignupQuantity());
			communityNeedsService.createSignUps(signupforNeeds);	
			}
			return new ResponseEntity<SignupforNeedsResource>(org.springframework.http.HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="{consumerId}/{needId}", method = RequestMethod.POST)
	public int deleteSignUps(
			@PathVariable Long consumerId,@PathVariable Long needId) throws Exception {
		communityNeedsService.deleteSignUps(consumerId,needId);
		return 0;
	}

	@RequestMapping(value="/{consumerId}/{consumerName}/mailSignUps",method = RequestMethod.POST)
	public void mailSendForRequestCompletion(@PathVariable Long consumerId,@PathVariable String consumerName,@Validated @RequestBody SignupforNeedsResource signupforNeedsResource) throws Exception {				
		String email=consumerService.findEmailByConsumerId(consumerId);
		CommunityNeeds communityNeed=communityNeedsService.getNeedByNeedId(signupforNeedsResource.getNeedId());
		Map<String, String> input = new HashMap<String, String>();
		input.put("username",consumerName);
		input.put("needName",communityNeed.getNeedName());
	//	input.put("slotsCnt",signupforNeedsResource.getSignupQuantity());
		String htmlText = fileUploadController.readEmailFromHtml(emailTemplatesPath+"signUpForNeedsTemplate.html",input);
		EmailSending email1=new EmailSending();
		email1.emailSending(email, htmlText);
		return;	
	}

}
