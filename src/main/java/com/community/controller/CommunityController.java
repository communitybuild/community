package com.community.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Activity;
import com.community.model.Community;
import com.community.resource.CommunityResource;
import com.community.service.ActivityService;
import com.community.service.CommunityService;
import com.community.service.ConsumerService;

/**
 * 
 * Controller for handling communities in the system.
 * Links tag in the response represents hyperlinks to related entities of a community
 * 
 */
@RestController
@RequestMapping("/communities")
public class CommunityController extends BaseController {

	@Autowired
	private CommunityService communityService;

	@Autowired
	private ActivityService activityService;	

	@Autowired
	private ConsumerService consumerService;	

	@Value("${profilePicsDirectoryPath}")
	private String profilePicsDirectoryPath;

	private CommunityResource convertEntityToResource(Community community) {
		CommunityResource resource = modelMapper.map(community, CommunityResource.class);
		return resource;
	}

	private Community convertResourceToEntity(CommunityResource communityResource) {
		Community community = modelMapper.map(communityResource, Community.class);
		return community;
	}	

	/**
	 * Fetch all the Communities in the System
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<CommunityResource> getCommunities() throws Exception {
		List<Community> communities = communityService.getAllCommunities();
		List<CommunityResource> resources = new ArrayList<CommunityResource>();
		for (Community community : communities) {
			CommunityResource resource = convertEntityToResource(community);
			Link detail = linkTo(methodOn(CommunityController.class).findByCommunityId(
					community.getId())).withSelfRel();
			resource.add(detail);
			resources.add(resource);
		}
		return resources;
	}

	/**
	 * Fetch Community By Id
	 */
	@RequestMapping(value = "/{communityId}", method = RequestMethod.GET)
	public CommunityResource findByCommunityId(@PathVariable Long communityId) throws Exception {		
		Community community = communityService.findByCommunityId(communityId);
		if (community == null) {
			throw new IllegalArgumentException("Community with id " 
					+ communityId + " does not exist in system");
		}
		CommunityResource resource = convertEntityToResource(community);
		Link detail = linkTo(CommunityController.class).slash(
				community.getId()).withSelfRel();
		resource.add(detail);
		return resource;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public CommunityResource findByCommunityName(@RequestParam String communityName) throws Exception {		
		Community community = communityService.findByCommunityName(communityName);
		if (community == null) {
			throw new IllegalArgumentException("Community with name '" 
					+ communityName + "' does not exist in system");
		}
		CommunityResource resource = convertEntityToResource(community);
		Link detail = linkTo(CommunityController.class).slash(
				community.getId()).withSelfRel();
		resource.add(detail);
		return resource;
	}	

	/**
	 * Create Community
	 * 
	 * @param Community
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public HttpEntity<CommunityResource> create(@Validated @RequestBody 
			CommunityResource communityResource) throws Exception 
			{
		Community savedEntity = communityService.create(convertResourceToEntity(communityResource));
		CommunityResource resource = convertEntityToResource(savedEntity);		
		Link detail = linkTo(methodOn(CommunityController.class).findByCommunityId(
				savedEntity.getId())).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<CommunityResource>(resource, HttpStatus.OK);
			}

	/**
	 * Update Community
	 * 
	 * @param Community id, updated Community details
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{communityId}", method = RequestMethod.PUT)
	public HttpEntity<CommunityResource> update(@PathVariable Long communityId, 
			@Validated @RequestBody CommunityResource communityResource) throws Exception 
			{
		Community savedEntity = communityService.update(communityId, 
				convertResourceToEntity(communityResource));
		CommunityResource resource = convertEntityToResource(savedEntity);						
		Link detail = linkTo(methodOn(CommunityController.class).findByCommunityId(
				savedEntity.getId())).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<CommunityResource>(resource, org.springframework.http.HttpStatus.OK);
			}

	/**
	 * Delete Community
	 * 
	 * @param Community id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{communityId}", method = RequestMethod.DELETE)
	public HttpEntity<String> delete(@PathVariable Long communityId) throws Exception {
		String result = communityService.delete(communityId);
		return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
	}

	/**
	 * Get the associated activities for a particular Community
	 * 
	 * @param community id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{communityId}/activities", method = RequestMethod.GET)
	public HttpEntity<CommunityResource> getCommunityActivities(@PathVariable 
			Long communityId) throws Exception 
			{
		Community community = communityService.findByCommunityId(communityId);
		if (community == null) {
			throw new IllegalArgumentException("Community with id " 
					+ communityId + " does not exist in system");
		}
		CommunityResource resource = convertEntityToResource(community);			
		Link detail = linkTo(methodOn(CommunityController.class).getCommunityActivities(
				communityId)).withRel("Associated Activities");
		resource.add(detail);	
		return new ResponseEntity<CommunityResource>(resource, HttpStatus.OK);
			}

	/**
	 * Add activity to a particular community
	 * 
	 * @param community id, activity id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{communityId}/activities/{activityId}", method = RequestMethod.POST)
	public HttpEntity<CommunityResource> addActivityToCommunity(@PathVariable Long communityId, 
			@PathVariable Long activityId) throws Exception 
			{
		Community community = communityService.findByCommunityId(communityId);
		if (community == null) {
			throw new IllegalArgumentException("Community with id " 
					+ communityId +" does not exist in system");
		} else {
			Activity activity = activityService.findByActivityId(activityId);
			if (activity == null) {
				throw new IllegalArgumentException("Activity with id " 
						+ activityId + " does not exist in system");
			} else {
				Community savedEntity = communityService.addActivityToCommunity(activity, community);
				CommunityResource resource = convertEntityToResource(savedEntity);			
				Link detail = linkTo(methodOn(CommunityController.class).getCommunityActivities(
						communityId)).withRel("Associated Activities");
				resource.add(detail);	
				return new ResponseEntity<CommunityResource>(resource, HttpStatus.OK);
			}
		}
			}

	/**
	 * Remove activity from a particular community
	 * 
	 * @param community id, activity id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{communityId}/activities/{activityId}", method = RequestMethod.DELETE)
	public HttpEntity<CommunityResource> removeActivityFromCommunity(@PathVariable Long communityId, 
			@PathVariable Long activityId) throws Exception 
			{
		Community community = communityService.findByCommunityId(communityId);
		if (community == null) {
			throw new IllegalArgumentException("Community with id " 
					+ communityId +" does not exist in system");
		} else {
			Activity activity = activityService.findByActivityId(activityId);
			if (activity == null) {
				throw new IllegalArgumentException("Activity with id " 
						+ activityId + " does not exist in system");
			} else {
				Community savedEntity = communityService.removeActivityFromCommunity(activity, community);
				CommunityResource resource = convertEntityToResource(savedEntity);			
				Link detail = linkTo(methodOn(CommunityController.class).getCommunityActivities(
						communityId)).withRel("Associated Activities");
				resource.add(detail);	
				return new ResponseEntity<CommunityResource>(resource, HttpStatus.OK);
			}
		}
			}
	//Code by Aishwarya to get referral code for community
	@RequestMapping(value = "{communityId}/referral", method = RequestMethod.GET)
	public List<String> findReferralByCommunityId(@PathVariable Long communityId) throws Exception 
	{
		List<String> referral=communityService.findReferralByCommunityId(communityId);
		System.out.println("referral"+referral);
		return referral;
	}
	//Code by Aishwarya
	@RequestMapping(value = "/commImages/{consumerId}", method = RequestMethod.GET)
	public List<Object> findCommImagesByConsumerId(@PathVariable Long consumerId) throws Exception 
	{
		List<Object> CommImages=communityService.findCommImagesByConsumerId(consumerId);
		System.out.println("referral"+CommImages);
		return CommImages;
	}

	@RequestMapping(value = "/communityImages/{communityId}", method = RequestMethod.GET)
	public Object findCommImagesByCommunityId(@PathVariable Long communityId) throws Exception 
	{

		Object CommImages=communityService.findCommImagesByCommunityId(communityId);
		System.out.println("referral"+CommImages);
		return CommImages;
	}

	@RequestMapping(value="/delSponsor/{communityID}/{img}",method = RequestMethod.POST)
	public int deleteSponsorImage(@PathVariable Long communityID,@PathVariable String img,@RequestParam(required = false) String[] imagePaths) throws Exception {
		File file = new File(profilePicsDirectoryPath+"CommunitySponsors"+communityID+"/"+img);
		if(file.exists()){
			file.delete();
		}
		String imageValues=StringUtils.join(imagePaths, ','); 
		communityService.deleteSponsorImage(communityID,imageValues);
		return 0;
	}
	
	@RequestMapping(value = "/{consumerId}/communitySettings/{communityId}", method = RequestMethod.GET)
	public CommunityResource findCommunityInfobyConsumerId(@PathVariable Long consumerId,@PathVariable Long communityId) throws Exception 
	{
		Community community=communityService.findByCommunityId(communityId);
	//	String commType=community.getCommunityType();
		String consumerUsername = consumerService.findEmailByConsumerId(consumerId);
		CommunityResource resource = convertEntityToResource(community);
		resource.setDescription(consumerUsername);
		//resource.setCommunityType(commType);
		//resource.setEntityId(consumerId);
		return resource;	
	}
	
	@RequestMapping(value = "/{communityParent}/communityParent", method = RequestMethod.GET)
	public List<Community> findCommunitybyParent(@PathVariable String communityParent) throws Exception {		
		List<Community> community = communityService.findCommunitybyParent(communityParent);
		//CommunityResource resource = convertEntityToResource(community);
		return community;
	}


}
