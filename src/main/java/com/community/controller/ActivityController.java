package com.community.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.Activity;
import com.community.resource.ActivityResource;
import com.community.service.ActivityService;

/**
 * 
 * Controller for handling activities in the system, it's a normal CRUD for activities.
 * Links tag in the response represents hyperlinks to related entities of an Activity
 * 
 */
@RestController
@RequestMapping("/activities")
public class ActivityController extends BaseController {

	@Autowired
	private ActivityService activityService;

	private ActivityResource convertEntityToResource(Activity activity) {
		ActivityResource resource = modelMapper.map(activity, ActivityResource.class);
		return resource;
	}
	
	private Activity convertResourceToEntity(ActivityResource activityResource) {
		Activity activity = modelMapper.map(activityResource, Activity.class);
		return activity;
	}	

	/**
	 * Fetch all the activities in the System
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<ActivityResource> getActivities() throws Exception {
		List<Activity> activities = activityService.getAllActivities();
		List<ActivityResource> resources = new ArrayList<ActivityResource>();
		for (Activity activity : activities) {
			ActivityResource resource = convertEntityToResource(activity);
			resource.setEntityId(activity.getId());
//			Link detail = linkTo(methodOn(ActivityController.class).findByActivityId(
//					activity.getId())).withSelfRel();
//			resource.add(detail);
			resources.add(resource);
		}
		return resources;
	}

	/**
	 * Fetch activity By Id
	 */
	@RequestMapping(value = "/{activityId}", method = RequestMethod.GET)
	public ActivityResource findByActivityId(@PathVariable Long activityId) throws Exception {
		Activity activity = activityService.findByActivityId(activityId);
		if (activity == null) {
			throw new IllegalArgumentException("Activity with id " 
					+ activityId +" does not exist in system");
		} else {
			ActivityResource resource = convertEntityToResource(activity);
			Link detail = linkTo(ActivityController.class).slash(
					activity.getId()).withSelfRel();
			resource.add(detail);
			return resource;
		}		
	}
	
	/**
	 * Fetch activity By name
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ActivityResource findByActivityName(@RequestParam String activityName) throws Exception {
		Activity activity = activityService.findByActivityName(activityName);
		if (activity == null) {
			throw new IllegalArgumentException("Activity with name '" 
					+ activityName + "' does not exist in system");
		} else {
			ActivityResource resource = convertEntityToResource(activity);
			Link detail = linkTo(ActivityController.class).slash(
					activity.getId()).withSelfRel();
			resource.add(detail);
			return resource;
		}		
	}	

	/**
	 * Create activity
	 * 
	 * @param Activity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public HttpEntity<ActivityResource> create(@Validated @RequestBody 
			ActivityResource activityResource) throws Exception 
	{
		Activity savedEntity = activityService.create(convertResourceToEntity(activityResource));
		ActivityResource resource = convertEntityToResource(savedEntity);		
		Link detail = linkTo(methodOn(ActivityController.class).findByActivityId(
				savedEntity.getId())).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<ActivityResource>(resource, org.springframework.http.HttpStatus.OK);
	}
	
	/**
	 * Update activity
	 * 
	 * @param Activity id, updated activity details
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{activityId}", method = RequestMethod.PUT)
	public HttpEntity<ActivityResource> update(@PathVariable Long activityId, 
			@Validated @RequestBody ActivityResource activityResource) throws Exception 
	{
		Activity savedEntity = activityService.update(activityId, 
				convertResourceToEntity(activityResource));
		ActivityResource resource = convertEntityToResource(savedEntity);						
		Link detail = linkTo(methodOn(ActivityController.class).findByActivityId(
				savedEntity.getId())).withSelfRel();
		resource.add(detail);
		return new ResponseEntity<ActivityResource>(resource, HttpStatus.OK);
	}
	
	/**
	 * Delete activity
	 * 
	 * @param Activity id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{activityId}", method = RequestMethod.DELETE)
	public HttpEntity<String> delete(@PathVariable Long activityId) throws Exception {
		String result = activityService.delete(activityId);
		return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
	}
}
