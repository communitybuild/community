package com.community.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Rendering Home Page, Old school way of rendering pages, but this project is
 * not an SPA angular-routing cannot be integrated so as Services and Factories.
 * 
 * @author Sameer Shukla
 *
 */
@Controller
public class SimpleUrlMappingController {

	/**
	 * For Index Page
	 * 
	 * @return
	 */
	@RequestMapping("/")
	public String index() {
		return "layout";
	}

	
}
