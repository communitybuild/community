package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.community.model.CommunityNeeds;


public interface CommunityNeedsRepository extends JpaRepository<CommunityNeeds, Long>{

	List<CommunityNeeds> findByCommunityId(Long communityId);

	@Modifying(clearAutomatically = true)
	@Query("delete from CommunityNeeds cn where cn.id = :needID")
	int deleteCommNeed(@Param("needID") Long needID);

	@Query(value="SELECT * FROM community_needs WHERE COMMUNITY_ID=:communityId order by ID desc limit 4",nativeQuery = true)
	List<CommunityNeeds> getListofNeedsOnDashboard(@Param("communityId") Long communityId);

	@Query(value="SELECT * FROM community_needs WHERE COMMUNITY_ID=:id order by CREATE_DATE desc limit 1",nativeQuery = true)
	CommunityNeeds getNeedsForNewsLetter(@Param("id") Long id);
	


}
