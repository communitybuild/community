package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.CommunityManagerEvent;
import com.community.model.CommunityManagerPosts;


@Repository
public interface CommunityManagerPostsRepository extends JpaRepository<CommunityManagerPosts, Long>{

	@Query("SELECT cm FROM CommunityManagerPosts cm WHERE cm.communityID=:managerCommunity")
	List<CommunityManagerPosts> getPostsByCommunityID(@Param("managerCommunity") Long managerCommunity);

	@Query("SELECT cm FROM CommunityManagerEvent cm WHERE cm.communityID = :managerCommunity and cm.eventDate>=CURRENT_TIMESTAMP")
	List<CommunityManagerEvent> getAllCommunityManagerEvents(@Param("managerCommunity") Long managerCommunity);

	@Modifying(clearAutomatically = true)
	@Query("update CommunityManagerPosts cmp set cmp.sharedFlag=1 where cmp.id = :postId")
    int markPostAsShared(@Param("postId") Long postId);

	@Modifying(clearAutomatically = true)
	@Query("update CommunityManagerEvent cmp set cmp.sharedFlag=1 where cmp.id = :eventId")
	int markEventAsShared(@Param("eventId") Long eventId);

	@Query(value="SELECT * FROM posts WHERE COMMUNITY_ID=:communityid order by POST_ID desc limit 6",nativeQuery = true)
    List<CommunityManagerPosts> getAllPostsByCommunityId(@Param("communityid") Long id);

	@Modifying(clearAutomatically = true)
	@Query("delete from CommunityManagerPosts cmp where cmp.id = :postId")
	int deletePost(@Param("postId") Long postId);

	@Modifying(clearAutomatically = true)
	@Query("delete from CommunityManagerEvent cmp where cmp.id = :eventId")
	int deleteEvent(@Param("eventId") Long eventId);

	//@Query(value="SELECT * FROM posts WHERE COMMUNITY_ID=:communityId and CREATE_DATE>=DATE(NOW()) - INTERVAL 6 DAY limit 5",nativeQuery = true)
	@Query(value="SELECT * FROM posts WHERE COMMUNITY_ID=:communityId order by CREATE_DATE desc limit 2",nativeQuery = true)
	List<CommunityManagerPosts> getPostsForNewsLetter(@Param("communityId") Long communityId);


	
}
