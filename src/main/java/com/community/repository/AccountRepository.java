package com.community.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByUsername(String username);
 
    @Modifying(clearAutomatically = true)
  	@Query("update Account cc set cc.password=:password where cc.username = :userName")
  	int updatePasswordByUserName(@Param("password") String password,@Param("userName") String userName);

    @Modifying(clearAutomatically = true)
  	@Query("update Account cc set cc.username=:email where cc.username = :userName")
  	int updateEmailByUserName(@Param("email") String email,@Param("userName") String userName);
}
