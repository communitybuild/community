package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.Community;

@Repository
public interface CommunityRepository extends JpaRepository<Community, Long> {

	Community findByCommunityNameLike(String communityName);

	@Modifying(clearAutomatically = true)
	@Query("update Community cc set cc.communityType=:type, cc.communityHiddenSections=:hidddenSections, cc.communityTitles=:titles where cc.id = :communityId")
	int updateTypeCommunitySettingsInfo(@Param("type") String type,@Param("communityId") Long communityId,@Param("hidddenSections") String hidddenSections,@Param("titles") String titles);

	Community findByReferralCode(String id);

	@Query(value="select REFERRAL_CODE from community where ID=:communityId",nativeQuery=true)
	List<String> findReferralByCommunityId(@Param("communityId") Long communityId);

	@Modifying(clearAutomatically = true)
	@Query(value="update community set COMMUNITY_IMAGES=:imageValues where ID=:communityId",nativeQuery=true)
	int insertCommunityImg(@Param("imageValues") String imageValues,@Param("communityId") Long communityId);

	@Query(value="select c.COMMUNITY_IMAGES,c.ID from consumer_communities con,community c where con.COMMUNITY_ID=c.ID and con.CONSUMER_ID=:consumerId",nativeQuery=true)
	List<Object> findCommImagesByConsumerId(@Param("consumerId") Long consumerId);

	@Query(value="select COMMUNITY_IMAGES,COMMUNITY_SPONSERS from community where ID=:communityId",nativeQuery=true)
	Object findCommImagesByCommunityId(@Param("communityId") Long communityId);

	@Modifying(clearAutomatically = true)
	@Query(value="update community set COMMUNITY_SPONSERS=:imageValues where ID=:communityId",nativeQuery=true)
	int insertCommunitySponsorImages(@Param("imageValues") String imageValues,@Param("communityId") Long communityId);

	@Query("select cc from Community cc where cc.communityParent = :communityParent")
	List<Community> findCommunitybyParent(@Param("communityParent") String communityParent);


	
}

