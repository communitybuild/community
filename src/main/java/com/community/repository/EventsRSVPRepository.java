package com.community.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.EventsRSVP;

@Repository
public interface EventsRSVPRepository extends JpaRepository<EventsRSVP, Long>{

	EventsRSVP findByEventIdAndConsumerId(Long id, Long consumerId);
	
	@Modifying(clearAutomatically = true)
	@Query("delete from EventsRSVP cmp where cmp.eventId = :eventId")
	int deleteEventsRSVP(@Param("eventId") Long eventId);

	@Modifying(clearAutomatically = true)
	@Query("update EventsRSVP cmp set cmp.rsvp=:rsvp where cmp.eventId = :eventId and cmp.consumerId=:consumerId")
	int updateRsvpEvents(@Param("consumerId") Long consumerId,@Param("eventId") Long eventId,@Param("rsvp") String rsvp);

}
