package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.community.model.Forum;


public interface ForumRepository extends JpaRepository<Forum, Long>{

	List<Forum> findByCommunityId(Long id);
	
	@Query("SELECT f.topicReplies FROM Forum f WHERE f.id=:topicId")
	Long getReplyCount(@Param("topicId") Long topicId);

	@Modifying(clearAutomatically = true)
	@Query("update Forum f set f.topicReplies=:replyCnt  WHERE f.id=:topicId")
	int updateReplyCount(@Param("topicId") Long topicId,@Param("replyCnt") Long replyCnt);

	@Modifying(clearAutomatically = true)
	@Query("delete from Forum f WHERE f.id=:topicId")
	void createTopic(@Param("topicId") Long topicId);

}
