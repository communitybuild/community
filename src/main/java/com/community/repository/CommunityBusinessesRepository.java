package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.CommunityBusinesses;

@Repository
public interface CommunityBusinessesRepository extends JpaRepository<CommunityBusinesses, Long> {
	
	List<CommunityBusinesses> findByCommunityId(Long id);

	CommunityBusinesses findByConsumerId(Long consumerId);

	@Modifying(clearAutomatically = true)
	@Query("delete from CommunityBusinesses cmp where cmp.id = :businessId")
	int deleteBusiness(@Param("businessId") Long businessId);

}
