package com.community.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.community.model.SignupforNeeds;


public interface SignupforNeedsRepository extends JpaRepository<SignupforNeeds, Long>{

	@Query(value="SELECT SUM(SIGNUP_QUANTITY) FROM signupforneeds WHERE NEED_ID=:id",nativeQuery = true)
	Long getSignupsCount(@Param("id") Long id);


	@Query(value="SELECT * FROM signupforneeds WHERE NEED_ID=:id",nativeQuery = true)
	List<SignupforNeeds> findNeedsById(@Param("id") Long id);

	@Modifying(clearAutomatically = true)
	@Query("delete from SignupforNeeds cn where cn.needId = :needId and cn.consumerId.id= :consumerId")
	int deleteSignUps(@Param("consumerId") Long consumerId,@Param("needId") Long needId);

	@Modifying(clearAutomatically = true)
	@Query("delete from SignupforNeeds cn where cn.needId = :needID")
	int deleteSignUpsforNeed(@Param("needID") Long needID);

	@Query(value="SELECT SIGNUP_QUANTITY FROM signupforneeds WHERE NEED_ID=:needId and CONSUMER_ID=:consumerId",nativeQuery = true)
	Long getSignupCountByConsumerId(@Param("consumerId") Long consumerId,@Param("needId") Long needId);

	@Modifying(clearAutomatically = true)
	@Query("update SignupforNeeds cn set cn.signupQuantity=:signupCnt where cn.needId = :needId and cn.consumerId.id= :consumerId")
	int updateSignups(@Param("consumerId") Long consumerId,@Param("needId") Long needId,@Param("signupCnt") Long signupCnt);


}
