package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.Consumer;
@Repository
public interface ConsumerRepository extends JpaRepository<Consumer, Long> {

	Consumer findByEmailAddressLike(String emailAddress);

	
	//@Query("SELECT wr FROM Consumer wr WHERE wr.consumerCommunity.activeStatus=1 and wr.consumerCommunity.communityID = :commId")
//	List<Consumer> findConsumersOfCommunity(@Param("commId") Long commId);

	@Modifying(clearAutomatically = true)
	@Query("update Consumer cc set cc.emailAddress=:emailAddress where cc.id = :consumerId")
	int updateEmailCommunitySettingsInfo(@Param("consumerId") Long consumerId, @Param("emailAddress") String emailAddress);

	@Query(value="select DISTINCT(c.NAME) from consumer_communities ch join community c on ch.COMMUNITY_ID=c.ID where ch.CONSUMER_ID=:consumerId",nativeQuery=true)
	List<String> fetchCommunityNameByConsumerId(@Param("consumerId") Long consumerId);

	@Query(value="select EMAIL_ADDRESS from consumer where ID=:consumerId",nativeQuery=true)
	String findEmailByConsumerId(@Param("consumerId") Long consumerId);


	@Modifying(clearAutomatically = true)
  	@Query("update Consumer cc set cc.password=:password where cc.emailAddress = :userName")
	int updatePasswordByUserName(@Param("password") String randomPassword,@Param("userName") String emailid);

//	@Modifying(clearAutomatically = true)
//	@Query("update Consumer cc set cc.emailAddress=: where cc.id = :consumerId")
//	Consumer updateConsumersInfo(@Param("consumerId") Long consumerId);
}

