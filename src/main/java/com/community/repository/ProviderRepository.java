package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.community.model.Provider;

public interface ProviderRepository extends JpaRepository<Provider, Long> {

	Provider findByConsumerId(Long consumerId);

	@Query(value="SELECT p.* FROM provider p left join consumer_communities cc on p.CONSUMER_ID=cc.CONSUMER_ID where cc.COMMUNITY_ID=:communityId order by id desc limit 5", nativeQuery=true)
	List<Provider> findTopFiveProviders(@Param("communityId") Long communityId);

	@Query(value="SELECT c.LAST_NAME FROM provider p join consumer c on p.CONSUMER_ID=c.ID where p.ID=:providerId", nativeQuery=true)
	String findProviderNameByProviderId(@Param("providerId") Long providerId);

	@Modifying(clearAutomatically = true)
	@Query("update Provider set ratingsAggregate=:aggr where ID=:providerId")
	int insertReviewsAggregate(@Param("providerId") Long providerId,@Param("aggr") Float aggr);

	Provider findById(Long providerId);


//	@Query(value="SELECT CONSUMER_ID FROM provider where ID=:providerId", nativeQuery=true)
//	Long findConsumerByProviderId(@Param("providerId") Long providerId);
}

