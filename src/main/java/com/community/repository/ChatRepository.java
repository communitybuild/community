package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.Chat;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {
	
	@Query("SELECT ch FROM Chat ch WHERE ch.workRequestId=:workRequestId")
	List<Chat> findConsumersChatById(@Param("workRequestId") Long workRequestId);

	@Query("SELECT COUNT(ch.id) FROM Chat ch WHERE ch.workRequestId=:workRequestId and ch.receiverID=:receiverId and readFlag=0")
	Long getChatCount(@Param("workRequestId") Long workRequestId,@Param("receiverId") Long receiverId);

	@Modifying(clearAutomatically = true)
	@Query("update Chat ch set ch.readFlag=1 WHERE ch.workRequestId=:workRequestId and ch.receiverID=:consumerId and ch.readFlag=0")
	int markMsgsAsRead(@Param("workRequestId") Long workid,@Param("consumerId") Long consumerId);

}
