package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.WorkRequest;

@Repository
public interface WorkRequestRepository extends JpaRepository<WorkRequest, Long> {
	@Query("SELECT wr FROM WorkRequest wr WHERE wr.communityId = :communityId and wr.processingStatus = 0 and (wr.providerId=:providerId or wr.providerId=0) and wr.consumer.id <> :consumerId and wr.requestDate > :currentDate")
	public List<WorkRequest> findWorkRequestsforCommunityId(@Param("communityId") Long communityId, @Param("consumerId") Long consumerId,@Param("currentDate") String currentDate,@Param("providerId") Long providerId);
	

	@Modifying(clearAutomatically = true)
	@Query("update WorkRequest wr set wr.processingStatus =1, wr.providerId = :providerId where wr.id = :workRequestId")
	int UpdateProcessStatus(@Param("workRequestId") Long workRequestId,@Param("providerId") Long providerId);
	
	
	@Query("SELECT wr FROM WorkRequest wr WHERE wr.providerId = :providerId and wr.processingStatus = 1 and wr.communityId=:communityId and wr.requestDate > :currentDate")
	public List<WorkRequest> findWorkRequestsOfProvider(@Param("providerId") Long providerId,@Param("currentDate") String currentDate,@Param("communityId") Long communityId);
	
	
	//@Query("SELECT wr FROM WorkRequest wr WHERE wr.consumer.id = :id and wr.processingStatus = 1 and wr.communityId=:communityId")
	@Query("SELECT wr FROM WorkRequest wr WHERE wr.consumer.id = :id and wr.communityId=:communityId and wr.requestDate> :currentDate")
	public List<WorkRequest> findWorkRequestPostedByConsumer(@Param("id") Long id,@Param("communityId") Long communityId,@Param("currentDate") String currentDate);

	@Query("SELECT wr FROM WorkRequest wr WHERE wr.providerId = :providerId and wr.requestDate< :currentDate and wr.communityId=:communityId")
    public List<WorkRequest> findWorkRequestsHistoryOfProvider(@Param("providerId") Long providerId,@Param("currentDate") String currentDate,@Param("communityId") Long communityId);


	@Modifying(clearAutomatically = true)
	@Query("update WorkRequest wr set wr.processingStatus =3, wr.actualHours = :actualHours where wr.id = :workRequestId")
	int UpdateProcessStatusToComplete(@Param("workRequestId") Long workRequestId,@Param("actualHours") Float actualHours);

//	@Modifying(clearAutomatically = true)
//	@Query("update WorkRequest wr set wr.ratings =:ratings, wr.reviews = :reviews where wr.id = :workRequestId")
//	public int insertReviews(@Param("workRequestId") Long workRequestId,@Param("ratings") Float ratings,@Param("reviews") String reviews);

    @Query(value="select COUNT('ID') from work_request where CONSUMER_ID=:consumerId",nativeQuery=true)
	public int checkForFirstWorkRequest(@Param("consumerId") Long consumerId);

    @Query(value="select c.FIRST_NAME,c.EMAIL_ADDRESS,wr.ACTIVITY_ID from work_request wr join consumer c on wr.CONSUMER_ID=c.ID where wr.ID=:workRequestId",nativeQuery=true)

    public List<Object[]> getEmailOfConsumerByWorkRequest(@Param("workRequestId") Long workRequestId);

    @Query(value="SELECT w.ID FROM work_request w where w.CONSUMER_ID=:consumerId and w.PROCESSING_STATUS=3 order by w.ID desc limit 1",nativeQuery=true)
   	public Long getLastWorkrequest(@Param("consumerId") Long consumerId);
    
    @Query(value="SELECT COUNT('ID') FROM ratings where WORKREQUEST_ID=:workId",nativeQuery=true)
	public int checkForShowingRatingModal(@Param("workId") Long workId);

}

