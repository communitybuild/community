package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.community.model.CommunityManagerEvent;


public interface CommunityManagerEventRepository extends JpaRepository<CommunityManagerEvent, Long>{

//	@Query("SELECT cm FROM CommunityManagerEvent cm WHERE cm.communityID = :community and cm.eventDate>=CURRENT_TIMESTAMP and cm.sharedFlag=1")
	@Query("SELECT cm FROM CommunityManagerEvent cm WHERE cm.communityID = :community and cm.eventDate>=CURRENT_TIMESTAMP")
	List<CommunityManagerEvent> getAllEventsByCommunityId(@Param("community") Long id);

	@Query(value="SELECT e.EVENT_ID,e.TITLE,er.RSVP,c.FIRST_NAME,c.LAST_NAME,c.PROFILE_PIC FROM events e,events_rsvp er,consumer c where e.EVENT_ID=er.EVENT_ID and er.CONSUMER_ID=c.ID and e.COMMUNITY_ID=:community and e.EVENT_DATE > CURRENT_DATE",nativeQuery=true)
	List<Object> getEventsRSVPforManager(@Param("community") Long id);

	//@Query(value="SELECT * FROM events WHERE COMMUNITY_ID=:id and CREATE_DATE>=DATE(NOW()) - INTERVAL 6 DAY",nativeQuery = true)
	@Query(value="SELECT * FROM events WHERE COMMUNITY_ID=:id and EVENT_DATE>=CURRENT_TIMESTAMP order by EVENT_DATE limit 2",nativeQuery=true)
	List<CommunityManagerEvent> getEventsForNewsLetter(@Param("id") Long id);
}
