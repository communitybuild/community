package com.community.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.Ratings;

@Repository
public interface RatingsRepository extends JpaRepository<Ratings, Long>{

	@Query(value="SELECT AVG(RATINGS) FROM ratings where PROVIDER_ID=:providerId",nativeQuery=true)
	Float getReviewsAggregate(@Param("providerId") Long providerId);

}
