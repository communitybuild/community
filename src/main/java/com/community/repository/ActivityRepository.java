package com.community.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

	Activity findByActivityNameLike(String activityName);

	@Query(value="select NAME from activity where ID=:activityId",nativeQuery=true)
	String findByActivityId(@Param("activityId") Object activityId);
}

