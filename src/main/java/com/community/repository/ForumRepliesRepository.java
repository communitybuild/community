package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.community.model.ForumReplies;


public interface ForumRepliesRepository extends JpaRepository<ForumReplies, Long>{

	List<ForumReplies> findByTopicId(Long topicId);

	@Modifying(clearAutomatically = true)
	@Query("delete from ForumReplies f WHERE f.topicId=:topicId")
	void deleteRepliesforTopic(@Param("topicId") Long topicId);

	@Modifying(clearAutomatically = true)
	@Query("delete from ForumReplies f WHERE f.id=:replyId")
	void deleteReply(@Param("replyId") Long replyId);




}
