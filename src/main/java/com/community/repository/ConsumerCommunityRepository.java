package com.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.community.model.ConsumerCommunity;


@Repository
public interface ConsumerCommunityRepository extends JpaRepository<ConsumerCommunity, Long>{

	@Modifying(clearAutomatically = true)
	@Query("update ConsumerCommunity cc set cc.activeStatus=0 where cc.consumerID = :consumerId and cc.communityID.id= :communityId")
	int removeMemberfromCommunity(@Param("consumerId") Long consumerId,@Param("communityId") Long communityId);

	@Query("select cc from ConsumerCommunity cc where cc.consumerID = :consumerId and cc.activeStatus=1")
	List<ConsumerCommunity> findAll(@Param("consumerId") Long consumerId);
	
	@Query("SELECT wr FROM ConsumerCommunity wr WHERE wr.activeStatus=1 and wr.communityID.id = :commId")
    List<ConsumerCommunity> findConsumersOfCommunity(@Param("commId") Long commId);

	List<ConsumerCommunity> findByConsumerID(Long consumerId);
}
