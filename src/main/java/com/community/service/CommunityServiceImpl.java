package com.community.service;

import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.Activity;
import com.community.model.Community;
import com.community.repository.CommunityRepository;

@Service
@Transactional
public class CommunityServiceImpl implements CommunityService {

	@Autowired
	private CommunityRepository communityRepository;
	
	@Override
	public List<Community> getAllCommunities() throws Exception {
		return communityRepository.findAll();
	}

	@Override
	public Community findByCommunityId(Long id) throws Exception {
		return communityRepository.findOne(id);
	}

	@Override
	public Community findByCommunityName(String communityName) throws Exception {
		return communityRepository.findByCommunityNameLike(communityName);
	}	
	
	@Override
	public Community create(Community community) throws Exception {
		return communityRepository.saveAndFlush(community);
	}

	@Override
	public Community update(Long id, Community community) throws Exception {
		Community existingCommunity = communityRepository.findOne(id);
		if (existingCommunity == null) {
			throw new IllegalArgumentException("Community with id " 
					+ id + " does not exist in system");
		} else {
			community.setId(existingCommunity.getId());
			community.setCreateDate(existingCommunity.getCreateDate());
			return communityRepository.saveAndFlush(community);
		}
	}

	@Override
	public String delete(Long id) throws Exception {
		Community existingCommunity = communityRepository.findOne(id);
		if (existingCommunity == null) {
			throw new IllegalArgumentException("Community with id " 
					+ id + " does not exist in system");
		} else {
			communityRepository.delete(existingCommunity);		
			return "Community '" + existingCommunity.getCommunityName() + "' deleted from system";
		}
	}

	public Community addActivityToCommunity(Activity activity, Community community) throws Exception {
		if (community.getActivities() == null) {
			community.setActivities(new HashSet<Activity>());
		}
		if (community.getActivities().contains(activity)) {
			throw new IllegalArgumentException("Activity '" + activity.getActivityName() 
				+ "' is already added to the community '" + community.getCommunityName() + "'");
		}		
		community.getActivities().add(activity);
		return communityRepository.saveAndFlush(community);
	}
	
	public Community removeActivityFromCommunity(Activity activity, Community community) throws Exception {
		if (community.getActivities() == null || !community.getActivities().contains(activity)) {
			throw new IllegalArgumentException("Activity '" + activity.getActivityName() 
				+ "' is not associated with the community '" + community.getCommunityName() + "'");
		}
		community.getActivities().remove(activity);
		return communityRepository.saveAndFlush(community);
	}

	@Override
	public int updateTypeCommunitySettingsInfo(String type, Long communityId,String hidddenSections,String titles) {
		return communityRepository.updateTypeCommunitySettingsInfo(type,communityId,hidddenSections,titles);
		
	}

	@Override
	public Community findByReferralCode(String id) throws Exception {
		return communityRepository.findByReferralCode(id);
	}

	@Override
	public List<String> findReferralByCommunityId(Long communityId) {
	return communityRepository.findReferralByCommunityId(communityId);
	}

	@Override
	public int insertCommunityImages(String imageValues, Long communityId) {
		return communityRepository.insertCommunityImg(imageValues,communityId);
	}

	@Override
	public List<Object> findCommImagesByConsumerId(Long consumerId) {
		return communityRepository.findCommImagesByConsumerId(consumerId);
	}

	@Override
	public Object findCommImagesByCommunityId(Long communityId) {
		return communityRepository.findCommImagesByCommunityId(communityId);
	}

	@Override
	public int insertCommunitySponsorImages(String imageValues,Long communityId) {
		return communityRepository.insertCommunitySponsorImages(imageValues,communityId);
		
	}

	@Override
	public int deleteSponsorImage(Long communityID, String imageValues) {
		return communityRepository.insertCommunitySponsorImages(imageValues,communityID);
	}

	@Override
	public List<Community> findCommunitybyParent(String communityParent) {
		return communityRepository.findCommunitybyParent(communityParent);
	}




}
