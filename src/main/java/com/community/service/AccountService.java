package com.community.service;

import java.util.List;
import java.util.Set;

import com.community.model.Account;
import com.community.model.Role;

/**
 * The AccountService interface defines all public business behaviors for
 * operations on the Account entity model and some related entities such as
 * Role.
 * 
 */
public interface AccountService {

	List<Account> getAllAccounts() throws Exception;
	
	Account findByAccountId(Long id) throws Exception;
	
    Account findByUsername(String username);
    
    Account create(String username, String password) throws Exception;
    
    String delete(String username) throws Exception;

    Account addRolestoAccount(Account account, Set<Role> roles) throws Exception;
    
    Account removeRolesFromAccount(Account account, Set<Role> roles) throws Exception;
    public int updatePasswordByUserName(String password,String userName) throws Exception;

	int updateEmailByUserName(String email, String userName);
}
