package com.community.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.Forum;
import com.community.model.ForumReplies;
import com.community.repository.ForumRepliesRepository;
import com.community.repository.ForumRepository;


@Service
@Transactional
public class ForumServiceImpl implements ForumService {

	@Autowired
	private ForumRepository forumRepository;
	
	@Autowired
	private ForumRepliesRepository forumRepliesRepository;
	
	@Override
	public void createForumTopic(Forum forum) {
		forumRepository.saveAndFlush(forum);
	}

	@Override
	public List<Forum> findTopicsByCommunityId(Long id) {
		return forumRepository.findByCommunityId(id);
		}

	@Override
	public Forum fetchTopicByTopicId(Long topicId) {
		return forumRepository.findOne(topicId);	
				}

	@Override
	public List<ForumReplies> fetchTopicRepliesByTopicId(Long topicId) {
		return forumRepliesRepository.findByTopicId(topicId);
	}

	@Override
	public void giveReplytoTopic(ForumReplies forum) {
		forumRepliesRepository.saveAndFlush(forum);
	}

	@Override
	public Long getReplyCount(Long topicId) {
		return forumRepository.getReplyCount(topicId);
	}

	@Override
	public int updateReplyCount(Long topicId,Long replyCnt) {
		return forumRepository.updateReplyCount(topicId,replyCnt);
	}

	@Override
	public void deleteTopic(Long topicId) {
		forumRepository.createTopic(topicId);
		return;
	}

	@Override
	public void deleteRepliesforTopic(Long topicId) {
		forumRepliesRepository.deleteRepliesforTopic(topicId);
		return;
	}

	@Override
	public void deleteReply(Long replyId) {
		forumRepliesRepository.deleteReply(replyId);
		return;
	}

}
