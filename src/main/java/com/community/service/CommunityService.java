package com.community.service;

import java.util.List;

import com.community.model.Community;
import com.community.model.Activity;


public interface CommunityService {
	
	public List<Community> getAllCommunities() throws Exception;
	
	public Community findByCommunityId(Long id) throws Exception;
	
	public Community findByCommunityName(String communityName) throws Exception;
	
	public Community create(Community Community) throws Exception;
	
	public Community update(Long id, Community Community) throws Exception;
	
	public String delete(Long id) throws Exception;
	
	public Community addActivityToCommunity(Activity activity, Community community) throws Exception;
	
	public Community removeActivityFromCommunity(Activity activity, Community community) throws Exception;

	public int updateTypeCommunitySettingsInfo(String type, Long communityId, String hidddenSections, String titles);	
	public Community findByReferralCode(String id) throws Exception;

	public List<String> findReferralByCommunityId(Long communityId);

	public int insertCommunityImages(String imageValues, Long communityId);

	public List<Object> findCommImagesByConsumerId(Long consumerId);

	public Object findCommImagesByCommunityId(Long communityId);

	public int insertCommunitySponsorImages(String imageValues,
			Long communityId);
	
	public int deleteSponsorImage(Long communityID, String imageValues);

	public List<Community> findCommunitybyParent(String communityParent);

	//public List<Community> findCommunitybyParent(String communityParent);



}
