package com.community.service;

import com.community.model.Activity;
import com.community.model.Provider;


public interface ProviderService {
	
	public Provider getProviderForConsumer(Long consumerId) throws Exception;
	
	public Provider findByProviderId(Long consumerId, Long providerId) throws Exception;
	
	public Provider findByProviderIdOnly(Long providerId) throws Exception;
	
	public Provider create(Long consumerId, Provider provider) throws Exception;
	
	public Provider update(Long consumerId, Long providerId, Provider provider) throws Exception;
	
	public String delete(Long consumerId, Long providerId) throws Exception;
	
	public Provider addActivityToProvider(Activity activity, Provider provider) throws Exception;
	
	public Provider removeActivityFromProvider(Activity activity, Provider provider) throws Exception;

	public String findProviderNameByProviderId(Long providerId);
	
	public int insertReviewsAggregate(Long providerId, Float aggr);

	public Provider findProviderByProviderId(Long providerId);

	public Provider updateProvider(Long providerId, Provider provider);
	
//	public Long findConsumerByProviderId(Long providerId) throws Exception;
}
