package com.community.service;

import java.util.List;

import com.community.model.Ratings;
import com.community.model.WorkRequest;

public interface WorkRequestService {
	
	public List<WorkRequest> getAllWorkRequests() throws Exception;
	
	public WorkRequest findByWorkRequestId(Long id) throws Exception;
	
	public List<WorkRequest> findByWorkRequestByProviderId(Long id,String currentDate, Long communityId) throws Exception;
	
	public List<WorkRequest> findWorkRequestPostedByConsumer(Long consumerid, Long communityId,String currentDate) throws Exception;
	
	public WorkRequest create(WorkRequest workRequest) throws Exception;
	
	public WorkRequest update(Long id, WorkRequest workRequest) throws Exception;
	
	public String delete(Long id) throws Exception;

	public List<WorkRequest> getAllWorkRequestsByCommunityId(Long communityId,
			Long consumerId,String currentDate, Long providerId);
	public int updateProcessStatusRequest(Long workRequestId, Long providerId)throws Exception;

	public List<WorkRequest> findByWorkRequestHistoryByProviderId(Long providerId, String currentDate, Long communityId);

	public int UpdateProcessStatusToComplete(Long workRequestId,Float actualHours);

	public Ratings insertReviews(Ratings rating);

	public int checkForFirstWorkRequest(Long consumerId);

	public List<Object[]> getEmailOfConsumerByWorkRequest(Long workRequestId);

	public void declineWorkRequest(Long workRequestId);

	public int checkForShowingRatingModal(Long workId);

	public Long getLastWorkrequest(Long consumerId);

	public Float getReviewsAggregate(Long providerId);
}
