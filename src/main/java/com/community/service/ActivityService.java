package com.community.service;

import java.util.List;

import com.community.model.Activity;

public interface ActivityService {
	
	public List<Activity> getAllActivities() throws Exception;
	
	public Activity findByActivityId(Long id) throws Exception;
	
	public Activity findByActivityName(String activityName) throws Exception;
	
	public Activity create(Activity Activity) throws Exception;
	
	public Activity update(Long id, Activity Activity) throws Exception;
	
	public String delete(Long id) throws Exception;

	public String findByActivityId(Object activityId);
}
