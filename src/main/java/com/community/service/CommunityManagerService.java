package com.community.service;

import java.util.List;

import com.community.model.CommunityManagerEvent;
import com.community.model.CommunityManagerPosts;
import com.community.model.EventsRSVP;


public interface CommunityManagerService {
	
	public List<CommunityManagerPosts> getAllCommunityManagerPosts() throws Exception;
	
	public CommunityManagerPosts findByCommunityManagerPostsId(Long id) throws Exception;

	public List<CommunityManagerPosts> getAllCommunityManagerPostsByCommunityId(Long managerCommunity);

	public List<CommunityManagerEvent> getAllCommunityManagerEvents(
			Long managerCommunity);

	public int markPostAsShared(Long postId);

	public int markEventAsShared(Long eventId);

	public CommunityManagerEvent createEvent(CommunityManagerEvent communityManagerEvent);

	public CommunityManagerPosts createPost(CommunityManagerPosts communityManagerpost);

	public List<CommunityManagerPosts> getAllPostsByCommunityId(Long id);

	public List<CommunityManagerEvent> getAllEventsByCommunityId(Long id);

	public EventsRSVP rsvpEvents(EventsRSVP eventsRSVP);

	public EventsRSVP fetchRsvpByEventsAndConsumer(Long id,Long consumerId);

	public List<Object> getEventsRSVPforManager(Long communityId);

	public int deletePost(Long postId);

	public int deleteEvent(Long eventId);

	public int deleteEventsRSVP(Long eventId);

	public int updateRsvpEvents(Long consumerId, Long eventId, String rsvp);

	public List<CommunityManagerPosts> getPostsForNewsLetter(Long communityId);

	public List<CommunityManagerEvent> getEventsForNewsLetter(Long id);
	
	public CommunityManagerEvent getEventInfo(Long eventId);
}
