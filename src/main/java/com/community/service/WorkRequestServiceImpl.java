package com.community.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.Activity;
import com.community.model.Ratings;
import com.community.model.WorkRequest;
import com.community.repository.RatingsRepository;
import com.community.repository.WorkRequestRepository;

@Service
@Transactional
public class WorkRequestServiceImpl implements WorkRequestService {

	@Autowired
	private WorkRequestRepository workRequestRepository;
	
	@Autowired
	private RatingsRepository ratingsRepository;
	
	@Override
	public List<WorkRequest> getAllWorkRequests() throws Exception {
		return workRequestRepository.findAll();
	}

	@Override
	public WorkRequest findByWorkRequestId(Long id) throws Exception {
		return workRequestRepository.findOne(id);
	}
	
	@Override
	public WorkRequest create(WorkRequest workRequest) throws Exception {
		return workRequestRepository.saveAndFlush(workRequest);
	}
	@Override
	public WorkRequest update(Long id, WorkRequest workRequest) throws Exception {
		WorkRequest existingworkRequest = workRequestRepository.findOne(id);
		if (id == null) {
			throw new IllegalArgumentException("WorkRequest with id " 
					+ id + " does not exist in system");
		} else {
			System.out.println("date"+workRequest.getRequestDate());
			workRequest.setId(id);
			Activity activity=workRequest.getActivity();
			workRequest.setActivity(activity);
			workRequest.setConsumer(existingworkRequest.getConsumer());
			workRequest.setActualHours(existingworkRequest.getActualHours());
			workRequest.setProcessingStatus(existingworkRequest.getProcessingStatus());
	    	workRequest.setProviderId(existingworkRequest.getProviderId());
	    	workRequest.setCommunityId(existingworkRequest.getCommunityId());
			return workRequestRepository.saveAndFlush(workRequest);
		}
	}

	@Override
	public String delete(Long id) throws Exception {
		WorkRequest existingworkRequest = workRequestRepository.findOne(id);
		if (existingworkRequest == null) {
			throw new IllegalArgumentException("WorkRequest with id " 
					+ id + " does not exist in system");
		} else {
			workRequestRepository.delete(existingworkRequest);		
			return "WorkRequest '" + existingworkRequest.getDescription() 
				+ "' deleted from system";
		}
	}

//	public WorkRequest assignWorkRequestToProvider(WorkRequest workRequest, Provider provider) throws Exception {
//		if (workRequest.getProviders() == null) {
//			workRequest.setProviders(new HashSet<Provider>());
//		}
//		if (workRequest.getProviders().contains(provider)) {
//			throw new IllegalArgumentException("Provider '" + provider.getConsumer().getFirstName() + " " 
//					+ provider.getConsumer().getLastName() + "' is already assigned to the work request ");
//		}
//		workRequest.setProcessingStatus(1);
//		workRequest.getProviders().add(provider);
//		return workRequestRepository.saveAndFlush(workRequest);
//	}
	
//	public WorkRequest providerStatusForWorkRequest(WorkRequest workRequest, Long provider, 
//			String status) throws Exception 
//	{
//		if (workRequest.getProviders() == null) {
//			throw new IllegalArgumentException("No providers are assigned to the work request ");
//		}
//		if (!workRequest.getProviders().contains(provider)) {
//			throw new IllegalArgumentException("Provider '" + provider.getConsumer().getFirstName() + " " 
//					+ provider.getConsumer().getLastName() + "' is not assigned to the work request ");
//		}
//		if (StringUtils.isNotBlank(status)) {
//			switch(status.toLowerCase()) {
//				case "accept":
//					workRequest.setSelectedProvider(provider);
//					workRequest.setProcessingStatus(2);
//					break;
//					
//				case "reject":
//					workRequest.setSelectedProvider(null);
//					workRequest.setProcessingStatus(0);
//					break;
//
//				case "cancel":
//					workRequest.setProcessingStatus(3);
//					break;
//
//				case "close":
//					// Verify if request closure is necessary
//					if (workRequest.getProcessingStatus() == 0 || workRequest.getSelectedProvider() == null) {
//						throw new IllegalArgumentException("No provider is accepted for the work request");	
//					}
//					workRequest.setProcessingStatus(3);
//					break;
//					
//				default:
//					throw new IllegalArgumentException("Invalid status provided for the work request");
//			}
//			return workRequestRepository.saveAndFlush(workRequest);
//		}
//		return workRequest;
//	}
	

	@Override
	public List<WorkRequest> findByWorkRequestByProviderId(Long id,String currentDate,Long communityId) throws Exception {
		return this.workRequestRepository.findWorkRequestsOfProvider(id,currentDate,communityId);
	}

	@Override
	public List<WorkRequest> findWorkRequestPostedByConsumer(Long consumerid,Long communityId, String currentDate) throws Exception {
		return this.workRequestRepository.findWorkRequestPostedByConsumer(consumerid,communityId,currentDate);
	}

/*	@Override
	public List<WorkRequest> findByCommunityId(
			Set<Community> communityId) throws Exception {
		// TODO Auto-generated method stub
		return this.workRequestRepository.findByCommunityId(communityId);
	}*/
//
//	@Override
//	public List<WorkRequest> findWorkRequestByCommunityId(
//			Long communityId) {
//		// TODO Auto-generated method stub
//		return this.workRequestRepository.findByCommunityId(communityId);
//	}

	public List<WorkRequest> getAllWorkRequestsByCommunityId(Long communityId,Long consumerId,String currentDate,Long providerId) {
		// TODO Auto-generated method stub
		return this.workRequestRepository.findWorkRequestsforCommunityId(communityId,consumerId,currentDate,providerId);
		//return this.workRequestRepository.findWorkRequestsforCommunityId(consumerId);
	}

	@Override
	public int updateProcessStatusRequest(Long workRequestId,Long providerId) throws Exception {
		return this.workRequestRepository.UpdateProcessStatus(workRequestId,providerId);
	}

	@Override
	public List<WorkRequest> findByWorkRequestHistoryByProviderId(Long providerId,String currentDate,Long communityId) {
		return this.workRequestRepository.findWorkRequestsHistoryOfProvider(providerId,currentDate,communityId);
	}

	@Override
	public int UpdateProcessStatusToComplete(Long workRequestId,Float actualHours) {
		return this.workRequestRepository.UpdateProcessStatusToComplete(workRequestId, actualHours);
	}

	@Override
	public Ratings insertReviews(Ratings rating) {
		return ratingsRepository.saveAndFlush(rating);
	}

	@Override
	public int checkForFirstWorkRequest(Long consumerId) {
		return this.workRequestRepository.checkForFirstWorkRequest(consumerId);
	}

	@Override
	public List<Object[]> getEmailOfConsumerByWorkRequest(Long workRequestId) {
		return this.workRequestRepository.getEmailOfConsumerByWorkRequest(workRequestId);
	}

	@Override
	public void declineWorkRequest(Long workRequestId) {
		WorkRequest existingRequest = workRequestRepository.findOne(workRequestId);
		if (existingRequest == null) {
			throw new IllegalArgumentException("No WorkRequest exists in system for ID: " + workRequestId);
		} else {
			workRequestRepository.delete(existingRequest);		
			//return "WorkRequest deleted from system";
		}
		//return this.workRequestRepository.declineWorkRequest(workRequestId);
	}

	@Override
	public int checkForShowingRatingModal(Long workId) {
		return workRequestRepository.checkForShowingRatingModal(workId);
	}

	@Override
	public Long getLastWorkrequest(Long consumerId) {
		return workRequestRepository.getLastWorkrequest(consumerId);
	}

	@Override
	public Float getReviewsAggregate(Long providerId) {
		return ratingsRepository.getReviewsAggregate(providerId);
	}
	
}
