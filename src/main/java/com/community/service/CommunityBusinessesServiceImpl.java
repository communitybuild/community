package com.community.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.CommunityBusinesses;
import com.community.repository.CommunityBusinessesRepository;


@Service
@Transactional
public class CommunityBusinessesServiceImpl implements  CommunityBusinessesService{
	@Autowired
	private CommunityBusinessesRepository communityBusinesses;
	
	@Override
	public CommunityBusinesses create(CommunityBusinesses businessInfo, Long id,Long consumerId) {
		businessInfo.setBusinessName(businessInfo.getBusinessName());
		businessInfo.setCommunityId(id);
		businessInfo.setCommunityImage(businessInfo.getCommunityImage());
		businessInfo.setConsumerId(consumerId);
		businessInfo.setDescription(businessInfo.getDescription());
		businessInfo.setEmailAddress(businessInfo.getEmailAddress());
		businessInfo.setPhoneNumber(businessInfo.getPhoneNumber());
		return communityBusinesses.saveAndFlush(businessInfo);
	}

	@Override
	public List<CommunityBusinesses> showBusinesses(Long id) {
		return communityBusinesses.findByCommunityId(id);
	}

	@Override
	public CommunityBusinesses findBusinessConsumerId(Long consumerId) {
		return communityBusinesses.findByConsumerId(consumerId);
	}

	@Override
	public CommunityBusinesses update(CommunityBusinesses businessInfo, Long id) {
		businessInfo.setId(id);
		return communityBusinesses.saveAndFlush(businessInfo);
	}

	@Override
	public CommunityBusinesses findBusinessBusinessId(Long businessId) {
		return communityBusinesses.findOne(businessId);
	}

	@Override
	public int deleteBusiness(Long businessId) {
		return communityBusinesses.deleteBusiness(businessId);
	}
	
}