package com.community.service;

import java.util.List;

import com.community.model.CommunityBusinesses;

public interface CommunityBusinessesService {

	CommunityBusinesses create(CommunityBusinesses businessInfo, Long id,Long consumerId);

	List<CommunityBusinesses> showBusinesses(Long id);

	CommunityBusinesses findBusinessConsumerId(Long consumerId);

	CommunityBusinesses update(CommunityBusinesses businessInfo, Long id);

	CommunityBusinesses findBusinessBusinessId(Long businessId);

	int deleteBusiness(Long businessId);
	
}
