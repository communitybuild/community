package com.community.service;

import java.util.List;

import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;


public interface ConsumerService {
	
	public List<Consumer> getAllConsumers() throws Exception;
	
	public Consumer findByConsumerId(Long id) throws Exception;
	
	public Consumer findByConsumerEmailAddress(String emailAddress) throws Exception;
	
	public Consumer create(Consumer consumer) throws Exception;
	
	public Consumer update(Long id, Consumer consumer) throws Exception;
	
	public String delete(Long id) throws Exception;
	
	public List<ConsumerCommunity> findCommunity(Long consumerId) throws Exception;

	List<ConsumerCommunity> findConsumersOfCommunity(Long communityId);

	public int removeMemberfromCommunity(Long consumerId,Long communityId);

	public int updateEmailCommunitySettingsInfo(Long consumerId,String emailAddress);

	ConsumerCommunity createEntryInConsumerCommunity(Long consumerId,Long communityId) throws Exception;

	public List<String> fetchCommunityNameByConsumerId(Long consumerId);

	public List<ConsumerCommunity> findCommunitiesByConsumerId(Long consumerId);

	public String findEmailByConsumerId(Long consumerId);

	public int updatePasswordByUserName(String randomPassword, String emailid);

	
}
