package com.community.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.Activity;
import com.community.repository.ActivityRepository;

@Service
@Transactional
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityRepository activityRepository;
	
	@Override
	public List<Activity> getAllActivities() throws Exception {
		return activityRepository.findAll();
	}

	@Override
	public Activity findByActivityId(Long id) throws Exception {
		return activityRepository.findOne(id);
	}
	
	@Override
	public Activity findByActivityName(String activityName) throws Exception {
		return activityRepository.findByActivityNameLike(activityName);
	}	

	@Override
	public Activity create(Activity activity) throws Exception {
		Activity existingActivity = findByActivityName(activity.getActivityName());
		if (existingActivity != null) {
			throw new IllegalArgumentException("Activity with name '" 
					+ activity.getActivityName() + "' already exists in system");
		} else {
			return activityRepository.saveAndFlush(activity);
		}
	}

	@Override
	public Activity update(Long id, Activity activity) throws Exception {
		Activity existingActivity = activityRepository.findOne(id);
		if (existingActivity == null) {
			throw new IllegalArgumentException("Activity with id '" 
					+ id + "' does not exist in system");
		} else {
			activity.setId(existingActivity.getId());
			activity.setCreateDate(existingActivity.getCreateDate());
			return activityRepository.saveAndFlush(activity);
		}
	}

	@Override
	public String delete(Long id) throws Exception {
		Activity existingActivity = activityRepository.findOne(id);
		if (existingActivity == null) {
			throw new IllegalArgumentException("Activity with id '" 
					+ id + "' does not exist in system");
		} else {
			activityRepository.delete(existingActivity);		
			return "Activity '" + existingActivity.getActivityName() + "' deleted from system";
		}
	}

	@Override
	public String findByActivityId(Object activityId) {
		System.out.println("activityjjjdjjd"+activityId);
		return activityRepository.findByActivityId(activityId);
	}

}
