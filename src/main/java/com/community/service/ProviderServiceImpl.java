package com.community.service;

import java.util.Date;
import java.util.HashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.Activity;
import com.community.model.Consumer;
import com.community.model.Provider;
import com.community.repository.ConsumerRepository;
import com.community.repository.ProviderRepository;

@Service
@Transactional
public class ProviderServiceImpl implements ProviderService {

	@Autowired
	private ProviderRepository providerRepository;

	@Autowired
	private ConsumerRepository consumerRepository;

	@Override
	public Provider getProviderForConsumer(Long consumerId) throws Exception {
		return providerRepository.findByConsumerId(consumerId);
	}

	@Override
	public Provider findByProviderId(Long consumerId, Long providerId) throws Exception {
		return providerRepository.findOne(providerId);
	}

	@Override
	public Provider create(Long consumerId, Provider provider) throws Exception {
		/*
		 * String emailAddress = provider.getConsumer() != null ?
		 * provider.getConsumer().getEmailAddress() : ""; if
		 * (StringUtils.isNotEmpty(emailAddress)) { Consumer existingConsumer =
		 * consumerRepository.findByEmailAddressLike(emailAddress); if
		 * (existingConsumer != null) { provider.setConsumer(existingConsumer);
		 * return providerRepository.saveAndFlush(provider); } }
		 */
		Consumer existingConsumer = consumerRepository.findOne(consumerId);
		if (existingConsumer == null) {
			throw new IllegalArgumentException("No Consumer found!");
		} else {
			provider.setConsumer(existingConsumer);
			provider.setCreateDate(new Date());
		//	provider.setLastUpdateDate(new Date());
			return providerRepository.saveAndFlush(provider);
		}
	}

	@Override
	public Provider update(Long consumerId, Long providerId, Provider provider) throws Exception {
		// TODO: Verify if provider is to be fetched by using consumer email
		// address or directly by provider id
		Consumer existingConsumer = consumerRepository.findOne(consumerId);
		if (existingConsumer == null) {
			throw new IllegalArgumentException("No Consumer found!");
		} else {
			Provider existingProvider = providerRepository.findOne(providerId);
			if (existingProvider == null) {
				throw new IllegalArgumentException("No provider found for the given consumer!");
			} else {
				provider.setId(existingProvider.getId());
				// provider.setConsumer(existingProvider.getConsumer());
				provider.setConsumer(existingConsumer);
				provider.setCreateDate(existingProvider.getCreateDate());
				return providerRepository.saveAndFlush(provider);
			}
		}
	}

	@Override
	public String delete(Long consumerId, Long providerId) throws Exception {
		Provider existingProvider = providerRepository.findOne(providerId);
		if (existingProvider == null) {
			throw new IllegalArgumentException("No provider found!");
		} else {
			providerRepository.delete(existingProvider);
			return "Provider '" + existingProvider.getConsumer().getFirstName() + " "
					+ existingProvider.getConsumer().getLastName() + "' deleted from system";
		}
	}

	public Provider addActivityToProvider(Activity activity, Provider provider) throws Exception {
		if (provider.getActivities() == null) {
			provider.setActivities(new HashSet<Activity>());
		}
		if (provider.getActivities().contains(activity)) {
			throw new IllegalArgumentException(
					"Activity '" + activity.getActivityName() + "' is already added to the provider '"
							+ provider.getConsumer().getFirstName() + " " + provider.getConsumer().getLastName() + "'");
		}
		provider.getActivities().add(activity);
		return providerRepository.saveAndFlush(provider);
	}

	public Provider removeActivityFromProvider(Activity activity, Provider provider) throws Exception {
		if (provider.getActivities() == null || !provider.getActivities().contains(activity)) {
			throw new IllegalArgumentException(
					"Activity '" + activity.getActivityName() + "' is not associated with the Provider '"
							+ provider.getConsumer().getFirstName() + " " + provider.getConsumer().getLastName() + "'");
		}
		provider.getActivities().remove(activity);
		return providerRepository.saveAndFlush(provider);
	}

	@Override
	public Provider findByProviderIdOnly(Long providerId) throws Exception {
		return this.providerRepository.findOne(providerId);
	}

	@Override
	public String findProviderNameByProviderId(Long providerId) {
		return this.providerRepository.findProviderNameByProviderId(providerId);
	}

	@Override
	public int insertReviewsAggregate(Long providerId,Float aggr) {
		return this.providerRepository.insertReviewsAggregate(providerId,aggr);
	}

	@Override
	public Provider findProviderByProviderId(Long providerId) {
		return this.providerRepository.findById(providerId);
	}

	@Override
	public Provider updateProvider(Long providerId, Provider provider) {
		provider.setId(providerId);
		return providerRepository.saveAndFlush(provider);
	}



//	@Override
//	public Long findConsumerByProviderId(Long providerId) throws Exception {
//		return this.providerRepository.findConsumerByProviderId(providerId);
//	}

}
