package com.community.service;

import java.util.List;

import com.community.model.Forum;
import com.community.model.ForumReplies;



public interface ForumService {

	void createForumTopic(Forum forum);

	List<Forum> findTopicsByCommunityId(Long id);

	Forum fetchTopicByTopicId(Long topicId);

	List<ForumReplies> fetchTopicRepliesByTopicId(Long topicId);

	void giveReplytoTopic(ForumReplies forum);

	Long getReplyCount(Long topicId);

	int updateReplyCount(Long topicId, Long replyCnt);

	void deleteTopic(Long topicId);

	void deleteRepliesforTopic(Long topicId);

	void deleteReply(Long replyId);


}
