package com.community.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.CommunityManagerEvent;
import com.community.model.CommunityManagerPosts;
import com.community.model.EventsRSVP;
import com.community.repository.CommunityManagerEventRepository;
import com.community.repository.CommunityManagerPostsRepository;
import com.community.repository.EventsRSVPRepository;


@Service
@Transactional
public class CommunityManagerServiceImpl implements CommunityManagerService{

	@Autowired
	private CommunityManagerPostsRepository communitymanagerpostsRepository;
	
	@Autowired
	private CommunityManagerEventRepository communitymanagereventRepository;
	
	@Autowired
	private EventsRSVPRepository EventsRSVPRepository;
	@Override
	public List<CommunityManagerPosts> getAllCommunityManagerPosts()
			throws Exception {
		return this.communitymanagerpostsRepository.findAll();
	}

	@Override
	public CommunityManagerPosts findByCommunityManagerPostsId(Long id)
			throws Exception {
		return communitymanagerpostsRepository.findOne(id);
	}

	@Override
	public List<CommunityManagerPosts> getAllCommunityManagerPostsByCommunityId(
			Long managerCommunity) {
		return this.communitymanagerpostsRepository.getPostsByCommunityID(managerCommunity);
	}

	@Override
	public List<CommunityManagerEvent> getAllCommunityManagerEvents(
			Long managerCommunity) {
		return this.communitymanagerpostsRepository.getAllCommunityManagerEvents(managerCommunity);
	}

	@Override
	public int markPostAsShared(Long postId) {
		return this.communitymanagerpostsRepository.markPostAsShared(postId);
	}

	@Override
	public int markEventAsShared(Long eventId) {
		return this.communitymanagerpostsRepository.markEventAsShared(eventId);
	}

	@Override
	public CommunityManagerEvent createEvent(CommunityManagerEvent communityManagerEvent) {
		return this.communitymanagereventRepository.saveAndFlush(communityManagerEvent);
	}

	@Override
	public CommunityManagerPosts createPost(CommunityManagerPosts communityManagerpost) {
		return this.communitymanagerpostsRepository.saveAndFlush(communityManagerpost);
		
	}

	@Override
	public List<CommunityManagerPosts> getAllPostsByCommunityId(Long id) {
		return this.communitymanagerpostsRepository.getAllPostsByCommunityId(id);
	}

	@Override
	public List<CommunityManagerEvent> getAllEventsByCommunityId(Long id) {
		return this.communitymanagereventRepository.getAllEventsByCommunityId(id);
	}

	@Override
	public EventsRSVP rsvpEvents(EventsRSVP eventsRSVP) {
		return this.EventsRSVPRepository.saveAndFlush(eventsRSVP);
	}

	@Override
	public EventsRSVP fetchRsvpByEventsAndConsumer(Long id,
			Long consumerId) {
		return this.EventsRSVPRepository.findByEventIdAndConsumerId(id,consumerId);
	}

	@Override
	public List<Object> getEventsRSVPforManager(Long communityId) {
		return this.communitymanagereventRepository.getEventsRSVPforManager(communityId);
	}

	@Override
	public int deletePost(Long postId) {
		return this.communitymanagerpostsRepository.deletePost(postId);
	}
	@Override
	public int deleteEvent(Long eventId) {
		return this.communitymanagerpostsRepository.deleteEvent(eventId);
	}
	@Override
	public int deleteEventsRSVP(Long eventId) {
		return this.EventsRSVPRepository.deleteEventsRSVP(eventId);
	}

	@Override
	public int updateRsvpEvents(Long consumerId, Long eventId, String rsvp) {
		return this.EventsRSVPRepository.updateRsvpEvents(consumerId,eventId,rsvp);	
	}

	@Override
	public List<CommunityManagerPosts> getPostsForNewsLetter(Long communityId) {
		return communitymanagerpostsRepository.getPostsForNewsLetter(communityId);
	}

	@Override
	public List<CommunityManagerEvent> getEventsForNewsLetter(Long id) {
		return this.communitymanagereventRepository.getEventsForNewsLetter(id);
	}

	@Override
	public CommunityManagerEvent getEventInfo(Long eventId) {
	return communitymanagereventRepository.findOne(eventId);
	}

}
