package com.community.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.Chat;
import com.community.repository.ChatRepository;

@Service
@Transactional
public class ChatServiceImpl implements ChatService{
	
	@Autowired
	private ChatRepository chatRepository;

	@Override
	public List<Chat> findConsumersChat(Long workRequestId) {
		return chatRepository.findConsumersChatById(workRequestId);
	}

	public Chat create(Chat chat) throws Exception {
		return chatRepository.saveAndFlush(chat);
	}
	public Long getChatCount(Long workRequestId,Long receiverId) throws Exception{
		return chatRepository.getChatCount(workRequestId,receiverId);
	}

	@Override
	public int markMsgsAsRead(Long workid, Long consumerId) {
		return this.chatRepository.markMsgsAsRead(workid,consumerId);
	}

}
