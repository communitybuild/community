package com.community.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.Community;
import com.community.model.Consumer;
import com.community.model.ConsumerCommunity;
import com.community.repository.ConsumerCommunityRepository;
import com.community.repository.ConsumerRepository;
import com.community.util.HashingUtil;

@Service
@Transactional
public class ConsumerServiceImpl implements ConsumerService {

	@Autowired
	private ConsumerRepository consumerRepository;
	
	@Autowired
	private ConsumerCommunityRepository ConsumerCommunityRepository;
	
	
	@Autowired
	private AccountService accountService;
	@Autowired
	private ConsumerCommunityRepository consumercommunityRepository;
	
	@Autowired
	private CommunityService communityService;
	
	@Override
	public List<Consumer> getAllConsumers() throws Exception {
		return consumerRepository.findAll();
	}

	@Override
	public Consumer findByConsumerId(Long id) throws Exception {
		return consumerRepository.findOne(id);
	}

	@Override
	public Consumer findByConsumerEmailAddress(String emailAddress) throws Exception {
		return consumerRepository.findByEmailAddressLike(emailAddress);
	}
	
	@Override
	public Consumer create(Consumer consumer) throws Exception {
//		String emailAddress = consumer.getEmailAddress();
//		Consumer existingConsumer = findByConsumerEmailAddress(emailAddress);
//		if (existingConsumer != null) {
//			throw new IllegalArgumentException("Consumer with email address '" 
//					+ emailAddress + "' already exists in system");
//		} else {
			accountService.create(consumer.getEmailAddress(), consumer.getPassword());
			String hashedPassword = HashingUtil.convertToMd5HashString(consumer.getPassword());
			consumer.setPassword(hashedPassword);
			consumer.setCreateDate(new Date());
		//	consumer.setLastUpdateDate(new Date());
			//consumer.setPassword(new BCryptPasswordEncoder().encode(consumer.getPassword()));
			return consumerRepository.saveAndFlush(consumer);
		//}
	}

	@Override
	public Consumer update(Long id, Consumer consumer) throws Exception {
//		Consumer existingConsumer = consumerRepository.findOne(id);
//		if (existingConsumer == null) {
//			throw new IllegalArgumentException("Consumer with id " 
//					+ id + " does not exist in system");
//		} else {
			// TODO: Do not allow user to change his username and password from this service. 
			// Use 'reset password' service for it
	//		String hashedPassword = HashingUtil.convertToMd5HashString(consumer.getPassword());
		//	consumer.setPassword(hashedPassword);
			//consumer.setPassword(new BCryptPasswordEncoder().encode(consumer.getPassword()));
			consumer.setId(id);
		//	consumer.setCreateDate(existingConsumer.getCreateDate());
			return consumerRepository.saveAndFlush(consumer);
		//}
	}

	@Override
	public String delete(Long id) throws Exception {
		Consumer existingConsumer = consumerRepository.findOne(id);
		if (existingConsumer == null) {
			throw new IllegalArgumentException("Consumer with id " 
					+ id + " does not exist in system");
		} else {
			accountService.delete(existingConsumer.getEmailAddress());
			consumerRepository.delete(existingConsumer);		
			return "Consumer '" + existingConsumer.getFirstName() + " " 
				+ existingConsumer.getLastName() + "' deleted from system";
		}
	}
	
//	public Consumer associateConsumerToCommunity(Consumer consumer, 
//			Community community) throws Exception 
//	{
//		if (consumer.getCommunities() == null) {
//			consumer.setCommunities(new HashSet<Community>());
//		}
//		if (consumer.getCommunities().contains(community)) {
//			throw new IllegalArgumentException("Consumer is already associated with the community " 
//					+ community.getCommunityName());
//		}		
//		consumer.getCommunities().add(community);
//		return consumerRepository.saveAndFlush(consumer);
//	}
	
//	public Consumer removeConsumerFromCommunity(Consumer consumer, 
//			Community community) throws Exception 
//	{
//		if (consumer.getCommunities() == null || !consumer.getCommunities().contains(community)) {
//			throw new IllegalArgumentException("Consumer is not associated with the community " 
//					+ community.getCommunityName());
//		}
//		consumer.getCommunities().remove(community);
//		return consumerRepository.saveAndFlush(consumer);
//	}	
	@Override
	public List<ConsumerCommunity> findCommunity(Long consumerId) throws Exception {
		return consumercommunityRepository.findAll(consumerId);
	}

	@Override
	public List<ConsumerCommunity> findConsumersOfCommunity(Long communityId) {
		//return consumerRepository.findAll();
		return ConsumerCommunityRepository.findConsumersOfCommunity(communityId);
	}

	@Override
	public int removeMemberfromCommunity(Long consumerId,Long communityId) {
		return this.ConsumerCommunityRepository.removeMemberfromCommunity(consumerId,communityId);
	}

	@Override
	public int updateEmailCommunitySettingsInfo(Long consumerId,
			String emailAddress) {
		return consumerRepository.updateEmailCommunitySettingsInfo(consumerId,emailAddress);
	}

	@Override
	public ConsumerCommunity createEntryInConsumerCommunity(Long consumerId,Long communityId) throws Exception {
		   ConsumerCommunity consumerCommunity=new ConsumerCommunity();
		   consumerCommunity.setConsumerID(consumerId);
           Community community=communityService.findByCommunityId(communityId);
	       consumerCommunity.setCommunityID(community);
		   consumerCommunity.setActiveStatus(1);
		   return this.ConsumerCommunityRepository.saveAndFlush(consumerCommunity);
		
	}

	@Override
	public List<String> fetchCommunityNameByConsumerId(Long consumerId) {
		return this.consumerRepository.fetchCommunityNameByConsumerId(consumerId);
	}

	@Override
	public List<ConsumerCommunity> findCommunitiesByConsumerId(Long consumerId) {
		return this.ConsumerCommunityRepository.findByConsumerID(consumerId);
	}

	@Override
	public String findEmailByConsumerId(Long consumerId) {
		return this.consumerRepository.findEmailByConsumerId(consumerId);
	}

	@Override
	public int updatePasswordByUserName(String randomPassword, String emailid) {
		return this.consumerRepository.updatePasswordByUserName(randomPassword,emailid);
	}

	
}
