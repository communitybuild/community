package com.community.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.community.model.CommunityNeeds;
import com.community.model.SignupforNeeds;
import com.community.repository.CommunityNeedsRepository;
import com.community.repository.SignupforNeedsRepository;

@Service
@Transactional
public class CommunityNeedsServiceImpl implements CommunityNeedsService {

	@Autowired
	private CommunityNeedsRepository communityNeedsRepository;
	
	@Autowired
	private SignupforNeedsRepository signupforNeedsRepository;

	@Override
	public CommunityNeeds getNeedByNeedId(Long needId) {
		return communityNeedsRepository.findOne(needId);
	}
	
	@Override
	public CommunityNeeds insertCommunityNeeds(CommunityNeeds communityneeds) {
		return communityNeedsRepository.saveAndFlush(communityneeds);
	}

	@Override
	public List<CommunityNeeds> getListofAllNeeds(Long communityId) {
		return communityNeedsRepository.findByCommunityId(communityId);
	}

	@Override
	public int deleteCommNeed(Long needID) {
		return communityNeedsRepository.deleteCommNeed(needID);
	}

	@Override
	public List<CommunityNeeds> getListofNeedsOnDashboard(Long communityId) {
		return communityNeedsRepository.getListofNeedsOnDashboard(communityId);
	}

	@Override
	public Long getSignupsCount(Long id) {
		return signupforNeedsRepository.getSignupsCount(id);
	}

	@Override
	public List<SignupforNeeds> getListOfSignUps(Long id) {
		return signupforNeedsRepository.findNeedsById(id);
	}

	@Override
	public SignupforNeeds createSignUps(SignupforNeeds signupforNeeds) {
		return signupforNeedsRepository.saveAndFlush(signupforNeeds);
		
	}

	@Override
	public int deleteSignUps(Long consumerId, Long needId) {
		return signupforNeedsRepository.deleteSignUps(consumerId,needId);
	}

	@Override
	public int deleteSignUpsforNeed(Long needID) {
		return signupforNeedsRepository.deleteSignUpsforNeed(needID);
	}

	@Override
	public Long getSignupCountByConsumerId(Long consumerId, Long needId) {
		return signupforNeedsRepository.getSignupCountByConsumerId(consumerId,needId);
	}

	@Override
	public int updateSignups(Long consumerId, Long needId, Long signupCnt) {
		return signupforNeedsRepository.updateSignups(consumerId,needId,signupCnt);
	}

	@Override
	public CommunityNeeds getNeedsForNewsLetter(Long id) {
		return communityNeedsRepository.getNeedsForNewsLetter(id);
	}

	
}
