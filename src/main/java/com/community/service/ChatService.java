package com.community.service;

import java.util.List;

import com.community.model.Chat;

public interface ChatService {
	List<Chat> findConsumersChat(Long workRequestId);
	Chat create(Chat chat) throws Exception;
	Long getChatCount(Long id, Long consumerId) throws Exception;
	int markMsgsAsRead(Long workid, Long consumerId);
}
