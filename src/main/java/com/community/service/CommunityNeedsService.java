package com.community.service;

import java.util.List;

import com.community.model.CommunityNeeds;
import com.community.model.SignupforNeeds;

public interface CommunityNeedsService {

	CommunityNeeds insertCommunityNeeds(CommunityNeeds communityneeds);

	List<CommunityNeeds> getListofAllNeeds(Long communityId);

	int deleteCommNeed(Long needID);

	List<CommunityNeeds> getListofNeedsOnDashboard(Long communityId);

	Long getSignupsCount(Long id);

	List<SignupforNeeds> getListOfSignUps(Long id);

	SignupforNeeds createSignUps(SignupforNeeds signupforNeeds);

	int deleteSignUps(Long consumerId, Long needId);

	int deleteSignUpsforNeed(Long needID);

	CommunityNeeds getNeedByNeedId(Long needId);

	Long getSignupCountByConsumerId(Long consumerId, Long needId);

	int updateSignups(Long consumerId, Long needId, Long signupCnt);

	CommunityNeeds getNeedsForNewsLetter(Long id);

}
