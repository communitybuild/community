package com.community.util;

import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class GenericStoredProcedure extends StoredProcedure {
	public GenericStoredProcedure(JdbcTemplate jdbcTemplate, String sProcName, 
			String[] params, int[] dataTypes) 
	{
		super(jdbcTemplate, sProcName);
		for (int i = 0; i < params.length; i++) {
			declareParameter(new SqlParameter(params[i], dataTypes[i]));
		}
		compile();
	}

	public Map<String, Object> executeSP(Object... parameters) {
		Map<String, Object> results = super.execute(parameters);
		return results;
	}

}
