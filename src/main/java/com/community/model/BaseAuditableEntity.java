/**
 * 
 */
package com.community.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Base entity class with generic auditable fields
 */
@MappedSuperclass
public abstract class BaseAuditableEntity extends BaseEntity {

	private static final long serialVersionUID = 1078253925091161785L;

	protected Date createDate;

	//protected Date lastUpdateDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="LAST_UPDATE_DATE")
//	public Date getLastUpdateDate() {
//		return lastUpdateDate;
//	}
//
//	public void setLastUpdateDate(Date lastUpdateDate) {
//		this.lastUpdateDate = lastUpdateDate;
//	}
}
