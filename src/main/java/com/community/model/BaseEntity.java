/**
 * 
 */
package com.community.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 *
 * Base entity class with generic column details
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable,
		Comparable<BaseEntity> {

	private static final long serialVersionUID = 3078298697158056337L;
	protected Long id;
	
	@Transient
	public abstract Long getId();


	public void setId(Long id) {
		this.id = id;
	}


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(BaseEntity o) {
		if (o == null || o.getId() == null) {
			return 1;
		}
		if (this.id == null) {
			return -1;
		}
		return this.id.compareTo(o.getId());
	}

}
