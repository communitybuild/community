package com.community.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * 
 * @reviewer Sameer Shukla
 *
 *  Not sure why such models has been created which has no link with Request.... Another no brainer model created by Abhijit.
 *  POOR.
 */
@Entity
@Table(name = "ACTIVITY")
public class Activity extends BaseAuditableEntity {

	private static final long serialVersionUID = -8296351397654014687L;

	//@NotNull
	@Size(min = 5, max = 255)
	private String activityName;

	private String description;
	
	private Long activityCost;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name="NAME")
	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getActivityCost() {
		return activityCost;
	}

	public void setActivityCost(Long activityCost) {
		this.activityCost = activityCost;
	}

}
