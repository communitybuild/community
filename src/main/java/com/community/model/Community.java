package com.community.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "COMMUNITY")
public class Community extends BaseAuditableEntity {

	private static final long serialVersionUID = -4190755474997068159L;

	@NotNull
	@Size(min = 5, max = 255)
	private String communityName;

	private String description;
	
	@Size(max = 20)
	private String zipCode;
	      
	private Set<Activity> activities;
	private String communityType;
	
	private String referralCode;
	
	private String communityImages;
	
	private String communitySponsors;
	
	private String communityHiddenSections;
	
	private String communityTitles;
	
	private String communityParent;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="NAME")
	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="ZIP_CODE")
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})  
    @JoinTable(name = "COMMUNITY_ACTIVITIES", 
    		   joinColumns = { @JoinColumn(name = "COMMUNITY_ID", referencedColumnName = "ID") }, 
    		   inverseJoinColumns = { @JoinColumn(name = "ACTIVITY_ID", referencedColumnName = "ID") })	
	public Set<Activity> getActivities() {
		return activities;
	}

	public void setActivities(Set<Activity> activities) {
		this.activities = activities;
	}
	@Column(name="TYPE")
	public String getCommunityType() {
		return communityType;
	}

	public void setCommunityType(String communityType) {
		this.communityType = communityType;
	}
	@Column(name="REFERRAL_CODE")
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	@Column(name="COMMUNITY_IMAGES")
	public String getCommunityImages() {
		return communityImages;
	}

	public void setCommunityImages(String communityImages) {
		this.communityImages = communityImages;
	}

	@Column(name="COMMUNITY_SPONSERS")
	public String getCommunitySponsors() {
		return communitySponsors;
	}

	public void setCommunitySponsors(String communitySponsors) {
		this.communitySponsors = communitySponsors;
	}

	@Column(name="COMMUNITY_SECTIONS_HIDDEN")
	public String getCommunityHiddenSections() {
		return communityHiddenSections;
	}

	public void setCommunityHiddenSections(String communityHiddenSections) {
		this.communityHiddenSections = communityHiddenSections;
	}

	@Column(name="COMMUNITY_TITLES")
	public String getCommunityTitles() {
		return communityTitles;
	}

	public void setCommunityTitles(String communityTitles) {
		this.communityTitles = communityTitles;
	}

	@Column(name="COMMUNITY_PARENT")
	public String getCommunityParent() {
		return communityParent;
	}

	public void setCommunityParent(String communityParent) {
		this.communityParent = communityParent;
	}

}
