package com.community.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RATINGS")
public class Ratings extends BaseAuditableEntity {

	private static final long serialVersionUID = -6663518094885447818L;

	private Long workRequestId;
	private Long providerId;
	private Float ratings;
	private String reviews;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name="PROVIDER_ID")
	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	@Column(name="RATINGS")
	public Float getRatings() {
		return ratings;
	}

	public void setRatings(Float ratings) {
		this.ratings = ratings;
	}
	@Column(name="REVIEWS")
	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}

	@Column(name="WORKREQUEST_ID")
	public Long getWorkRequestId() {
		return workRequestId;
	}

	public void setWorkRequestId(Long workRequestId) {
		this.workRequestId = workRequestId;
	}
	
}
