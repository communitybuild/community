package com.community.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FORUM_TOPIC_REPLIES")
public class ForumReplies extends BaseAuditableEntity {

	private static final long serialVersionUID = -1428398331803554979L;

	
	private Long topicId;
	private String topicReply;
	private String topicReplyAttachment;
	private Consumer consumerId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="REPLY")
	public String getTopicReply() {
		return topicReply;
	}

	public void setTopicReply(String topicReply) {
		this.topicReply = topicReply;
	}

	@Column(name="ATTACHMENT")
	public String getTopicReplyAttachment() {
		return topicReplyAttachment;
	}

	public void setTopicReplyAttachment(String topicReplyAttachment) {
		this.topicReplyAttachment = topicReplyAttachment;
	}
	
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST})
	@JoinColumn(name = "CONSUMER_ID")
	public Consumer getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(Consumer consumerId) {
		this.consumerId = consumerId;
	}

	@Column(name="TOPIC_ID")
	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	
	

	

}
