package com.community.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 *  Another classic example of no brainer Model created by Abhijit, request sent activites 
 *  not considered here, code is written but where the hell Activities will come from
 * 
 *  Rubbish this mapping will create rows in Activity table as well, which is supposed to be Static... POOR again, unnecessary work increased....
 *  
 *  Also, all the mappings missing ID and created getters it clearly indicates that the person 
 *  has blindly copied all the mappings from the internet.
 */
@Entity
@Table(name = "PROVIDER")
public class Provider extends BaseAuditableEntity {

	private static final long serialVersionUID = -8158888845941181921L;

	//private Date dateOfBirth;
	
//	@NotNull
	//@Size(min = 1, max = 50)
	//private String ssn;

//	@NotNull
//	@Size(min = 1, max = 100)
	//private String imageUrl;

	private String description;

	private Consumer consumer;
	
	private Set<Activity> activities;
	
	private Float ratingsAggregate;
	
	public Provider() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

//	@Column(name="DATE_OF_BIRTH")
//	public Date getDateOfBirth() {
//		return dateOfBirth;
//	}
//
//	public void setDateOfBirth(Date dateOfBirth) {
//		this.dateOfBirth = dateOfBirth;
//	}
//
//	@Column(name="SSN")
//	public String getSsn() {
//		return ssn;
//	}
//
//	public void setSsn(String ssn) {
//		this.ssn = ssn;
//	}
//
//	@Column(name="PROFILE_PIC_URL")
//	public String getImageUrl() {
//		return imageUrl;
//	}
//
//	public void setImageUrl(String imageUrl) {
//		this.imageUrl = imageUrl;
//	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CONSUMER_ID", nullable = false)
	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	//I am 100% sure that this mapping has been copied like a fool from internet... removed Cascade  Merge, Poor mapping done by Abhijit
    @ManyToMany(fetch = FetchType.EAGER)  
    @JoinTable(name = "PROVIDER_ACTIVITIES", 
    		   joinColumns = { @JoinColumn(name = "PROVIDER_ID", referencedColumnName = "ID") }, 
    		   inverseJoinColumns = { @JoinColumn(name = "ACTIVITY_ID", referencedColumnName = "ID") })	
	public Set<Activity> getActivities() {
		return activities;
	}

	public void setActivities(Set<Activity> activities) {
		this.activities = activities;
	}

	@Column(name="RATINGS_AGGREGATE")
	public Float getRatingsAggregate() {
		return ratingsAggregate;
	}

	public void setRatingsAggregate(Float ratingsAggregate) {
		this.ratingsAggregate = ratingsAggregate;
	}
		
}
