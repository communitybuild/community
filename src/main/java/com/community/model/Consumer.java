package com.community.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CONSUMER")
public class Consumer extends BaseAuditableEntity {

	private static final long serialVersionUID = -1428398331803554979L;

	//@NotNull
	//@Size(min = 1, max = 300)
	private String firstName;

	//@NotNull
//	@Size(min = 1, max = 300)
	private String lastName;

//	@NotNull
	//@Size(max = 255)
	//@Email
	private String emailAddress;

//	@NotNull
	//@Size(min = 6, max = 200)	
	private String password;

	//@Size(max = 20)
	private String zipCode;

	//@NotNull
	private Long phoneNumber;
//	  @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE}) 
//	    @JoinColumn(name="ID",referencedColumnName = "CONSUMER_ID")
//	private ConsumerCommunity communities;
	private String profilePic;
	//private List<ConsumerCommunity> ConsumerCommunity;
	private Long managerCommunity;
	private String ssn;
	private String dob;
	public Consumer() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="LAST_NAME")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name="EMAIL_ADDRESS")
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name="ZIP_CODE")
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name="PHONE_NUMBER")
	public Long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

  
//    @JoinTable(name = "CONSUMER_COMMUNITIES", 
//    		   joinColumns = { @JoinColumn(name = "CONSUMER_ID", referencedColumnName = "ID") }, 
//    		   inverseJoinColumns = { @JoinColumn(name = "COMMUNITY_ID", referencedColumnName = "ID") })
//	public ConsumerCommunity getCommunities() {
//		return communities;
//	}
//
//	public void setCommunities(ConsumerCommunity communities) {
//		this.communities = communities;
//	}
//	@OneToMany(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST})
//	@JoinColumn(name = "CONSUMER_ID",insertable=false,updatable=false)
//	public List<ConsumerCommunity> getConsumerCommunity() {
//		return ConsumerCommunity;
//	}
//
//	public void setConsumerCommunity(List<ConsumerCommunity> consumerCommunity) {
//		ConsumerCommunity = consumerCommunity;
//	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public Long getManagerCommunity() {
		return managerCommunity;
	}

	public void setManagerCommunity(Long managerCommunity) {
		this.managerCommunity = managerCommunity;
	}
	@Column(name="SSN")
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@Column(name="DOB")
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

}
