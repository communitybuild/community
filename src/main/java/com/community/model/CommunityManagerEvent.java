package com.community.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EVENTS")
public class CommunityManagerEvent extends BaseAuditableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1996508693459219782L;
	
    private String title;
	
	private String description;
	
	private Long communityID;
	
	private Long consumerID;
	private String eventDate;
	private String startTime;
	
	private String endTime;
	private String location;
	private String eventImg;
	private Integer sharedFlag;

//	@ManyToOne
//	@JoinColumn(name="POST_ID")
//	private CommunityManagerPosts communityManagerPosts;
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "communityManagerPosts")
//	private CommunityManagerPosts communityManagerPosts;
//	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EVENT_ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="TITLE")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="COMMUNITY_ID")
	public Long getCommunityID() {
		return communityID;
	}

	public void setCommunityID(Long communityID) {
		this.communityID = communityID;
	}

	@Column(name="CONSUMER_ID")
	public Long getConsumerID() {
		return consumerID;
	}

	public void setConsumerID(Long consumerID) {
		this.consumerID = consumerID;
	}

	@Column(name="SHARED_FLAG")
	public Integer getSharedFlag() {
		return sharedFlag;
	}

	public void setSharedFlag(Integer sharedFlag) {
		this.sharedFlag = sharedFlag;
	}
	@Column(name="START_TIME")
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	@Column(name="EVENT_DATE")
	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	@Column(name="END_TIME")
	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Column(name="LOCATION")
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	@Column(name="EVENT_IMAGE")
	public String getEventImg() {
		return eventImg;
	}

	public void setEventImg(String eventImg) {
		this.eventImg = eventImg;
	}
	

}
