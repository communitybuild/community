package com.community.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "POSTS")
public class CommunityManagerPosts extends BaseAuditableEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5922217575639974068L;

	private String image;
	
	private String description;
	
	private Long communityID;
	
	private Long consumerID;
	private Integer sharedFlag;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POST_ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="IMAGE")
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="COMMUNITY_ID")
	public Long getCommunityID() {
		return communityID;
	}

	public void setCommunityID(Long communityID) {
		this.communityID = communityID;
	}
	@Column(name="CONSUMER_ID")
	public Long getConsumerID() {
		return consumerID;
	}

	public void setConsumerID(Long consumerID) {
		this.consumerID = consumerID;
	}
	@Column(name="SHARED_FLAG")
	public Integer getSharedFlag() {
		return sharedFlag;
	}

	public void setSharedFlag(Integer sharedFlag) {
		this.sharedFlag = sharedFlag;
	}

//	@Column(name="CONSUMER_ID")

//	@OneToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "CONSUMER_ID", nullable = false,insertable=false,updatable=false)
//	public Consumer getConsumer() {
//		return consumer;
//	}
//
//	public void setConsumer(Consumer consumer) {
//		this.consumer = consumer;
//	}


}
