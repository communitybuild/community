package com.community.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "WORK_REQUEST")
public class WorkRequest extends BaseAuditableEntity {

	private static final long serialVersionUID = -6663518094885447818L;

	//@NotNull
	private String requestDate;

	private String description;
	
	private Integer estimatedHours = 0;
	
	private Double estimatedCost = 0D;
	
	private Integer processingStatus = 0;
	
	private Consumer consumer;
	
	private Activity activity;
	
//	private Provider selectedProvider;
	private Long providerId;
	private Long communityId;
	private Float actualHours;
	private Float ratings;
	private String reviews;
//	private Set<Provider> providers;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	
	@Column(name="REQUEST_DATE")
	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="ESTIMATED_HOURS")
	public Integer getEstimatedHours() {
		return estimatedHours;
	}

	public void setEstimatedHours(Integer estimatedHours) {
		this.estimatedHours = estimatedHours;
	}
	
	@Column(name="ESTIMATED_COST")
	public Double getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(Double estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	@Column(name="PROCESSING_STATUS")
	public Integer getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(Integer processingStatus) {
		this.processingStatus = processingStatus;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name = "CONSUMER_ID", nullable = false)
	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.MERGE})
	@JoinColumn(name = "ACTIVITY_ID", nullable = false)
	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

//	@OneToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "PROVIDER_ID", nullable = true)
//    public Provider getSelectedProvider() {
//		return selectedProvider;
//	}
//
//	public void setSelectedProvider(Provider selectedProvider) {
//		this.selectedProvider = selectedProvider;
//	}

//	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})  
//    @JoinTable(name = "WORK_REQUEST_PROVIDERS", 
//    		   joinColumns = { @JoinColumn(name = "WORK_REQUEST_ID", referencedColumnName = "ID") }, 
//    		   inverseJoinColumns = { @JoinColumn(name = "PROVIDER_ID", referencedColumnName = "ID") })	
//	public Set<Provider> getProviders() {
//		return providers;
//	}
//
//	public void setProviders(Set<Provider> providers) {
//		this.providers = providers;
//	}
	@Column(name="PROVIDER_ID")
	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}
	@Column(name="COMMUNITY_ID")
	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}
	@Column(name="ACTUAL_HOURS")
	public Float getActualHours() {
		return actualHours;
	}

	public void setActualHours(Float actualHours) {
		this.actualHours = actualHours;
	}
	@Column(name="RATINGS")
	public Float getRatings() {
		return ratings;
	}

	public void setRatings(Float ratings) {
		this.ratings = ratings;
	}
	@Column(name="REVIEWS")
	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}
	
}
