package com.community.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CHAT")
public class Chat extends BaseAuditableEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6155241825229929178L;
	
    private Long senderID;
	
	private Long receiverID;
	
	private String chatContent;
	
	private Long workRequestId;
 
	private int readFlag;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="RECEIVER_ID")
	public Long getReceiverID() {
		return receiverID;
	}

	public void setReceiverID(Long receiverID) {
		this.receiverID = receiverID;
	}

	@Column(name="SENDER_ID")
	public Long getSenderID() {
		return senderID;
	}

	public void setSenderID(Long senderID) {
		this.senderID = senderID;
	}

	@Column(name="CHAT_CONTENT")
	public String getChatContent() {
		return chatContent;
	}

	public void setChatContent(String chatContent) {
		this.chatContent = chatContent;
	}

	@Column(name="WORKREQUEST_ID")
	public Long getWorkRequestId() {
		return workRequestId;
	}

	public void setWorkRequestId(Long workRequestId) {
		this.workRequestId = workRequestId;
	}
	@Column(name="READ_FLAG")
	public int getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(int readFlag) {
		this.readFlag = readFlag;
	}

}
