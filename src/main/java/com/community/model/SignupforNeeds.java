package com.community.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "signupforneeds")
public class SignupforNeeds extends BaseAuditableEntity {

	private static final long serialVersionUID = -1428398331803554979L;

	
	private Long needId;
	private Consumer consumerId;
	private Long signupQuantity;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public Long getNeedId() {
		return needId;
	}

	public void setNeedId(Long needId) {
		this.needId = needId;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST})
	@JoinColumn(name = "CONSUMER_ID")
	public Consumer getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(Consumer consumerId) {
		this.consumerId = consumerId;
	}

	public Long getSignupQuantity() {
		return signupQuantity;
	}

	public void setSignupQuantity(Long signupQuantity) {
		this.signupQuantity = signupQuantity;
	}

}
