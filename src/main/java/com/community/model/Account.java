package com.community.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Account describes the security credentials and authentication flags that permit 
 * access to application functionality.
 * 
 */
@Entity
@Table(name = "ACCOUNT")
public class Account extends BaseAuditableEntity {

	private static final long serialVersionUID = -93043523537810203L;

	@NotNull
    private String username;

    @NotNull
    private String password;

    private boolean enabled = true;

    private boolean credentialsexpired = false;

    private boolean expired = false;

    private boolean locked = false;

    private Set<Role> roles;

    public Account() {
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name="USERNAME")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name="PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name="ENABLED")
    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name="CREDENTIALSEXPIRED")
    public boolean getCredentialsexpired() {
        return credentialsexpired;
    }

    public void setCredentialsexpired(boolean credentialsexpired) {
        this.credentialsexpired = credentialsexpired;
    }

    @Column(name="EXPIRED")
    public boolean getExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    @Column(name="LOCKED")
    public boolean getLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "ACCOUNT_ROLES",
            joinColumns = @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID") ,
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID") )
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

}
