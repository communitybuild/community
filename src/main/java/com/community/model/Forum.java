package com.community.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FORUM_TOPICS")
public class Forum extends BaseAuditableEntity {

	private static final long serialVersionUID = -1428398331803554979L;

	
	private String topicTitle;
	private String topicDescription;
	private String topicAttachment;
	private Consumer consumerId;
	private Long topicReplies;
	private Long communityId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="TOPIC_TITLE")
	public String getTopicTitle() {
		return topicTitle;
	}

	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}

	@Column(name="TOPIC_DESCRIPTION")
	public String getTopicDescription() {
		return topicDescription;
	}

	public void setTopicDescription(String topicDescription) {
		this.topicDescription = topicDescription;
	}

	@Column(name="TOPIC_ATTACHMENT")
	public String getTopicAttachment() {
		return topicAttachment;
	}

	public void setTopicAttachment(String topicAttachment) {
		this.topicAttachment = topicAttachment;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST})
	@JoinColumn(name = "CONSUMER_ID")
	public Consumer getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(Consumer consumerId) {
		this.consumerId = consumerId;
	}

	@Column(name="TOPIC_REPLIES")
	public Long getTopicReplies() {
		return topicReplies;
	}

	public void setTopicReplies(Long topicReplies) {
		this.topicReplies = topicReplies;
	}

	@Column(name="COMMUNITY_ID")
	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	
	

	

}
