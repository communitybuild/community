package com.community.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EVENTS_RSVP")
public class EventsRSVP extends BaseAuditableEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6698800171649485687L;
	/**
	 * 
	 */
	
	private Long eventId;
	private Long consumerId;
	private String rsvp;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name = "EVENT_ID")
	public Long getEventId() {
		return eventId;
	}


	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	@Column(name = "CONSUMER_ID")
	public Long getConsumerId() {
		return consumerId;
	}


	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}

	@Column(name = "RSVP")
	public String getRsvp() {
		return rsvp;
	}


	public void setRsvp(String rsvp) {
		this.rsvp = rsvp;
	}
	

}
