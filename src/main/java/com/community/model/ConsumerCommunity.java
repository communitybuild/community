package com.community.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CONSUMER_COMMUNITIES")
public class ConsumerCommunity extends BaseAuditableEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6698800171649485687L;
	/**
	 * 
	 */
	
	private Community communityID;
	private Long consumerID;
	private int activeStatus;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}
	//@Id
	//@Column(name = "COMMUNITY_ID", unique = false, nullable = false)
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST})
	@JoinColumn(name = "COMMUNITY_ID")
	public Community getCommunityID() {
		return communityID;
	}
	public void setCommunityID(Community communityID) {
		this.communityID = communityID;
	}
	//@Id
	@Column(name = "CONSUMER_ID" , unique = false, nullable = false)
	public Long getConsumerID() {
		return consumerID;
	}
	public void setConsumerID(Long consumerID) {
		this.consumerID = consumerID;
	}
	public int getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(int i) {
		this.activeStatus = i;
	}
	
}
