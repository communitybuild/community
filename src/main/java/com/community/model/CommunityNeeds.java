package com.community.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COMMUNITY_NEEDS")
public class CommunityNeeds extends BaseAuditableEntity {

	private static final long serialVersionUID = -1428398331803554979L;

	
	private String needName;
	private Long communityId;
	private Long quantity;
	private String needComment;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	@Column(name="NEED_NAME")
	public String getNeedName() {
		return needName;
	}

	public void setNeedName(String needName) {
		this.needName = needName;
	}

	@Column(name="COMMUNITY_ID")
	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	@Column(name="QUANTITY")
	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	@Column(name="NEED_COMMENT")
	public String getNeedComment() {
		return needComment;
	}

	public void setNeedComment(String needComment) {
		this.needComment = needComment;
	}
	

}
