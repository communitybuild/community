package com.community.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The Role describes a privilege level within the application. 
 * A Role is used to authorize an Account to access a set of application resources.
 * 
 */
@Entity
@Table(name = "ROLE")
public class Role extends BaseEntity {

	private static final long serialVersionUID = -2669926573826780310L;

	@NotNull
    private String roleName;

    @NotNull
    private String description;

    public Role() {
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name="ROLE_NAME")
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
