package com.community.resource;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResource extends ResourceSupport {

//	@JsonProperty(value = "id", access = Access.WRITE_ONLY)
	@JsonProperty(value = "id")
	private Long entityId;
	
    public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long id) {
		this.entityId = id;
	}
}
