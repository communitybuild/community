package com.community.resource;

public class EventsRSVPResource extends BaseResource {
	private Long eventId;
	private Long consumerId;
	private String rsvp;
	public Long getEventId() {
		return eventId;
	}
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}
	public Long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}
	public String getRsvp() {
		return rsvp;
	}
	public void setRsvp(String rsvp) {
		this.rsvp = rsvp;
	}

}
