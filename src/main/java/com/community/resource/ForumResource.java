package com.community.resource;

import java.util.Date;
import java.util.List;

import com.community.model.Consumer;
import com.community.model.ForumReplies;


public class ForumResource extends BaseResource {

	private String topicTitle;
	private String topicDescription;
	private String topicAttachment;
	private Consumer consumerId;
	private Long topicReplies;
	private Long communityId;
	private List<ForumReplies> forumReplies;
	private Date createDate;
	
	public String getTopicTitle() {
		return topicTitle;
	}
	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}
	public String getTopicDescription() {
		return topicDescription;
	}
	public void setTopicDescription(String topicDescription) {
		this.topicDescription = topicDescription;
	}
	public String getTopicAttachment() {
		return topicAttachment;
	}
	public void setTopicAttachment(String topicAttachment) {
		this.topicAttachment = topicAttachment;
	}
	public Consumer getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Consumer consumerId) {
		this.consumerId = consumerId;
	}
	public Long getTopicReplies() {
		return topicReplies;
	}
	public void setTopicReplies(Long topicReplies) {
		this.topicReplies = topicReplies;
	}
	public Long getCommunityId() {
		return communityId;
	}
	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}
	public List<ForumReplies> getForumReplies() {
		return forumReplies;
	}
	public void setForumReplies(List<ForumReplies> forumReplies) {
		this.forumReplies = forumReplies;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
