package com.community.resource;

import java.util.List;

import com.community.model.ConsumerCommunity;

public class ConsumerResource extends BaseResource {
	private String firstName;

	private String lastName;

	private String emailAddress;

//	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;

	private String zipCode;

	private Long phoneNumber;
	private String profilePic;
//	private Set<Community> communities;
	private List<ConsumerCommunity> ConsumerCommunity;
    private Long managerCommunity;
    private String ssn;
	private String dob;
	private String communityType;
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

//	public Set<Community> getCommunities() {
//		return communities;
//	}
//
//	public void setCommunities(Set<Community> communities) {
//		this.communities = communities;
//	}

	public Long getManagerCommunity() {
		return managerCommunity;
	}

	public void setManagerCommunity(Long managerCommunity) {
		this.managerCommunity = managerCommunity;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getCommunityType() {
		return communityType;
	}

	public void setCommunityType(String communityType) {
		this.communityType = communityType;
	}

	public List<ConsumerCommunity> getConsumerCommunity() {
		return ConsumerCommunity;
	}

	public void setConsumerCommunity(List<ConsumerCommunity> consumerCommunity) {
		ConsumerCommunity = consumerCommunity;
	}


}
