package com.community.resource;

public class ChatResource extends BaseResource{
	
	private Long senderID;
	
	private Long receiverID;
	
	private String chatContent;
	
	private Long workRequestId;
	
	private int readFlag;

	public Long getSenderID() {
		return senderID;
	}

	public void setSenderID(Long senderID) {
		this.senderID = senderID;
	}

	public Long getReceiverID() {
		return receiverID;
	}

	public void setReceiverID(Long receiverID) {
		this.receiverID = receiverID;
	}

	public String getChatContent() {
		return chatContent;
	}

	public void setChatContent(String chatContent) {
		this.chatContent = chatContent;
	}

	public Long getWorkRequestId() {
		return workRequestId;
	}

	public void setWorkRequestId(Long workRequestId) {
		this.workRequestId = workRequestId;
	}

	public int getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(int readFlag) {
		this.readFlag = readFlag;
	}

	

}
