package com.community.resource;

import com.community.model.Community;

public class ConsumerCommunityResource extends BaseResource {
	private Community communityID;
	private Long consumerID;
	private int activeStatus;

	public Community getCommunityID() {
		return communityID;
	}
	public void setCommunityID(Community communityID) {
		this.communityID = communityID;
	}
	
	public Long getConsumerID() {
		return consumerID;
	}
	public void setConsumerID(Long consumerID) {
		this.consumerID = consumerID;
	}
	public int getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}
	

}
