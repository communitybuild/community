package com.community.resource;

import java.util.Date;


public class CommunityManagerPostsResource extends BaseResource{
	
    private Long postID;
	
	private String image;
	
	private String description;
	
	private Long communityID;
	
	private Long consumerID;
	private String managerFName;
	private String managerLName;
	private String profImg;
	private Integer sharedFlag;
	private Date createDate;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCommunityID() {
		return communityID;
	}

	public void setCommunityID(Long communityID) {
		this.communityID = communityID;
	}

	public Long getConsumerID() {
		return consumerID;
	}

	public void setConsumerID(Long consumerID) {
		this.consumerID = consumerID;
	}

	public Long getPostID() {
		return postID;
	}

	public void setPostID(Long postID) {
		this.postID = postID;
	}

	public String getManagerFName() {
		return managerFName;
	}

	public void setManagerFName(String managerFName) {
		this.managerFName = managerFName;
	}

	public String getManagerLName() {
		return managerLName;
	}

	public void setManagerLName(String managerLName) {
		this.managerLName = managerLName;
	}

	public String getProfImg() {
		return profImg;
	}

	public void setProfImg(String profImg) {
		this.profImg = profImg;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getSharedFlag() {
		return sharedFlag;
	}

	public void setSharedFlag(Integer sharedFlag) {
		this.sharedFlag = sharedFlag;
	}


}
