package com.community.resource;

import java.util.Date;

import com.community.model.EventsRSVP;

public class CommunityManagerEventResource extends BaseResource {
	
	private Long eventID;
	
	private String title;
	
	private String description;
	
	private Long communityID;
	
	private Long consumerID;
	
	private String eventDate;
	private String startTime;
	
	private String endTime;
	private String location;
	private String eventImg;
	private String managerFName;
	private String managerLName;
	private String profImg;
	private Date createDate;
	private Integer sharedFlag;

	private EventsRSVP eventsrsvp;
	public Long getEventID() {
		return eventID;
	}

	public void setEventID(Long eventID) {
		this.eventID = eventID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCommunityID() {
		return communityID;
	}

	public void setCommunityID(Long communityID) {
		this.communityID = communityID;
	}

	public Long getConsumerID() {
		return consumerID;
	}

	public void setConsumerID(Long consumerID) {
		this.consumerID = consumerID;
	}

	public String getManagerFName() {
		return managerFName;
	}

	public void setManagerFName(String managerFName) {
		this.managerFName = managerFName;
	}

	public String getManagerLName() {
		return managerLName;
	}

	public void setManagerLName(String managerLName) {
		this.managerLName = managerLName;
	}

	public String getProfImg() {
		return profImg;
	}

	public void setProfImg(String profImg) {
		this.profImg = profImg;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getSharedFlag() {
		return sharedFlag;
	}

	public void setSharedFlag(Integer sharedFlag) {
		this.sharedFlag = sharedFlag;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEventImg() {
		return eventImg;
	}

	public void setEventImg(String eventImg) {
		this.eventImg = eventImg;
	}

	public EventsRSVP getEventsrsvp() {
		return eventsrsvp;
	}

	public void setEventsrsvp(EventsRSVP eventsRsvpResource) {
		this.eventsrsvp = eventsRsvpResource;
	}
}
