package com.community.resource;

public class CommunityResource extends BaseResource {

	private String communityName;

	private String description;
	
	private String zipCode;
	
	private String referralCode;
	
	private String communityType;
	
	private String communityImages;
	
	private String communitySponsors;
	
	private String communityHiddenSections;
	
	private String communityTitles;
	
	private String communityParent;

	
	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getCommunityImages() {
		return communityImages;
	}

	public void setCommunityImages(String communityImages) {
		this.communityImages = communityImages;
	}
	
	public String getCommunitySponsors() {
		return communitySponsors;
	}

	public void setCommunitySponsors(String communitySponsors) {
		this.communitySponsors = communitySponsors;
	}

	public String getCommunityType() {
		return communityType;
	}

	public void setCommunityType(String communityType) {
		this.communityType = communityType;
	}

	public String getCommunityHiddenSections() {
		return communityHiddenSections;
	}

	public void setCommunityHiddenSections(String communityHiddenSections) {
		this.communityHiddenSections = communityHiddenSections;
	}

	public String getCommunityTitles() {
		return communityTitles;
	}

	public void setCommunityTitles(String communityTitles) {
		this.communityTitles = communityTitles;
	}

	public String getCommunityParent() {
		return communityParent;
	}

	public void setCommunityParent(String communityParent) {
		this.communityParent = communityParent;
	}
}
