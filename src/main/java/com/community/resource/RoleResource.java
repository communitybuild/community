package com.community.resource;

public class RoleResource extends BaseResource {

    private String roleName;

    private String description;

    public RoleResource() {
    }

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
