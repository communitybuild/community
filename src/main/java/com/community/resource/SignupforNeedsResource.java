package com.community.resource;

import com.community.model.Consumer;

public class SignupforNeedsResource extends BaseResource {
	
	private Long needId;
	private Consumer consumerId;
	private Long signupQuantity;
	public Long getNeedId() {
		return needId;
	}
	public void setNeedId(Long needId) {
		this.needId = needId;
	}
	public Consumer getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Consumer consumerId) {
		this.consumerId = consumerId;
	}
	public Long getSignupQuantity() {
		return signupQuantity;
	}
	public void setSignupQuantity(Long signupQuantity) {
		this.signupQuantity = signupQuantity;
	}

}
