package com.community.resource;

import java.util.Date;
import java.util.List;

import com.community.model.SignupforNeeds;

public class CommunityNeedsResource extends BaseResource {
	private String needName;
	private Long communityId;
	private Long quantity;
	private String needComment;
    private Date createDate;
    private List<SignupforNeeds> signups;
	
	public String getNeedName() {
		return needName;
	}
	public void setNeedName(String needName) {
		this.needName = needName;
	}
	
	public Long getCommunityId() {
		return communityId;
	}
	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}
	
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getNeedComment() {
		return needComment;
	}
	public void setNeedComment(String needComment) {
		this.needComment = needComment;
	}
	public List<SignupforNeeds> getSignups() {
		return signups;
	}
	public void setSignups(List<SignupforNeeds> signups) {
		this.signups = signups;
	}

}
