package com.community.resource;

import java.util.Date;

import com.community.model.Consumer;


public class ForumRepliesResource extends BaseResource {
    
	private Long topicId;
	private String topicReply;
	private String topicReplyAttachment;
	private Consumer consumerId;
	private Date createDate;
	
	public String getTopicReply() {
		return topicReply;
	}
	public void setTopicReply(String topicReply) {
		this.topicReply = topicReply;
	}
	public String getTopicReplyAttachment() {
		return topicReplyAttachment;
	}
	public void setTopicReplyAttachment(String topicReplyAttachment) {
		this.topicReplyAttachment = topicReplyAttachment;
	}
	public Consumer getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(Consumer consumerId) {
		this.consumerId = consumerId;
	}
	public Long getTopicId() {
		return topicId;
	}
	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}
