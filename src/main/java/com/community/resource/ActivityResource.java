package com.community.resource;

public class ActivityResource extends BaseResource {

	private String activityName;

	private String description;

	private Long activityCost;
	
	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getActivityCost() {
		return activityCost;
	}

	public void setActivityCost(Long activityCost) {
		this.activityCost = activityCost;
	}
	
}
