package com.community.resource;


import java.util.List;

import com.community.model.Activity;
import com.community.model.Chat;
import com.community.model.Consumer;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @reviewer Sameer Shukla
 *
 *           Poor DB modeling, either it should be timestamp or date or time....
 *           Estimated Hours where are the minutes ---- Poor modeling done by
 *           Abhijit.
 * 
 *           Removed JSONIgnore from Provider ... Never seen a poor server side
 *           modelling before
 */
public class WorkRequestResource extends BaseResource {

	private String requestDate;
	private String description;
	private Integer estimatedHours = 0;
	private Double estimatedCost = 0D;
	@JsonIgnore
	private Integer processingStatus = 0;
	
	private Consumer consumer;
	private Activity activity;
	private Long providerId;
    private Long communityId;
    private Float actualHours;
	private List<Chat> chat;
	private Float ratings;
	private String reviews;
	private Long chatCount;
	private List<String> communityName;
//private Long entityId;
	// Removed this shit resources dont know why to send incomplete json
	// @JsonProperty(access = Access.WRITE_ONLY)
	// private ConsumerResource consumer;
	//
	// @JsonProperty(access = Access.WRITE_ONLY)
	// private ActivityResource activity;
	//
	// @JsonProperty(access = Access.WRITE_ONLY)
	// private ProviderResource selectedProvider;

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getEstimatedHours() {
		return estimatedHours;
	}

	public Double getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(Double estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public void setEstimatedHours(Integer estimatedHours) {
		this.estimatedHours = estimatedHours;
	}

	public Integer getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(Integer processingStatus) {
		this.processingStatus = processingStatus;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Long getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Long communityId) {
		this.communityId = communityId;
	}

	public Float getActualHours() {
		return actualHours;
	}

	public void setActualHours(Float actualHours) {
		this.actualHours = actualHours;
	}


	public List<Chat> getChat() {
		return chat;
	}

	public void setChat(List<Chat> chatresource) {
		this.chat = chatresource;
	}

	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public Float getRatings() {
		return ratings;
	}

	public void setRatings(Float ratings) {
		this.ratings = ratings;
	}

	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}

	public Long getChatCount() {
		return chatCount;
	}

	public void setChatCount(Long chatCount) {
		this.chatCount = chatCount;
	}

	public List<String> getCommunityName() {
		return communityName;
	}

	public void setCommunityName(List<String> communityName) {
		this.communityName = communityName;
	}

	

}
