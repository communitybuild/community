package com.community.resource;


public class ProviderResource extends BaseResource {

	//private Date dateOfBirth;
	
	//private String ssn;

	//private String imageUrl;

	private String description;

	private ConsumerResource consumer; 
	
	private Float ratingsAggregate;
	
	public ProviderResource() {
	}
	
//	public Date getDateOfBirth() {
//		return dateOfBirth;
//	}
//
//	public void setDateOfBirth(Date dateOfBirth) {
//		this.dateOfBirth = dateOfBirth;
//	}
//
//	public String getSsn() {
//		return ssn;
//	}
//
//	public void setSsn(String ssn) {
//		this.ssn = ssn;
//	}
//
//	public String getImageUrl() {
//		return imageUrl;
//	}
//
//	public void setImageUrl(String imageUrl) {
//		this.imageUrl = imageUrl;
//	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ConsumerResource getConsumer() {
		return consumer;
	}

	public void setConsumer(ConsumerResource consumer) {
		this.consumer = consumer;
	}

	public Float getRatingsAggregate() {
		return ratingsAggregate;
	}

	public void setRatingsAggregate(Float ratingsAggregate) {
		this.ratingsAggregate = ratingsAggregate;
	}

}
