package com.community.resource;


public class RatingsResource extends BaseResource {

	private Long workRequestId;
	private Long providerId;
	private Float ratings;
	private String reviews;

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public Float getRatings() {
		return ratings;
	}

	public void setRatings(Float ratings) {
		this.ratings = ratings;
	}

	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}

	public Long getWorkRequestId() {
		return workRequestId;
	}

	public void setWorkRequestId(Long workRequestId) {
		this.workRequestId = workRequestId;
	}
	
}
